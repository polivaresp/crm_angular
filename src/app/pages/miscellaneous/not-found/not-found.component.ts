import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-not-found',
  styleUrls: ['./not-found.component.scss'],
  templateUrl: './not-found.component.html',
})
export class NotFoundComponent {

  home;

  constructor(private _router: Router) {
    let user = JSON.parse(localStorage.getItem('usr_login'));
    let perfil = user.perfil;
    this.home = perfil.home;
  }

  goToHome() {
    this._router.navigate([this.home]);
  }
}
