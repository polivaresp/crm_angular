import { Component } from '@angular/core';

import * as moment from 'moment';
/* import * as md5 from 'md5'; */

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsuarioService } from '../services/usuario_service';
import { ClienteService } from '../../../cliente/services/cliente.service';
/* import { PasswordValidation  } from '../../../validators/custom-validators'; */
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-agregar-usuario',
  templateUrl: './agregar_usuario.component.html',
  providers: [UsuarioService, ClienteService, BaseService]
})
export class AgregarUsuarioComponent {

  perfiles: any;
  public user: any;
  public clientes;
  public formUsuario: FormGroup;

  constructor(private _router: Router, private fb: FormBuilder, private _userService: UsuarioService,
    private _toasterService: ToasterService, private _clienteService: ClienteService, private _baseService: BaseService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.clientes = [];
    this.formUsuario = fb.group({
      id_perfil: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ])],
      password_confirm: ['', Validators.compose([
        Validators.required,
      ])],
      nombre: ['', Validators.compose([
        Validators.required,
      ])],
      apellido_paterno: ['', Validators.required],
      apellido_materno: '',
      email: ['', Validators.compose([
        Validators.required
      ])],
      sexo: '',
      activo: true,
      notificacion: '',
      gestionable: '',
    }, {
        /* validator: PasswordValidation.MatchPassword */
      });
  }

  ngOnInit() {
    this._baseService.lista_simple('perfil', { elegible: 1 }).subscribe(response => {
      if (response.resultado == "ACK") {
        let lista = [];
        let id_perfil = this.user.id_perfil;
        response.lista.forEach(function (e, i) {
          let id = e.id;
          if (id_perfil == 3) {
            if ([2, 3, 5, 7].indexOf(id) > -1) {
              lista.push(e);
            }
          } else if (id_perfil == 1 || id_perfil == 100) {
            lista = response.lista;
          }
        });
        this.perfiles = lista;
      } else {
        this.showToast("error", "Error", "error a cargar perfiles");
      }
    }, error => {
      this.showToast("error", "Error", "error a cargar perfiles");
    });
  }


  submit() {
    this._userService.agregar(this.formUsuario.value, this.user).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/usuario/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/usuario/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}