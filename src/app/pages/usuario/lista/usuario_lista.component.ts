import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario_service';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import * as moment from 'moment';

@Component({
  selector: 'ngx-usuario-lista',
  templateUrl: './usuario_lista.component.html',
  providers: [UsuarioService, BaseService]
})
export class UsuarioListaComponent {

  perfiles: any[];
  pagina_actual: number;
  public user: any;
  public usuarios: Array<any>;
  public cantidad: number;
  anular_usuario = [];
  habilitar_usuario = [];
  filtros;
  acciones: any;
  cantidad_pagina: any;
  previusPage: number;

  constructor(private router: Router, private _router: Router, private _userService: UsuarioService,
    private _toasterService: ToasterService, private _baseService: BaseService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.acciones = JSON.parse(localStorage.getItem('acciones'));
    this.usuarios = [];
    this.cantidad = 0;
    this.filtros = [];
    this.filtros["id_perfil"] = '';
    this.filtros["nombre"] = '';
    this.filtros["activo"] = '1';
    this.pagina_actual = 1;
  }

  ngOnInit() {
    this._baseService.lista_simple('perfil', { elegible: 1 }).subscribe(response => {
      if (response.resultado == "ACK") {
        this.perfiles = response.lista;
      } else {
        this.showToast("error", "Error", "error a cargar perfiles");
      }
    }, error => {
      this.showToast("error", "Error", "error a cargar perfiles");
    });
    this.submit(1);
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }

  agregar() {
    this.router.navigate(['pages/usuario/agregar']);
  }

  submit(reiniciar_pagina = 0) {
    if (reiniciar_pagina == 1) {
      this.pagina_actual = 1;
    }

    this.usuarios = [];
    let filtros = {
      id_perfil: this.filtros["id_perfil"] && this.filtros["id_perfil"] != '' ? this.filtros["id_perfil"] : '',
      nombre_completo: this.filtros["nombre"] && this.filtros["nombre"] != '' ? this.filtros["nombre"] : '',
      activo: this.filtros["activo"] && this.filtros["activo"] != '' ? this.filtros["activo"] : '',
    }
    this._userService.lista(filtros, this.user.id_perfil, this.pagina_actual).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.usuarios = response.lista;
        this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", 'Error interno al cargar Usuarios');
    });
  }

  limpiar_filtros() {
    this.filtros["id_perfil"] = "";
    this.filtros["nombre"] = "";
    this.filtros["activo"] = "";
    this.submit(1);
  }

  getDate(fecha, format) {
    if (fecha) {
      if (fecha != '') {
        let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
        return x;
      }
    }
    return '-';
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

  editar(obj) {
    this._router.navigate(['pages/usuario/editar/' + obj.id_usuario]);
  }

  anular(obj) {
    this._userService.desactivar(obj.id_usuario).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this.submit(1);
        this.anular_usuario = [];
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  habilitar(obj) {
    this._userService.habilitar(obj.id_usuario).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this.submit(1);
        this.habilitar_usuario = [];
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

}