import { Component } from '@angular/core';

import * as moment from 'moment';
/* import * as md5 from 'md5'; */

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsuarioService } from '../services/usuario_service';
import { ClienteService } from '../../../cliente/services/cliente.service';
/* import { PasswordValidation  } from '../../../validators/custom-validators'; */
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-editar-usuario',
  templateUrl: './editar_usuario.component.html',
  providers: [UsuarioService, ClienteService, BaseService]
})
export class EditarUsuarioComponent {

  perfiles: any[];
  user: any;
  id;
  //clientes;
  formUsuario: FormGroup;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _userService: UsuarioService,
    private _toasterService: ToasterService, private _baseService: BaseService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.formUsuario = fb.group({
      id_perfil: ['', Validators.required],
      username: ['', Validators.required],
      password: '',
      password_confirm: '',
      nombre: ['', Validators.compose([
        Validators.required,
      ])],
      apellido_paterno: ['', Validators.required],
      apellido_materno: '',
      email: ['', Validators.compose([
        Validators.required
      ])],
      sexo: '',
      activo: true,
      notificacion: '',
      gestionable: '',
    }, {
        /* validator: PasswordValidation.MatchPassword */
      });
  }

  ngOnInit() {
    this._baseService.lista_simple('perfil', { elegible: 1 }).subscribe(response => {
      if (response.resultado == "ACK") {
        let lista = [];
        let id_perfil = this.user.id_perfil;
        response.lista.forEach(function (e, i) {
          let id = e.id;
          if (id_perfil == 3) {
            if ([2, 3, 5, 7].indexOf(id) > -1) {
              lista.push(e);
            }
          } else if (id_perfil == 1 || id_perfil == 100) {
            lista = response.lista;
          }

        });
        this.perfiles = lista;
      } else {
        this.showToast("error", "Error", "error a cargar perfiles");
      }
    }, error => {
      this.showToast("error", "Error", "error a cargar perfiles");
    });
    this.cargar_form();
  }

  cargar_form() {
    this._userService.ver(this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.formUsuario.controls['username'].setValue(response.usuario.username);
        this.formUsuario.controls['nombre'].setValue(response.usuario.nombre);
        this.formUsuario.controls['apellido_paterno'].setValue(response.usuario.apellido_paterno);
        this.formUsuario.controls['apellido_materno'].setValue(response.usuario.apellido_materno);
        this.formUsuario.controls['email'].setValue(response.usuario.email);
        this.formUsuario.controls['id_perfil'].setValue(response.usuario.id_perfil);
        this.formUsuario.controls['activo'].setValue(response.usuario.activo == 1 ? true : false);
        this.formUsuario.controls['notificacion'].setValue(response.usuario.notificacion ? response.usuario.notificacion : '');
        this.formUsuario.controls['gestionable'].setValue(response.usuario.gestionable ? response.usuario.gestionable : '');
      } else {
        this.showToast('error', 'Error Interno', response.mensaje);
      }
    }, error => {
      console.log(error);
      this.showToast('error', 'Error Interno', error.statusText);
    });
  }


  submit() {

    this._userService.editar(this.formUsuario.value, this.id).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/usuario/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/usuario/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}