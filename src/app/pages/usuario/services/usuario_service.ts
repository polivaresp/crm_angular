import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
//import * as md5 from 'md5';
import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs';

@Injectable()
export class UsuarioService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista_simple(perfiles): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_perfil: perfiles };

        return this._http.post(this.base_url + "usuario/lista_simple", data, { headers: headerss });

    }

    lista_gestionables(perfiles): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_perfil: perfiles };

        return this._http.post(this.base_url + "usuario/lista_gestionables", data, { headers: headerss });

    }

    lista_operadores(pagina): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + "usuario/lista_operadores/" + pagina, data, { headers: headerss });

    }

    lista(filtros, id_perfil, pagina): Observable<any> {
        let headerss = this.getHeaders();
        let data = { filtros: filtros, perfil: id_perfil };

        return this._http.post(this.base_url + "usuario/lista/" + pagina, data, { headers: headerss });

    }

    agregar(usuario, user): Observable<any> {
        let headerss = this.getHeaders();
        //usuario.password = md5(usuario.password)
        let data = { usuario: usuario };

        return this._http.post(this.base_url + "usuario/agregar", data, { headers: headerss });

    }

    editar(usuario, id): Observable<any> {
        let headerss = this.getHeaders();
        //usuario.password = usuario.password == '' ? '' : md5(usuario.password)
        let data = { usuario: usuario, cliente: this.codigo };

        return this._http.post(this.base_url + "usuario/editar/" + id, data, { headers: headerss });

    }

    ver(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + "usuario/ver/" + id, data, { headers: headerss });

    }

    desactivar(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_usuario: id };

        return this._http.post(this.base_url + "usuario/desactivar", data, { headers: headerss });

    }

    habilitar(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_usuario: id };

        return this._http.post(this.base_url + "usuario/habilitar", data, { headers: headerss });

    }
}