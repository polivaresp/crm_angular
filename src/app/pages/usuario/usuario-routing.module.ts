import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { UsuarioListaComponent } from './lista/usuario_lista.component';
import { AgregarUsuarioComponent } from './agregar/agregar_usuario.component';
import { EditarUsuarioComponent } from './editar/editar_usuario.component';

const routes: Routes = [
  {
    path: '',
    component: UsuarioComponent,
    children: [
      {
        path: 'lista',
        component: UsuarioListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarUsuarioComponent,
      },
      {
        path: 'editar/:id',
        component: EditarUsuarioComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsuarioRoutingModule { }

export const routedComponents = [
  UsuarioComponent,
  AgregarUsuarioComponent,
  UsuarioListaComponent,
  EditarUsuarioComponent
];
