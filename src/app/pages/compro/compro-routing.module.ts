import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComproComponent } from './compro.component';
import { ComproListaComponent } from './lista/compro_lista.component';
import { EditarComproComponent } from './editar/editar_compro.component';
import { AgregarComproComponent } from './agregar/agregar_compro.component';


const routes: Routes = [
    {
    path: '',
    component: ComproComponent,
    children: [
    {
      path: 'lista',
      component: ComproListaComponent,
    },
    {
        path: 'agregar',
        component: AgregarComproComponent,
    },
    {
      path: 'editar/:id',
      component: EditarComproComponent,
    },
    {
      path: '',
      redirectTo: 'lista',
      pathMatch:  'full'
    }
  ],
  }];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComproRoutingModule { }

export const routedComponents = [
  ComproComponent,
  ComproListaComponent,
  EditarComproComponent,
  AgregarComproComponent
];
