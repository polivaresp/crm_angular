import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { ComproService } from '../services/compro.service';


@Component({
  selector: 'ngx-editar-compro',
  templateUrl: './editar_compro.component.html',
  providers: [BaseService, ComproService]
})
export class EditarComproComponent {

  padres;
  form: FormGroup;
  id: any;
  dad: any;
  hijos: any[];
  child: string;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _comproService: ComproService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.padres = [];
    this.hijos = [];
    this.form = fb.group({
      glosa_compro: ['', Validators.required],
      id_padre: null,
      activo: true
    });
    this.dad = null;
    this.child = null;
  }

  ngOnInit() {
    this._baseService.lista_simple('compro', { id_padre: null }).subscribe(response => {
      if (response.resultado == "ACK") {
        this.padres = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });

    this._comproService.ver(this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        let padre = "null";
        let hijo = "null";
        if (response.padre) {
          padre = response.padre.id_compro;
          if (response.abuelo) {
            hijo = response.padre.id_compro;
            padre = response.abuelo.id_compro;
          }
        }
        this.dad = padre;
        this.cambio_padre();
        this.child = hijo;
        this.cambio_hijo();
        this.form.controls['glosa_compro'].setValue(response.modelo["glosa_compro"]);
        this.form.controls['activo'].setValue(response.modelo["activo"]);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_padre() {
    this.child = "null";
    if (this.dad != null) {
      this._baseService.lista_simple('compro', { id_padre: this.dad }).subscribe(response => {
        if (response.resultado == "ACK") {
          let aux = [];
          let id = this.id;
          response.lista.forEach(function (el) {
            let value = el;
            if (value.id != id) {
              aux.push(el);
            }
          });
          this.hijos = aux;

        } else {
          this.showToast("error", "Error", response.mensaje);
        }
      }, error => {
        this.showToast("error", "Error", error);
      });
    }
    this.form.controls["id_padre"].setValue(this.dad);
  }

  cambio_hijo() {
    if (this.child != "null") {
      this.form.controls["id_padre"].setValue(this.child);
    } else {
      this.form.controls["id_padre"].setValue(this.dad);
    }
  }

  submit() {
    let form = [];
    form["glosa_compro"] = this.form.value.glosa_compro;
    form["activo"] = this.form.value.activo;
    form["id_padre"] = this.form.value.id_padre;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.editar('compro', formulario, this.id).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/compro/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/compro/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}