import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'ngx-compro',
  template: `<router-outlet></router-outlet>`
})
export class ComproComponent {
}
