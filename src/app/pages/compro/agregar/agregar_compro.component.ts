import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-agregar-compro',
  templateUrl: './agregar_compro.component.html',
  providers: [BaseService]
})
export class AgregarComproComponent {

  padres;
  hijos;
  form: FormGroup;
  dad;
  child;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService) {
    this.padres = [];
    this.hijos = [];
    this.form = fb.group({
      glosa_compro: ['', Validators.required],
      id_padre: null,
      activo: true,
    });
    this.dad = null;
    this.child = null;
  }

  ngOnInit() {
    this._baseService.lista_simple('compro', { id_padre: null }).subscribe(response => {
      if (response.resultado == "ACK") {
        this.padres = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_padre() {
    if (this.dad != null) {
      this._baseService.lista_simple('compro', { id_padre: this.dad }).subscribe(response => {
        if (response.resultado == "ACK") {
          this.hijos = response.lista;
        } else {
          this.showToast("error", "Error", response.mensaje);
        }
      }, error => {
        this.showToast("error", "Error", error);
      });
    }
    this.form.controls["id_padre"].setValue(this.dad);
  }

  cambio_hijo() {
    if (this.child != "null") {
      this.form.controls["id_padre"].setValue(this.child);
    } else {
      this.form.controls["id_padre"].setValue(this.dad);
    }
  }

  submit() {
    let form = [];
    form["glosa_compro"] = this.form.value.glosa_compro;
    form["activo"] = this.form.value.activo;
    form["id_padre"] = this.form.value.id_padre;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.agregar('compro', formulario).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/compro/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/compro/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}