import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { ComproService } from '../services/compro.service';

import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import * as moment from 'moment';
import { NbDialogService } from '@nebular/theme';

@Component({
    selector: 'ngx-compro-lista',
    templateUrl: './compro_lista.component.html',
    providers: [BaseService, ComproService]
})
export class ComproListaComponent {

    cantidad: number;
    pagina_actual: number;
    lista;
    anular_compro = [];
    habilitar_compro = [];
    acciones: any;
    ver_hijos: any[];
    ver_nietos: any[];
    etiquetas: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _modalService: NbDialogService, private _toasterService: ToasterService, private _baseService: BaseService,
        private _comproService: ComproService) {
        this.lista = [];
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.pagina_actual = 1;
        this.cantidad = 0;
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
        this.ver_hijos = [];
        this.ver_nietos = [];
    }

    ngOnInit() {
        this.submit(1);
    }

    anular(obj) {
        this._comproService.desactivar(obj.id_compro).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_compro = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar('compro', obj.id_compro).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_compro = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/compro/editar/' + obj.id_compro]);
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'compro',
                lista: lista,
                obj: obj,
            }
        });

        activeModal.onClose.subscribe(result => {
            if (result) {
                this.submit();
            } else {
                console.log("closed");
            }
        });
    }

    agregar() {
        this._router.navigate(['pages/compro/agregar']);
    }

    boton_hijos(padre) {
        padre["ver_hijos"] = !padre["ver_hijos"];
    }

    boton_nietos(child) {
        child.ver_nietos = !child.ver_nietos;
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._comproService.lista_desplegada({}, ["id_compro", "ASC"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {

                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}