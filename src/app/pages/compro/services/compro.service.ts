import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { APP_CONFIG} from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ComproService {

    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders(){
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista(post={},orden = ['orden','asc'], pagina) :Observable<any>{
        let headerss = this.getHeaders();
        return this._http.post(this.base_url +"compro/lista/"+pagina, {post,orden}, {headers:headerss});
    }

    lista_desplegada(post={},orden = ['orden','asc'], pagina) : Observable<any>{
        let headerss = this.getHeaders();
        return this._http.post(this.base_url +"compro/lista_desplegada", {post,orden}, {headers:headerss});
    }

    ver(id) : Observable<any>{
        let headerss = this.getHeaders();

        // get users from api
        return this._http.post(this.base_url+'compro/ver/'+id, JSON.stringify({}) ,{headers:headerss});
    }

    agregar( form) : Observable<any>{
        let headerss = this.getHeaders();
        let data = {campos: form};

        return this._http.post(this.base_url+'compro/agregar', data, {headers:headerss});
    }

    editar( form, id) : Observable<any>{
        let headerss = this.getHeaders();

        let data = {'campos': form};

        return this._http.post(this.base_url+'compro/editar/'+id, data, {headers:headerss});
    }

    desactivar(id) : Observable<any>{
        let headerss = this.getHeaders();

        let data = {};

        return this._http.post(this.base_url+'compro/desactivar/'+id, data, {headers:headerss});
    }
}