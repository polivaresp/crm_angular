import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ComproRoutingModule, routedComponents } from './compro-routing.module';
import { NbButtonModule, NbCheckboxModule, NbCardModule } from '@nebular/theme';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    ComproRoutingModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NgbPaginationModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class ComproModule { }
