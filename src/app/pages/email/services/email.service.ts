import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EmailService {

    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista_simple(post = {}, orden = ['orden', 'asc']): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "plantilla/lista_simple", { post, orden }, { headers: headerss });
    }

    lista(post = {}, orden = ['orden', 'asc'], pagina): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "plantilla/lista/" + pagina, { post, orden }, { headers: headerss });
    }

    ver(id): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + 'plantilla/ver/' + id, JSON.stringify({}), { headers: headerss });
    }

    distribucion(data): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + 'plantilla/distribucion', data, { headers: headerss });
    }

    funciones(): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + 'plantilla/funciones', {}, { headers: headerss });
    }

    tags(post): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + 'plantilla/tags', post, { headers: headerss });
    }
}