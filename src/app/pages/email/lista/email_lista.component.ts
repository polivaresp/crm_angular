import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { EmailService } from '../services/email.service';

import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import * as moment from 'moment';
import { NbDialogService } from '@nebular/theme';

@Component({
    selector: 'ngx-email-lista',
    templateUrl: './email_lista.component.html',
    providers: [BaseService, EmailService]
})
export class EmailListaComponent {

    cantidad: number;
    pagina_actual: number;
    lista;
    anular_email = [];
    habilitar_email = [];
    //funciones;
    //plantillas;
    //editar_funcion;
    form;
    tipos;
    acciones: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _emailService: EmailService, private _modalService: NbDialogService) {
        this.lista = [];
        this.pagina_actual = 1;
        this.cantidad = 0;
        //this.plantillas = [];
        //this.editar_funcion = 0;        
        //this.funciones = [];
        this.form = [];
        this.form["id_tipo"] = "";
        this.tipos = [];
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
    }

    ngOnInit() {
        this._baseService.lista_simple('tipo_email', {}, ['orden', 'asc']).subscribe(response => {
            if (response.resultado == "ACK") {
                this.tipos = response.lista;
            } else {
                console.log("error a cargar tipos");
            }
        }, err => {
            this.showToast("error", "Error Interno", err);
        });
        //this.cargar_distribucion();
        this.submit(1);
    }

    /* cargar_distribucion(){
        this._emailService.funciones().subscribe(response => {
            if (response.resultado == "ACK") {
                this.funciones = response.lista;
            } else {
                console.log("error a cargar funciones");
            }
        }, err => {
            this.showToast("error","Error Interno",err);
        });
        this._baseService.lista_simple('plantilla',{},['orden', 'asc']).subscribe(response => {
            if (response.resultado == "ACK") {
                this.plantillas = response.lista;
            } else {
                console.log("error a cargar plantilla");
            }
        }, err => {
            this.showToast("error","Error Interno",err);
        });
    } */

    anular(obj) {
        this._baseService.desactivar('plantilla', obj.id_plantilla).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_email = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar('plantilla', obj.id_plantilla).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_email = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/email/editar/' + obj.id_plantilla]);
    }

    agregar() {
        this._router.navigate(['pages/email/agregar']);
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'plantilla',
                obj: obj,
            }
        })

        activeModal.onClose.subscribe(result => {
            if (result) {
                this.submit();
            } else {
                console.log("closed");
            }
        });
    }

    /* guardar_funciones(){
        let aux = this.funciones;
        let distribucion = [];
        aux.forEach(element => {
            distribucion.push(element.funcion);
        });
        console.log(distribucion);
        this._emailService.distribucion(distribucion).subscribe(response => {
            if(response.resultado == "ACK"){
                this.showToast("success", "Guardado", response.mensaje);
                this.editar_funcion = 0;
            }else{
                this.showToast("error", "Error", response.mensaje);
            }
        }, err =>{
            this.showToast("error", "Error", err);
        })
    } */

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        let form = [];
        form["id_tipo"] = this.form["id_tipo"];
        let formulario = {};
        Object.assign(formulario, form);
        this._emailService.lista(formulario, ["orden", "ASC"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;

            } else {
                //                this.showToast("error","Error",response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}