import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmailComponent } from './email.component';
import { EmailListaComponent } from './lista/email_lista.component';
import { EditarEmailComponent } from './editar/editar_email.component';
import { AgregarEmailComponent } from './agregar/agregar_email.component';
import { AsignacionEmailComponent } from './asignacion/asignacion.component';


const routes: Routes = [
  {
    path: '',
    component: EmailComponent,
    children: [
      {
        path: 'lista',
        component: EmailListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarEmailComponent,
      },
      {
        path: 'editar/:id',
        component: EditarEmailComponent,
      },
      {
        path: 'asignacion',
        component: AsignacionEmailComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailRoutingModule { }

export const routedComponents = [
  EmailComponent,
  EmailListaComponent,
  EditarEmailComponent,
  AgregarEmailComponent,
  AsignacionEmailComponent
];
