import { Component } from '@angular/core';

import '../ckeditor/ckeditor.loader';
import 'ckeditor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { BaseService } from '../../../services';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { EmailService } from '../services/email.service';
//import { ClipboardService } from 'ngx-clipboard';

@Component({
  selector: 'ngx-editar-email',
  templateUrl: './editar_email.component.html',
  providers: [BaseService, EmailService]
})
export class EditarEmailComponent {
  tags;
  tipos;
  editor_model: any;
  editor_config: { extraPlugins: string; height: string; };
  form: FormGroup;
  id: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _emailService: EmailService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.form = fb.group({
      glosa_plantilla: ['', Validators.required],
      id_tipo: ['', Validators.required],
      para: ['', Validators.required],
      copia: '',
      asunto: ['', Validators.required],
      activo: true,
    });
    this.editor_config = { extraPlugins: 'divarea', height: '400' };
    this.editor_model;
    this.tipos = [];
    this.tags = [];
  }

  ngOnInit() {
    this._baseService.lista_simple('tipo_email', {}, ['orden', 'asc']).subscribe(response => {
      if (response.resultado == "ACK") {
        this.tipos = response.lista;
      } else {
        console.log("error a cargar estado");
      }
    }, function (err) {
      this.showToast("error", "Error Interno", err);
    });
    this._baseService.ver('plantilla', this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.form.controls['glosa_plantilla'].setValue(response.modelo["glosa_plantilla"]);
        this.form.controls['id_tipo'].setValue(response.modelo["id_tipo"] ? response.modelo["id_tipo"] : '');
        this.cambio_tipo();
        this.form.controls['para'].setValue(response.modelo["para"]);
        this.form.controls['asunto'].setValue(response.modelo["asunto"]);
        this.form.controls['copia'].setValue(response.modelo["copia"]);
        this.form.controls['activo'].setValue(response.modelo["activo"]);
        this.editor_model = atob(response.modelo.body); //decode base64
        //this.showToast("success","Exito",response.mensaje);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_tipo() {
    this._emailService.tags({ id_tipo: this.form.value.id_tipo }).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.tags = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  submit() {

    let form = [];
    form["glosa_plantilla"] = this.form.value.glosa_plantilla;
    form["id_tipo"] = this.form.value.id_tipo;
    form["body"] = btoa(this.editor_model); //encode base64
    form["asunto"] = this.form.value.asunto; //encode base64
    let para = this.form.value.para.split(',').join(';');
    para = para.split(';').join(';');
    form["para"] = para;
    if (this.form.value.copia != '') {
      let copia = this.form.value.copia.split(',').join(';');
      copia = copia.split(';').join(';');
      form["copia"] = copia;
    } else {
      form["copia"] = ''
    }
    form["activo"] = this.form.value.activo;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.editar('plantilla', formulario, this.id).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/email/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/email/lista']);
  }

  copy_tag(tag) {
    //this._clipboardService.copyFromContent(tag);
    this.showToast('info', 'Tag Copiado', 'Se ha copiado el tag ' + tag + ' al portapapeles');
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}