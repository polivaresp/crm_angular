import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { EmailRoutingModule, routedComponents } from './email-routing.module';
import { CKEditorModule } from 'ng2-ckeditor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbTabsetModule, NbButtonModule } from '@nebular/theme';

@NgModule({
  imports: [
    ThemeModule,
    EmailRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NbCardModule,
    NbCheckboxModule,
    CKEditorModule,
    NbTabsetModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class EmailModule { }
