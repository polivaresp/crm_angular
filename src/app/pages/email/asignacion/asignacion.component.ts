import { Component } from '@angular/core';

import '../ckeditor/ckeditor.loader';
import 'ckeditor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { BaseService } from '../../../services';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { EmailService } from '../services/email.service';

@Component({
  selector: 'ngx-asignacion-email',
  templateUrl: './asignacion.component.html',
  providers: [BaseService, EmailService]
})
export class AsignacionEmailComponent {
  funciones: any;
  servicios: any[];
  tabs: any[];
  estados_visible: any;
  item_visible: any;
  estados: any;
  items: any;
  plantillas_items: any[];
  plantillas_estados: any[];
  etiquetas: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _emailService: EmailService) {
    this.servicios = localStorage.getItem('servicios') ? JSON.parse(localStorage.getItem('servicios')) : [];
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.estados_visible = 0;
    this.item_visible = 0;
    this.plantillas_items = [];
    this.plantillas_estados = [];
  }

  ngOnInit() {
    this.carga_listados();
    this.carga_tabs();
  }

  carga_listados() {
    this._emailService.funciones().subscribe(response => {
      if (response.resultado == "ACK") {
        this.funciones = response.lista;
      } else {
        console.log("error a cargar funciones");
      }
    }, err => {
      this.showToast("error", "Error Interno", err);
    });
    this._baseService.lista('estado', { 'notificable': 1 }, ["orden", "ASC"], "all").subscribe(response => {
      if (response.resultado == "ACK") {
        this.estados = response.lista;
        this._baseService.lista_simple('plantilla', { id_tipo: 11 }, ["orden", "ASC"]).subscribe(response => {
          if (response.resultado == "ACK") {
            this.plantillas_estados = response.lista;
          } else {
            console.log("error a cargar plantillas");
          }
        }, err => {
          this.showToast("error", "Error Interno", err);
        });
      } else {
        console.log("error a cargar funciones");
      }
    }, err => {
      this.showToast("error", "Error Interno", err);
    });
    this._baseService.lista('item', {}, ["orden", "ASC"], "all").subscribe(response => {
      if (response.resultado == "ACK") {
        this.items = response.lista;
        this._baseService.lista_simple('plantilla', { id_tipo: 8 }, ["orden", "ASC"]).subscribe(response => {
          if (response.resultado == "ACK") {
            this.plantillas_items = response.lista;
          } else {
            console.log("error a cargar plantillas");
          }
        }, err => {
          this.showToast("error", "Error Interno", err);
        });
      } else {
        console.log("error a cargar funciones");
      }
    }, err => {
      this.showToast("error", "Error Interno", err);
    });
  }

  carga_tabs() {
    let aux_item = 0;
    let aux_estado = 0;
    this.servicios.forEach(function (servicio) {
      if (servicio.id_servicio == 2) {
        aux_item = 1;
      } else if (servicio.id_servicio == 4) {
        aux_estado = 1;
      }
    });
    this.estados_visible = aux_estado;
    this.item_visible = aux_item;
  }

  guardar_asignacion() {
    let aux = this.funciones;
    let funciones = [];
    aux.forEach(element => {
      funciones.push(element.funcion);
    });
    let data = {
      "funciones": funciones,
      "estados": this.estados,
      "items": this.items
    };
    this._emailService.distribucion(data).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, err => {
      this.showToast("error", "Error", err);
    });
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/email/lista']);
  }


  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}