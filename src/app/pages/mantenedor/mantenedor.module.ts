import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { MantenedorRoutingModule, routedComponents } from './mantenedor-routing.module';
import { NbCardModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    ThemeModule,
    MantenedorRoutingModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NgbPaginationModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class MantenedorModule { }
