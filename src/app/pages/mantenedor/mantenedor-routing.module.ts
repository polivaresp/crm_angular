import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MantenedorComponent } from './mantenedor.component';
import { MantenedorListaComponent } from './lista/mantenedor_lista.component';
import { EditarMantenedorComponent } from './editar/editar_mantenedor.component';
import { AgregarMantenedorComponent } from './agregar/agregar_mantenedor.component';

const routes: Routes = [
  {
    path: '',
    component: MantenedorComponent,
    children: [
      {
        path: 'lista',
        component: MantenedorListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarMantenedorComponent,
      },
      {
        path: 'editar/:modelo/:id',
        component: EditarMantenedorComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MantenedorRoutingModule { }

export const routedComponents = [
  MantenedorComponent,
  MantenedorListaComponent,
  EditarMantenedorComponent,
  AgregarMantenedorComponent,
];
