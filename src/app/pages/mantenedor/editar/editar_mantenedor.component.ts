import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-editar-mantenedor',
  templateUrl: './editar_mantenedor.component.html',
  providers: [BaseService]
})
export class EditarMantenedorComponent {

  modelos: { id: string; glosa: string; }[];
  form: FormGroup;
  modelo: any;
  id: any;
  etiquetas: any;
  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
      this.modelo = params['modelo'];
    });
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.form = fb.group({
      modelo: [{ value: '', disabled: true }, Validators.required],
      glosa: ['', Validators.required],
      activo: true,
    });
    this.modelos = [
      { id: "origen", glosa: "Origen" },
      { id: "campania", glosa: "Campaña" },
      { id: "grupo_sucursal", glosa: this.etiquetas.grupo_sucursal },
      { id: "medio_contacto", glosa: "Medio Contacto" },
    ];
  }

  ngOnInit() {
    this._baseService.ver(this.modelo, this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.form.controls['modelo'].setValue(this.modelo);
        this.form.controls['glosa'].setValue(response.modelo["glosa_" + this.modelo]);
        this.form.controls['activo'].setValue(response.modelo["activo"]);
        //this.showToast("success","Exito",response.mensaje);
      } else {
        //this.showToast("error","Error",response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  submit() {

    let form = [];
    form["glosa_" + this.modelo] = this.form.value.glosa;
    form["activo"] = this.form.value.activo;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.editar(this.modelo, formulario, this.id).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/mantenedor/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/mantenedor/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}