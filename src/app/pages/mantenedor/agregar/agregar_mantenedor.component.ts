import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-agregar-mantenedor',
  templateUrl: './agregar_mantenedor.component.html',
  providers: [BaseService]
})
export class AgregarMantenedorComponent {

  modelos: { id: string; glosa: string; }[];
  form: FormGroup;
  etiquetas: any;
  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService) {
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.form = fb.group({
      modelo: ['', Validators.required],
      glosa: ['', Validators.required],
      activo: true,
    });
    this.modelos = [
      { id: "origen", glosa: "Origen" },
      { id: "campania", glosa: "Campaña" },
      { id: "grupo_sucursal", glosa: this.etiquetas.grupo_sucursal },
      { id: "medio_contacto", glosa: "Medio Contacto" },
    ];
  }

  ngOnInit() {

  }

  submit() {

    let form = [];
    form["glosa_" + this.form.value.modelo] = this.form.value.glosa;
    form["activo"] = this.form.value.activo;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.agregar(this.form.value.modelo, formulario).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/mantenedor/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/mantenedor/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}