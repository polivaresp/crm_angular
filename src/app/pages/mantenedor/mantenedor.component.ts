import { Component } from '@angular/core';

@Component({
  selector: 'ngx-mantenedor',
  template: `<router-outlet></router-outlet>`
})
export class MantenedorComponent {
}
