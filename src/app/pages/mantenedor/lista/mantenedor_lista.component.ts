import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';

import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import * as moment from 'moment';
import { NbDialogService } from '@nebular/theme';

@Component({
    selector: 'ngx-mantenedor-lista',
    templateUrl: './mantenedor_lista.component.html',
    providers: [BaseService]
})
export class MantenedorListaComponent {

    form;
    modelo;
    modelos;
    anular_modelo = [];
    habilitar_modelo = [];
    acciones: any;
    etiquetas: any;
    lista: any[];
    pagina_actual: number;
    cantidad: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _modalService: NbDialogService) {
        this.lista = [];
        this.pagina_actual = 1;
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.form = [];
        this.form["modelo"] = "";
        this.modelo = "";
        this.modelos = [
            { id: "origen", glosa: "Origen" },
            { id: "campania", glosa: "Campaña" },
            { id: "grupo_sucursal", glosa: this.etiquetas.grupo_sucursal },
            { id: "medio_contacto", glosa: "Medio Contacto" },
        ];
    }

    ngOnInit() {

    }

    anular(obj) {
        this._baseService.desactivar(this.modelo, obj["id_" + this.modelo]).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_modelo = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar(this.modelo, obj["id_" + this.modelo]).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_modelo = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/mantenedor/editar/' + this.modelo + '/' + obj["id_" + this.modelo]]);
    }

    agregar() {
        this._router.navigate(['pages/mantenedor/agregar']);
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: this.modelo,
                obj: obj
            }
        });
        activeModal.onClose.subscribe(result => {
            if (result) {
                this.submit();
            } else {
                console.log("closed");
            }
        });
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        this.modelo = this.form["modelo"];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._baseService.lista(this.form["modelo"], {}, ["orden", "asc"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}