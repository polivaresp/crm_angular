import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import { NbDialogService } from '@nebular/theme';


@Component({
    selector: 'ngx-perfil-lista',
    templateUrl: './perfil_lista.component.html',
    providers: [BaseService]
})
export class PerfilListaComponent {

    cantidad: number;
    form;
    modelo;
    pagina_actual: number;
    lista;
    modelos;
    anular_modelo = [];
    habilitar_modelo = [];
    acciones: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _modalService: NbDialogService) {
        this.lista = [];
        this.acciones = JSON.parse(localStorage.getItem('acciones'));  
        this.pagina_actual = 1; 
        this.cantidad = 0;
    }

    ngOnInit() {
        this.submit();
        this.refresh();
    }

    refresh() {
        let cambio = localStorage.getItem('cambio_permisos');
        if (cambio == '1') {
            localStorage.removeItem('cambio_permisos');
            location.reload();
        }
    }

    editar_permisos(obj) {
        this._router.navigate(["pages/perfil/permisos/" + obj.id_perfil]);
    }

    editar_acciones(obj) {
        this._router.navigate(["pages/perfil/acciones/" + obj.id_perfil]);
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'perfil',
                obj: obj
            }
        });
        activeModal.onClose.subscribe(result=>{
            if(result){
                this.submit();
            }else{
                console.log("closed");
            }
        });
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._baseService.lista("perfil", { elegible: 1 }, ["orden", "asc"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}