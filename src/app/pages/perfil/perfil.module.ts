import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { PerfilRoutingModule, routedComponents } from './perfil-routing.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    ThemeModule,
    PerfilRoutingModule,
    NgbPaginationModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class PerfilModule { }
