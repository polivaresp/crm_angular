import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './perfil.component';
import { PerfilListaComponent } from './lista/perfil_lista.component';
import { PerfilPermisosComponent } from './permisos/perfil_permisos.component';
import { PerfilAccionesComponent } from './acciones/perfil_acciones.component';

const routes: Routes = [
  {
    path: '',
    component: PerfilComponent,
    children: [
      {
        path: 'lista',
        component: PerfilListaComponent,
      },
      {
        path: 'permisos/:id_perfil',
        component: PerfilPermisosComponent,
      },
      {
        path: 'acciones/:id_perfil',
        component: PerfilAccionesComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilRoutingModule { }

export const routedComponents = [
  PerfilComponent,
  PerfilListaComponent,
  PerfilPermisosComponent,
  PerfilAccionesComponent
];
