import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators'
import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PerfilService {

    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';

    }

    getHeaders() {
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    permisos(perfil): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "id_perfil": perfil };
        return this._http.post(this.base_url + "perfil/permisos", data, { headers: headerss });
    }

    acciones(perfil): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "id_perfil": perfil };
        return this._http.post(this.base_url + "perfil/acciones", data, { headers: headerss });
    }

    modulos(perfil): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "id_perfil": perfil };
        return this._http.post(this.base_url + "perfil/modulos", data, { headers: headerss });
    }

    guardar_permisos(modulos, id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "modulos": modulos, "id_perfil": id };
        return this._http.post(this.base_url + "perfil/guardar_permisos", data, { headers: headerss });
    }

    guardar_acciones(acciones, id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "acciones": acciones, "id_perfil": id };
        return this._http.post(this.base_url + "perfil/guardar_acciones", data, { headers: headerss });
    }

    acciones_por_perfil(perfil): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "id_perfil": perfil };
        return this._http.post(this.base_url + "perfil/acciones_por_perfil", data, { headers: headerss });
    }
}