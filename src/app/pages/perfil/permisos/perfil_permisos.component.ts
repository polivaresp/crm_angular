import { Component } from '@angular/core';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PerfilService } from '../services/perfil.service';

@Component({
    selector: 'ngx-perfil-permisos',
    templateUrl: './perfil_permisos.component.html',
    providers: [PerfilService]
})
export class PerfilPermisosComponent {
    id: any;
    modulos: any;
    etiquetas: any;


    constructor(private _route: ActivatedRoute, private _router: Router, private _toasterService: ToasterService,
        private _perfilService: PerfilService) {
        this._route.params.forEach((params: Params) => {
            this.id = params['id_perfil'];
        });
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    }

    ngOnInit() {
        this._perfilService.modulos(this.id).subscribe(res => {
            if (res.resultado == "ACK") {
                this.modulos = res.lista;
            } else {
                this.showToast('error', 'Error', res.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error Interno', err);
        });
        localStorage.removeItem('cambio_permisos');
    }

    salir_sin_guardar() {
        this._router.navigate(["pages/perfil/lista"]);
    }

    submit() {
        let usuario = JSON.parse(localStorage.getItem('usr_login')).usuario;
        let msg = "";
        if (this.id == usuario.id_perfil) {
            localStorage.setItem('cambio_permisos', '1');
            msg = "<br> Espere Mientras recarga la página";
        }
        this._perfilService.guardar_permisos(this.modulos, this.id).subscribe(res => {
            if (res.resultado == "ACK") {
                this._perfilService.permisos(this.id).subscribe(resp => {
                    if (resp.resultado == "ACK") {
                        localStorage.setItem('permisos', JSON.stringify(resp.lista));
                        this.showToast('success', 'Exitoso', res.mensaje + msg);
                        this._router.navigate(["pages/perfil/lista"]);
                    } else {
                        this.showToast('error', 'Error de Permisos', resp.mensaje);
                        localStorage.setItem('cambio_permisos', '0');
                    }
                });

            } else {
                this.showToast('error', 'Error', res.mensaje);
                localStorage.setItem('cambio_permisos', '0');
            }
        }, err => {
            this.showToast('error', 'Error Interno', err);
            localStorage.setItem('cambio_permisos', '0');
        });
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}