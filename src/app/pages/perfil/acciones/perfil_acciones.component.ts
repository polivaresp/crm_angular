import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PerfilService } from '../services/perfil.service';

@Component({
    selector: 'ngx-perfil-acciones',
    templateUrl: './perfil_acciones.component.html',
    providers: [BaseService, PerfilService]
})
export class PerfilAccionesComponent {
    id: any;
    acciones: any;


    constructor(private _route: ActivatedRoute, private _router: Router, private _toasterService: ToasterService,
        private _baseService: BaseService, private _perfilService: PerfilService) {
        this._route.params.forEach((params: Params) => {
            this.id = params['id_perfil'];
        });
    }

    ngOnInit() {
        this._perfilService.acciones_por_perfil(this.id).subscribe(res => {
            if (res.resultado == "ACK") {
                this.acciones = res.lista;
            } else {
                this.showToast('error', 'Error', res.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error Interno', err);
        });
    }

    salir_sin_guardar() {
        this._router.navigate(["pages/perfil/lista"]);
    }

    submit() {
        let perfil = JSON.parse(localStorage.getItem('usr_login')).perfil;
        this._perfilService.guardar_acciones(this.acciones, this.id).subscribe(res => {
            if (res.resultado == "ACK") {
                this.showToast('success', 'Exitoso', res.mensaje);
                this._perfilService.acciones(perfil.id_perfil).subscribe(response => {
                    if (response.resultado == "ACK") {
                        localStorage.setItem('acciones', JSON.stringify(response.lista));
                        this._router.navigate(["pages/perfil/lista"]);
                    } else {
                        this.showToast('error', 'Error de Permisos', response.mensaje);
                    }
                }, err => {
                    this.showToast('error', 'Error Interno', err.statusText);
                });
            } else {
                this.showToast('error', 'Error', res.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error Interno', err);
        });
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}