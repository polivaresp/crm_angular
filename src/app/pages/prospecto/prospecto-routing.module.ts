import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProspectoComponent } from './prospecto.component';
import { ProspectoListaComponent } from './lista/prospecto_lista.component';
import { AgregarProspectoComponent } from './agregar/agregar_prospecto.component';
import { EditarProspectoComponent } from './editar/editar_prospecto.component';

const routes: Routes = [
  {
    path: '',
    component: ProspectoComponent,
    children: [
      {
        path: 'lista',
        component: ProspectoListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarProspectoComponent,
      },
      {
        path: 'editar/:id',
        component: EditarProspectoComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProspectoRoutingModule { }

export const routedComponents = [
  ProspectoComponent,
  AgregarProspectoComponent,
  ProspectoListaComponent,
  EditarProspectoComponent
];
