import { Component } from '@angular/core';

import * as moment from 'moment';
/* import * as md5 from 'md5'; */;

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ProspectoService } from '../services/prospecto.service';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-agregar-prospecto',
  templateUrl: './agregar_prospecto.component.html',
  providers: [BaseService, ProspectoService]
})
export class AgregarProspectoComponent {

  camposvariables: any[];
  emails_prospecto: any;
  telefonos_prospecto: any;
  contactables: any[];
  emails;
  telefonos: any;
  public user: any;
  public clientes;
  public formProspecto: FormGroup;
  regiones;
  comunas;

  constructor(private _router: Router, private fb: FormBuilder, private _baseService: BaseService, private _prospectoService: ProspectoService,
    private _toasterService: ToasterService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.clientes = [];
    this.comunas = [];
    this.regiones = [];
    this.emails_prospecto = [];
    this.emails_prospecto.push({ glosa: '' });
    this.telefonos_prospecto = [];
    this.telefonos_prospecto.push({ glosa: '' });
    this.formProspecto = fb.group({
      id_contactable: 1,
      rut: ['', Validators.required],
      nombre: ['', Validators.required],
      apellido_paterno: ['', Validators.required],
      emails: ['', Validators.required],
      telefonos: ['', Validators.required],
      apellido_materno: '',
      id_region: '',
      id_comuna: '',
      direccion: '',
      invalido: '',
      dato_0: '',
      dato_1: '',
      dato_2: '',
      dato_3: '',
      dato_4: '',
      dato_5: '',
      dato_6: '',
      dato_7: '',
      dato_8: '',
      dato_9: '',
    });
  }

  ngOnInit() {
    //cargar camposvariables
    this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.camposvariables = response.lista;
        } else {
          this.showToast("error", "Error", "error a cargar camposvariables");
        }
      }, (err) => {
        this.showToast("error", "Error", "camposvariables")
      });
    this.formProspecto.get('rut').valueChanges.subscribe((rut) => {
      this.formProspecto.controls['invalido'].setValue('');
      this.formProspecto.get('invalido').setValidators([]);
      let rut_r = rut.split('.').join('');
      this._prospectoService.ver_rut(rut_r).subscribe(response => {
        if (response.resultado == "ACK") {
          this.showToast("error", "Error", "Ya existe un prospecto con este rut");
          this.formProspecto.get('invalido').setValidators([Validators.required]);
          this.formProspecto.get('invalido').updateValueAndValidity();
        } else {
          //this.showToast("error","Error",response.mensaje);
        }
      }, error => {
        this.showToast("error", "Error", error);
      });
      this.formProspecto.get('invalido').updateValueAndValidity();
    });
    this._baseService.lista_simple('region').subscribe(response => {
      if (response.resultado == "ACK") {
        this.regiones = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });

    this._baseService.lista_simple('contactable').subscribe(response => {
      if (response.resultado == "ACK") {
        this.contactables = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_region() {
    this._baseService.lista_simple('comuna', { id_region: this.formProspecto.value.id_region }).subscribe(response => {
      if (response.resultado == "ACK") {
        this.comunas = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_rut() {
    let rut = this.formProspecto.value.rut.replace('.', '');
    this._prospectoService.ver_rut(rut).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  agregar_telefono() {
    if (this.telefonos_prospecto[this.telefonos_prospecto.length - 1].glosa != '') {
      this.telefonos_prospecto.push({ glosa: '' });
    }
  }

  cambio_telefono() {
    var x = [];
    this.telefonos_prospecto.forEach(function (phone, i) {
      x.push(phone.glosa);
    });
    this.formProspecto.controls["telefonos"].setValue(x.join(','));
  }

  anular_telefono(telefono, index) {
    if (index > 0) {
      this.telefonos_prospecto.splice(index, 1);
    }
    this.cambio_telefono();
  }

  agregar_email() {
    if (this.emails_prospecto[this.emails_prospecto.length - 1].glosa != '') {
      this.emails_prospecto.push({ glosa: '' });
    }
  }

  cambio_email() {
    var x = [];
    this.emails_prospecto.forEach(function (email, i) {
      x.push(email.glosa);
    });
    this.formProspecto.controls["emails"].setValue(x.join(','));
  }

  anular_email(email, index) {
    if (index > 0) {
      this.emails_prospecto.splice(index, 1);
    }
    this.cambio_email();
  }


  submit() {
    var form = this.formProspecto.value;
    form.rut = this.arreglar_rut(form.rut);
    if (!form.rut) {
      this.showToast("error", "Error", "Rut Invalido");
    } else {
      this._prospectoService.agregar(form, this.telefonos_prospecto, this.emails_prospecto).subscribe(response => {
        if (response.resultado == "ACK") {
          this.showToast("success", "Exito", response.mensaje);
          this._router.navigate(['pages/prospecto/lista']);
        } else {
          this.showToast("error", "Error", response.mensaje);
        }
      }, error => {
        this.showToast("error", "Error", error);
      });
    }
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/prospecto/lista']);
  }

  validar_rut(rut) {
    var x_rut = rut.split('.').join('');
    x_rut = x_rut.split('-').join('');
    x_rut = x_rut.split(',').join('');
    x_rut = x_rut.split('/').join('');
    x_rut = x_rut.split('_').join('');
    x_rut = x_rut.split('—').join('');
    x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
    var rut_split = x_rut.split('-');
    var k = 2;
    var total = 0;
    for (let i = rut_split[0].length - 1; i >= 0; i--) {
      if (k > 7) {
        k = 2;
      }
      total += (k * rut_split[0][i]);
      k += 1;
    }
    var resto = total % 11;
    var verificador = 11 - resto;
    var x_verificador = verificador == 10 ? 'k' : verificador == 11 ? 0 : verificador;
    if (rut_split[1] == x_verificador) {
      return x_rut;
    } else {
      return false;
    }
  }

  arreglar_rut(rut) {
    var x_rut = rut.split('.').join('');
    x_rut = x_rut.split('-').join('');
    x_rut = x_rut.split(',').join('');
    x_rut = x_rut.split('/').join('');
    x_rut = x_rut.split('_').join('');
    x_rut = x_rut.split('—').join('');
    x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
    return x_rut;
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}