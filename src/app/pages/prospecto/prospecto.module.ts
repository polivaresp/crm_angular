import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ProspectoRoutingModule, routedComponents } from './prospecto-routing.module';
import { NbCardModule, NbButtonModule, NbCheckboxModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ProspectoRoutingModule,
    NgbPaginationModule,
    NbCheckboxModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class ProspectoModule { }
