import { Component } from '@angular/core';

import * as moment from 'moment';
/* import * as md5 from 'md5'; */;

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ProspectoService } from '../services/prospecto.service';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-editar-prospecto',
  templateUrl: './editar_prospecto.component.html',
  providers: [BaseService, ProspectoService]
})
export class EditarProspectoComponent {

  camposvariables: any[];
  emails_prospecto: any;
  telefonos_prospecto: any;
  contactables: any[];
  emails;
  telefonos: any;
  id;
  public user: any;
  public clientes;
  public formProspecto: FormGroup;
  regiones;
  comunas;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService, private _prospectoService: ProspectoService,
    private _toasterService: ToasterService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.clientes = [];
    this.comunas = [];
    this.regiones = [];
    this.emails_prospecto = [];
    this.emails_prospecto.push({ glosa: '' });
    this.telefonos_prospecto = [];
    this.telefonos_prospecto.push({ glosa: '' });
    this.formProspecto = fb.group({
      id_contactable: 1,
      rut: ['', Validators.required],
      nombre: ['', Validators.required],
      apellido_paterno: ['', Validators.required],
      emails: ['', Validators.required],
      telefonos: ['', Validators.required],
      apellido_materno: '',
      id_region: '',
      id_comuna: '',
      direccion: '',
      dato_0: '',
      dato_1: '',
      dato_2: '',
      dato_3: '',
      dato_4: '',
      dato_5: '',
      dato_6: '',
      dato_7: '',
      dato_8: '',
      dato_9: '',
    });
  }

  ngOnInit() {
    //cargar camposvariables
    this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.camposvariables = response.lista;
        } else {
          this.showToast("error", "Error", "error a cargar camposvariables");
        }
      }, (err) => {
        this.showToast("error", "Error", "camposvariables")
      });
    this._baseService.lista_simple('region').subscribe(response => {
      if (response.resultado == "ACK") {
        this.regiones = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });

    this._baseService.lista_simple('contactable').subscribe(response => {
      if (response.resultado == "ACK") {
        this.contactables = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
    this.cargar_form();
  }

  cargar_form() {
    this._prospectoService.ver(this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.formProspecto.controls['rut'].setValue(response.prospecto.rut);
        this.formProspecto.controls['id_contactable'].setValue(response.prospecto.id_contactable);
        this.formProspecto.controls['nombre'].setValue(response.prospecto.nombre);
        this.formProspecto.controls['apellido_paterno'].setValue(response.prospecto.apellido_paterno);
        this.formProspecto.controls['apellido_materno'].setValue(response.prospecto.apellido_materno);
        this.formProspecto.controls['id_region'].setValue(response.prospecto.id_region);
        this.cambio_region();
        this.formProspecto.controls['id_comuna'].setValue(response.prospecto.id_comuna);
        this.formProspecto.controls['direccion'].setValue(response.prospecto.direccion);
        this.formProspecto.controls['dato_0'].setValue(response.prospecto.dato_0);
        this.formProspecto.controls['dato_1'].setValue(response.prospecto.dato_1);
        this.formProspecto.controls['dato_2'].setValue(response.prospecto.dato_2);
        this.formProspecto.controls['dato_3'].setValue(response.prospecto.dato_3);
        this.formProspecto.controls['dato_4'].setValue(response.prospecto.dato_4);
        this.formProspecto.controls['dato_5'].setValue(response.prospecto.dato_5);
        this.formProspecto.controls['dato_6'].setValue(response.prospecto.dato_6);
        this.formProspecto.controls['dato_7'].setValue(response.prospecto.dato_7);
        this.formProspecto.controls['dato_8'].setValue(response.prospecto.dato_8);
        this.formProspecto.controls['dato_9'].setValue(response.prospecto.dato_9);
        this._baseService.lista_simple('telefonoprospecto', { id_prospecto: this.id, activo: 1 })
          .subscribe(response => {
            if (response.resultado == "ACK") {
              let arraytelefonos = [];
              response.lista.forEach(function (e, i) {
                arraytelefonos.push({ glosa: e.glosa, id: e.id, activo: e.activo });
              });
              this.telefonos_prospecto = arraytelefonos
              this.cambio_telefono();
            } else {
              this.showToast("error", "Error", "error al cargar telefonos prospecto");
            }
          }, (err) => {
            this.showToast("error", "Error", "ayayai telefonos")
          });
        this._baseService.lista_simple('emailprospecto', { id_prospecto: this.id, activo: 1 })
          .subscribe(response => {
            if (response.resultado == "ACK") {
              let arrayemails = [];
              response.lista.forEach(function (e, i) {
                arrayemails.push({ glosa: e.glosa, id: e.id, activo: e.activo });
              });
              this.emails_prospecto = arrayemails;
              this.cambio_email();
            } else {
              this.showToast("error", "Error", "error emails prospecto");
            }
          }, (err) => {
            this.showToast("error", "Error", "ayayai emails")
          });
      } else {
        this.showToast('error', 'Error Interno', response.mensaje);
      }
    }, error => {
      this.showToast('error', 'Error Interno', error);
    });
  }
  cambio_region() {
    this._baseService.lista_simple('comuna', { id_region: this.formProspecto.value.id_region }).subscribe(response => {
      if (response.resultado == "ACK") {
        this.comunas = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_rut() {
    let rut = this.formProspecto.value.rut.replace('.', '');
    this._prospectoService.ver_rut(rut).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  agregar_telefono() {
    if (this.telefonos_prospecto[this.telefonos_prospecto.length - 1].glosa != '') {
      this.telefonos_prospecto.push({ glosa: '', activo: 1 });
    }
  }

  cambio_telefono() {
    var x = [];
    this.telefonos_prospecto.forEach(function (phone, i) {
      if (phone.activo == 1) {
        x.push(phone.glosa);
      }
    });
    this.formProspecto.controls["telefonos"].setValue(x.join(','));
  }

  activar_telefono(telefono, index) {
    this.telefonos_prospecto[index].activo = 1;
  }

  anular_telefono(telefono, index) {
    if (this.telefonos_prospecto[index].glosa == '') {
      this.telefonos_prospecto.splice(index, 1);
    } else {
      this.telefonos_prospecto[index].activo = 0;
    }
    this.cambio_telefono();
  }

  agregar_email() {
    if (this.emails_prospecto[this.emails_prospecto.length - 1].glosa != '') {
      this.emails_prospecto.push({ glosa: '', activo: 1 });
    }
  }

  activar_email(email, index) {
    this.emails_prospecto[index].activo = 1;
  }

  cambio_email() {
    var x = [];
    this.emails_prospecto.forEach(function (email, i) {
      if (email.activo == 1) {
        x.push(email.glosa);
      }
    });
    this.formProspecto.controls["emails"].setValue(x.join(','));
  }

  anular_email(email, index) {
    if (this.emails_prospecto[index].glosa == '') {
      this.emails_prospecto.splice(index, 1);
    } else {
      this.emails_prospecto[index].activo = 0;
    }
    this.cambio_email();
  }


  submit() {
    var form = this.formProspecto.value;
    form.rut = this.arreglar_rut(form.rut);
    this._prospectoService.editar(form, this.telefonos_prospecto, this.emails_prospecto, this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this._router.navigate(['pages/prospecto/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  arreglar_rut(rut) {
    var x_rut = rut.split('.').join('');
    x_rut = x_rut.split('-').join('');
    x_rut = x_rut.split(',').join('');
    x_rut = x_rut.split('/').join('');
    x_rut = x_rut.split('_').join('');
    x_rut = x_rut.split('—').join('');
    x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
    return x_rut;
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/prospecto/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}