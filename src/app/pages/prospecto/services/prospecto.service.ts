import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProspectoService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    ver_rut(rut): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'rut': rut };
        return this._http.post(this.base_url + "prospecto/ver_rut", data, { headers: headerss });

    }

    ver_completo(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'id_prospecto': id };
        return this._http.post(this.base_url + "prospecto/ver_completo", data, { headers: headerss });

    }

    ver(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};
        return this._http.post(this.base_url + "prospecto/ver/" + id, data, { headers: headerss });

    }

    lista(filtros, pagina): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'filtros': filtros };
        return this._http.post(this.base_url + "prospecto/lista/" + pagina, data, { headers: headerss });

    }

    editar(prospecto, telefonos, emails, id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'prospecto': prospecto, telefonos: telefonos, emails: emails };
        return this._http.post(this.base_url + "prospecto/editar/" + id, data, { headers: headerss });

    }

    agregar(prospecto, telefonos, emails): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'prospecto': prospecto, telefonos: telefonos, emails: emails };
        return this._http.post(this.base_url + "prospecto/agregar", data, { headers: headerss });

    }

    camposvariables(post = {}, orden = ['orden', 'asc'], pagina): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "prospecto/camposvariables/" + pagina, { post, orden }, { headers: headerss });

    }
}