import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ProspectoService } from '../services/prospecto.service';


@Component({
  selector: 'ngx-prospecto-lista',
  templateUrl: './prospecto_lista.component.html',
  providers: [ProspectoService]
})
export class ProspectoListaComponent {

  user: any;
  public prospectos: Array<any>;
  cantidad: number;
  pagina: number;
  filtros: any;
  acciones: any;
  previusPage: number;
  cantidad_pagina: any;

  anular_prospecto = [];
  habilitar_prospecto = [];

  constructor(private router: Router, private _router: Router, private _prospectoService: ProspectoService,
    private _toasterService: ToasterService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.prospectos = [];
    this.cantidad = 0;
    this.pagina = 1;
    this.filtros = [];
    this.acciones = JSON.parse(localStorage.getItem('acciones'));
  }

  ngOnInit() {
    this.submit(1);
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }

  agregar() {
    this.router.navigate(['pages/prospecto/agregar']);
  }
  anular(obj){

  }
  habilitar(obj){

  }

  submit(reiniciar_pagina = 0) {
    if (reiniciar_pagina == 1) {
      this.pagina = 1;
    }
    this.prospectos = [];
    let filtros = {
      rut: this.filtros["rut"] && this.filtros["rut"] != '' ? this.filtros["rut"] : '',
      nombre: this.filtros["nombre"] && this.filtros["nombre"] != '' ? this.filtros["nombre"] : '',
      apellidos: this.filtros["apellidos"] && this.filtros["apellidos"] != '' ? this.filtros["apellidos"] : '',
      emails: this.filtros["emails"] && this.filtros["emails"] != '' ? this.filtros["emails"] : '',
      telefonos: this.filtros["telefonos"] && this.filtros["telefonos"] != '' ? this.filtros["telefonos"] : ''
    }
    this._prospectoService.lista(filtros, this.pagina).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.prospectos = response.lista.length > 0 ? response.lista : [];
        this.cantidad = response.lista.length > 0 ? response.cantidad : 0;
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", 'Error interno al cargar Prospectos');
    });
  }

  limpiar_filtros() {
    this.filtros = [];
    this.submit(1);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

  editar(obj) {
    this._router.navigate(['pages/prospecto/editar/' + obj.id_prospecto]);
  }

}