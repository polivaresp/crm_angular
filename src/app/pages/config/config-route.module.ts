import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigComponent } from './config.component';
import { ParametrosComponent } from './parametros/parametros.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigComponent,
    children: [
      {
        path: 'parametros',
        component: ParametrosComponent,
      },
      {
        path: '',
        redirectTo: 'parametros',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigRoutingModule { }

export const routedComponents = [
  ConfigComponent,
  ParametrosComponent
];
