import { Component } from '@angular/core';

@Component({
  selector: 'ngx-config',
  template: `<router-outlet></router-outlet>`
})
export class ConfigComponent {
}
