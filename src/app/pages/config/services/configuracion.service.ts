import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ConfiguracionService {

    public base_url: string;
    public codigo: string;
    public config;
    user;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }


    lista_parametros(): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};
        return this._http.post(this.base_url + "configuracion/lista_parametros", data, { headers: headerss });
    }

    guardar_parametros(parametros): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "parametros": parametros };
        return this._http.post(this.base_url + "configuracion/guardar_parametros", data, { headers: headerss });
    }
}