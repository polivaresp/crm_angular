import { Component } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ConfiguracionService } from '../services/configuracion.service';


@Component({
    selector: 'ngx-parametros',
    templateUrl: './parametros.component.html',
    providers: [ConfiguracionService] //aqui se importan los servicios
})
export class ParametrosComponent {
    parametros: any[];
    constructor(private _route: ActivatedRoute, private _router: Router, private _toasterService: ToasterService,
        private _configuracionService: ConfiguracionService) {
        this.parametros = [];
    }

    ngOnInit() {
        this._configuracionService.lista_parametros().subscribe(res => {
            if (res.resultado == "ACK") {
                this.parametros = res.lista;
            } else {
                this.showToast('error', 'Error', res.mensaje);
            }

        }, err => {
            this.showToast('error', 'Error Interno', err.statusText);
        })
    }

    validar(param) {
        if (param.valor_parametro < param.min) {
            param.valido = 0;
            param.mensaje = "No puede ser valor menor a " + param.min;
        } else if (param.valor_parametro > param.max) {
            param.valido = 0;
            param.mensaje = "No puede ser valor mayor a " + param.max;
        } else {
            param.valido = 1;
            param.mensaje = "";
        }
    }

    verificar() {
        let disabled = false;
        this.parametros.forEach(elem => {
            if (elem.valido == 0) {
                disabled = true;
            }
        });
        return disabled;
    }

    submit() {
        this._configuracionService.guardar_parametros(this.parametros).subscribe(res => {
            if (res.resultado == "ACK") {
                this.showToast('success', 'Exito', res.mensaje);
            } else {
                this.showToast('error', 'Error', res.mensaje);
            }

        }, err => {
            this.showToast('error', 'Error Interno', err.statusText);
        })
    }

    private showToast(type: string, title: string, body: string) { //funcion para mostrar notificaciones de alertas
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}