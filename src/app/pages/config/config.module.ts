import { NgModule } from '@angular/core';
import { ConfigRoutingModule, routedComponents } from './config-route.module';
import { ThemeModule } from '../../@theme/theme.module';
import { NbButtonModule, NbCheckboxModule, NbCardModule } from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    ConfigRoutingModule,
    NbButtonModule,
    NbCardModule,
    NgbModule,
    NbCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule
  ],
  declarations: [
    routedComponents,
  ],
})
export class ConfigModule {
}
