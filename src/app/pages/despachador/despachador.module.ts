import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DespachadorRoutingModule, routedComponents } from './despachador-routing.module';
import { CKEditorModule } from 'ng2-ckeditor';
import { ReenviarModalComponent } from './modal/reenviar.modal';
import { NbCardModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    DespachadorRoutingModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule,
    NgbPaginationModule,
    CKEditorModule,
    NbDialogModule.forChild()
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
  entryComponents: [
    ReenviarModalComponent,
  ],
})
export class DespachadorModule { }
