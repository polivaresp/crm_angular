import { Component } from '@angular/core';

import * as moment from 'moment';


import { Router, Params, ActivatedRoute } from '@angular/router';

import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { DespachadorService } from '../services/despachador.service';


import { ReenviarModalComponent } from '../modal/reenviar.modal';
import { NbDialogService } from '@nebular/theme';


@Component({
  selector: 'ngx-despachador-lista',
  templateUrl: './despachador_lista.component.html',
  providers: [BaseService, DespachadorService]
})
export class DespachadorListaComponent {

  id: any;
  pagina_actual: number;
  lista: Array<any>;
  cantidad: number;
  acciones: any;
  cantidad_pagina: any;
  previusPage: number;

  constructor(private _route: ActivatedRoute, private router: Router, private _router: Router, private _despachadorService: DespachadorService,
    private _toasterService: ToasterService, private _baseService: BaseService, private _modalService: NbDialogService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.lista = [];
    this.cantidad = 0;
    this.pagina_actual = 1;
    this.acciones = JSON.parse(localStorage.getItem('acciones'));
  }

  ngOnInit() {
    this.submit();
  }

  submit(reiniciar_pagina = 0) {
    if (reiniciar_pagina == 1) {
      this.pagina_actual = 1;
    }

    this.lista = [];
    this.cantidad = 0;
    this._baseService.lista('despachador', { "id_cotizacion": this.id }, ["id_despachador", "DESC"], this.pagina_actual).subscribe((response) => {
      if (response.resultado == "ACK") {
        let lista = response.lista;
        this.lista = lista;
        this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;


      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", 'Error interno al cargar despachos');
    });
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }



  reenviar(obj) {
    const activeModal = this._modalService.open(ReenviarModalComponent, {
      context: {
        modalHeader: 'Reenviar Email',
        id: obj.id_despachador,
        para: obj.para,
        copia: obj.copia,
      }
    });
    activeModal.onClose.subscribe(result => {
      if (result) {
        this.submit(1);
      } else {
        console.log("closed");
      }
    });
  }

  preview(obj) {
    this._router.navigate(['pages/despachador/preview/' + this.id + '/' + obj.id_despachador]);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}