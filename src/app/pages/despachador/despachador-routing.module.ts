import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DespachadorComponent } from './despachador.component';
import { DespachadorListaComponent } from './lista/despachador_lista.component';
import { PreviewDespachoComponent } from './preview/preview_despacho.component';
import { ReenviarModalComponent } from './modal/reenviar.modal';


const routes: Routes = [
  {
    path: '',
    component: DespachadorComponent,
    children: [
      {
        path: 'lista/:id',
        component: DespachadorListaComponent,
      },
      {
        path: 'preview/:id_cotizacion/:id',
        component: PreviewDespachoComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DespachadorRoutingModule { }

export const routedComponents = [
  DespachadorComponent,
  DespachadorListaComponent,
  PreviewDespachoComponent,
  ReenviarModalComponent
];
