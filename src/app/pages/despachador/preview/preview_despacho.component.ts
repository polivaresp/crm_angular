import { Component } from '@angular/core';

import '../ckeditor/ckeditor.loader';
import 'ckeditor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { BaseService } from '../../../services';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { EmailService } from '../../email/services/email.service';

@Component({
  selector: 'ngx-preview-despacho',
  templateUrl: './preview_despacho.component.html',
  providers: [BaseService, EmailService]
})
export class PreviewDespachoComponent {
  id_cotizacion: any;
  tags;
  tipos;
  editor_model: any;
  editor_config: { extraPlugins: string; height: string; };
  form: FormGroup;
  id: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _emailService: EmailService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
      this.id_cotizacion = params['id_cotizacion'];
    });
    this.form = fb.group({
      para: [{ value: '', disabled: true }, Validators.required],
      copia: { value: '', disabled: true },
      asunto: [{ value: '', disabled: true }, Validators.required],
    });
    this.editor_config = { extraPlugins: 'divarea', height: '400' };
    this.editor_model;
    this.tipos = [];
    this.tags = [];
  }

  ngOnInit() {
    this._baseService.lista_simple('tipo_email', {}, ['orden', 'asc']).subscribe(response => {
      if (response.resultado == "ACK") {
        this.tipos = response.lista;
      } else {
        console.log("error a cargar estado");
      }
    }, function (err) {
      this.showToast("error", "Error Interno", err);
    });
    this._baseService.ver('despachador', this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.form.controls['para'].setValue(response.modelo["para"]);
        this.form.controls['asunto'].setValue(response.modelo["asunto"]);
        this.form.controls['copia'].setValue(response.modelo["copia"]);
        this._baseService.ver('plantilla', response.modelo["id_template"]).subscribe(res => {
          if (response.resultado == "ACK") {
            this.cambio_tipo(res.modelo["id_tipo"]);
          } else {
            this.showToast("error", "Error", response.mensaje);
          }
        }, err => {
          this.showToast("error", "Error", response.mensaje);
        });
        this.editor_model = atob(response.modelo.body_email); //decode base64
        //this.showToast("success","Exito",response.mensaje);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  cambio_tipo(tipo) {
    this._emailService.tags({ id_tipo: tipo }).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.tags = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/despachador/lista/' + this.id_cotizacion]);
  }


  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}