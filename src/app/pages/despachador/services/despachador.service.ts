import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DespachadorService {

    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    reenviar_despacho(post, id): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + 'despachador/reenviar/' + id, post, { headers: headerss });
    }

    no_contacto(id_cotizacion, id_usuario): Observable<any> {
        let headerss = this.getHeaders();

        let post = { "id_cotizacion": id_cotizacion, "id_usuario": id_usuario };
        return this._http.post(this.base_url + 'despachador/no_contactado', post, { headers: headerss });
    }
}