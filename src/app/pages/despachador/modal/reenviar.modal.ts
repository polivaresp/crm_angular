import { Component } from '@angular/core';

import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import * as moment from 'moment';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from '../../usuario/services/usuario_service';
import { DespachadorService } from '../services/despachador.service';

@Component({
  selector: 'ngx-reenviar-modal',
  templateUrl: './reenviar.modal.html',
  providers: [UsuarioService, DespachadorService]
})
export class ReenviarModalComponent {

  user: any;
  cotizaciones;
  modalHeader: string;
  form: FormGroup;
  para;
  copia;
  id;

  data: any;


  constructor(private activeModal: NbDialogRef<ReenviarModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
    private _usuarioService: UsuarioService, private _despachadorService: DespachadorService) {
    this.form = fb.group({
      para: ['', Validators.required],
      copia: '',
      usuario: '',
    });
    this.data = {};
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
  }

  ngOnInit() {
    this.form.controls['para'].setValue(this.para);
    this.form.controls['copia'].setValue(this.copia);
    this.form.controls['usuario'].setValue(this.user.id_usuario);
  }

  reenviar_email() {

    let form = [];
    form["para"] = this.form.value.para;
    form["copia"] = this.form.value.copia;
    form["usuario"] = this.form.value.usuario;
    let formulario = {};
    Object.assign(formulario, form);

    this._despachadorService.reenviar_despacho(formulario, this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast('success', 'Reenvio Exitoso', response.mensaje);
        this.data = response;
        this.closeModal();
      } else {
        this.showToast('error', 'Error de Reenvio', response.mensaje);
      }
    }, err => {
      this.showToast('error', 'Error de Reenvio*', err);
    });
  }


  closeModal() {
    this.activeModal.close(this.data);
  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
