import { Component } from '@angular/core';

import * as moment from 'moment';
/* import * as md5 from 'md5'; */;

import { Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { UsuarioService } from '../../usuario/services/usuario_service';


@Component({
  selector: 'ngx-agregar-sucursal',
  templateUrl: './agregar_sucursal.component.html',
  providers: [BaseService, UsuarioService]
})
export class AgregarSucursalComponent {

  grupos: any[];
  public formSucursal: FormGroup;
  regiones;
  comunas;
  jefes;
  etiquetas: any;

  constructor(private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _usuarioService: UsuarioService) {
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.formSucursal = fb.group({
      id_grupo_sucursal: ['', Validators.required],
      glosa_sucursal: ['', Validators.required],
      direccion: '',
      id_region: ['', Validators.required],
      id_supervisor: ['', Validators.required],
      id_comuna: ['', Validators.required],
      activo: true,
      alias_excel: '',
    });
    this.regiones = [];
    this.comunas = [];
    this.grupos = [];
    this.jefes = [];
  }

  ngOnInit() {
    //cargar regiones
    this._baseService.lista_simple('region', {})
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.regiones = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });
    // grupos sucursales
    this._baseService.lista_simple('grupo_sucursal', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.grupos = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });
    this._usuarioService.lista_simple([7])
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.jefes = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });
  }

  cambiar_region() {
    let data = { id_region: this.formSucursal.value.id_region };
    this._baseService.lista_simple('comuna', data)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.comunas = response.lista;
        } else {
          console.log("error a cargar comuna");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err)
      });
  }


  submit() {
    this._baseService.agregar('sucursal', this.formSucursal.value).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/sucursal/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/sucursal/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}