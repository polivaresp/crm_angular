import { Component } from '@angular/core';

import * as moment from 'moment';


import { Router } from '@angular/router';

import { UsuarioService } from '../../usuario/services/usuario_service';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';

import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import { NbDialogService } from '@nebular/theme';


@Component({
  selector: 'ngx-sucursal-lista',
  templateUrl: './sucursal_lista.component.html',
  providers: [UsuarioService, BaseService]
})
export class SucursalListaComponent {

  grupos: any[];
  public user: any;
  public lista: Array<any>;
  anular_sucursal = [];
  habilitar_sucursal = [];
  acciones: any;
  etiquetas: any;

  previusPage: number;
  pagina_actual: number;
  cantidad: any;
  cantidad_pagina: any;

  constructor(private _router: Router, private _usuarioService: UsuarioService,
    private _toasterService: ToasterService, private _baseService: BaseService,
    private _modalService: NbDialogService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.acciones = JSON.parse(localStorage.getItem('acciones'));
    this.lista = [];
  }

  ngOnInit() {
    this._baseService.lista_simple('grupo_sucursal', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.grupos = response.lista;
          this.submit(1);
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });

  }

  ordenar(obj, lista) {
    const activeModal = this._modalService.open(OrdenModalComponent, {
      context: {
        modulo: 'sucursal',
        obj: obj
      }
    });
    activeModal.onClose.subscribe(res => {
      if (res) {
        this.submit();
      } else {
        console.log("closed")
      }
    });
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }

  getDate(fecha, format) {
    if (fecha) {
      if (fecha != '') {
        let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
        return x;
      }
    }
    return '-';
  }

  agregar() {
    this._router.navigate(['pages/sucursal/agregar']);
  }

  submit(reiniciar_pagina = 0) {
    this.lista = [];
    if (reiniciar_pagina == 1) {
      this.pagina_actual = 1;
    }
    this._baseService.lista('sucursal', {}, ["orden", "ASC"], this.pagina_actual).subscribe((response) => {
      if (response.resultado == "ACK") {
        let lista = response.lista;
        let lista_grupos = this.grupos;
        lista.forEach(function (e, i) {
          lista_grupos.forEach(function (element, index) {
            if (element.id == e.id_grupo_sucursal) {
              e.grupo_sucursal = element.glosa;
            }
          });
        });
        this.lista = lista;
        this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", 'Error interno al cargar sucursales');
    });
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

  editar(obj) {
    this._router.navigate(['pages/sucursal/editar/' + obj.id_sucursal]);
  }

  asignar(obj) {
    this._router.navigate(['pages/sucursal/asignar_ejecutivo/' + obj.id_sucursal]);
  }

  anular(obj) {
    this._baseService.desactivar('sucursal', obj.id_sucursal).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this.submit(1);
        this.anular_sucursal = [];
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  habilitar(obj) {
    this._baseService.habilitar('sucursal', obj.id_sucursal).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this.submit(1);
        this.habilitar_sucursal = [];
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

}