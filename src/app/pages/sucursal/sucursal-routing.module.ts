import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SucursalComponent } from './sucursal.component';
import { SucursalListaComponent } from './lista/sucursal_lista.component';
import { AgregarSucursalComponent } from './agregar/agregar_sucursal.component';
import { EditarSucursalComponent } from './editar/editar_sucursal.component';
import { AsignarEjecutivoComponent } from './asignar_ejecutivo/asignar_ejecutivo.component';

const routes: Routes = [
  {
    path: '',
    component: SucursalComponent,
    children: [
      {
        path: 'lista',
        component: SucursalListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarSucursalComponent,
      },
      {
        path: 'editar/:id',
        component: EditarSucursalComponent,
      },
      {
        path: 'asignar_ejecutivo/:id',
        component: AsignarEjecutivoComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SucursalRoutingModule { }

export const routedComponents = [
  SucursalComponent,
  SucursalListaComponent,
  AgregarSucursalComponent,
  EditarSucursalComponent,
  AsignarEjecutivoComponent
];
