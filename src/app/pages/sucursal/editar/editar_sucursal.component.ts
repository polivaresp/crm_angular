import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { UsuarioService } from '../../usuario/services/usuario_service';


@Component({
  selector: 'ngx-editar-sucursal',
  templateUrl: './editar_sucursal.component.html',
  providers: [BaseService, UsuarioService]
})
export class EditarSucursalComponent {

  grupos: any[];
  id: any;
  public formSucursal: FormGroup;
  regiones;
  comunas;
  jefes;
  etiquetas: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _usuarioService: UsuarioService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.formSucursal = fb.group({
      id_grupo_sucursal: ['', Validators.required],
      glosa_sucursal: ['', Validators.required],
      id_supervisor: ['', Validators.required],
      direccion: '',
      id_region: ['', Validators.required],
      id_comuna: ['', Validators.required],
      activo: true,
      alias_excel: '',
    });
    this.regiones = [];
    this.comunas = [];
    this.grupos = [];
  }

  ngOnInit() {
    //cargar regiones
    this._baseService.lista_simple('region', {})
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.regiones = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });
    // grupos sucursales
    this._baseService.lista_simple('grupo_sucursal', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.grupos = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });
    this._usuarioService.lista_simple([7])
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.jefes = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err);
      });
    this.cargar_form();
  }

  cargar_form() {
    this._baseService.ver('sucursal', this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.formSucursal.controls['id_grupo_sucursal'].setValue(response.modelo.id_grupo_sucursal ? response.modelo.id_grupo_sucursal : '');
        this.formSucursal.controls['glosa_sucursal'].setValue(response.modelo.glosa_sucursal);
        this.formSucursal.controls['id_supervisor'].setValue(response.modelo.id_supervisor ? response.modelo.id_supervisor : '');
        this.formSucursal.controls['id_region'].setValue(response.modelo.id_region);
        this.cambiar_region();
        this.formSucursal.controls['id_comuna'].setValue(response.modelo.id_comuna);
        this.formSucursal.controls['direccion'].setValue(response.modelo.direccion);
        this.formSucursal.controls['alias_excel'].setValue(response.modelo.alias_excel);
        this.formSucursal.controls['activo'].setValue(response.modelo.activo == 1 ? true : false);
      } else {
        this.showToast('error', 'Error Interno', response.mensaje);
      }
    }, error => {
      this.showToast('error', 'Error Interno', error);
    });
  }

  cambiar_region() {
    let data = { id_region: this.formSucursal.value.id_region };
    this._baseService.lista_simple('comuna', data)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.comunas = response.lista;
        } else {
          console.log("error a cargar comuna");
        }
      }, function (err) {
        this.showToast("error", "Error Interno", err)
      });
  }


  submit() {
    this._baseService.editar('sucursal', this.formSucursal.value, this.id).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/sucursal/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/sucursal/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}