import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { SucursalRoutingModule, routedComponents } from './sucursal-routing.module';
import { FilterPipe } from '../../services/filter.pipe';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    ThemeModule,
    NgbPaginationModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule,
    SucursalRoutingModule
  ],
  declarations: [
    ...routedComponents,
    FilterPipe
  ],
  providers: [],
})
export class SucursalModule { }
