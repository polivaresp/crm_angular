import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { UsuarioService } from '../../usuario/services/usuario_service';
import { SucursalService } from '../services/sucursal.service';


@Component({
  selector: 'ngx-asignar-ejecutivo',
  templateUrl: './asignar_ejecutivo.component.html',
  providers: [BaseService, UsuarioService, SucursalService]
})
export class AsignarEjecutivoComponent {

  sucursal: any;
  ejecutivos: any[];
  form: any[];
  id: any;
  searchText;
  etiquetas: any;

  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _usuarioService: UsuarioService, private _sucursalService: SucursalService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.form = [];
    this.ejecutivos = [];
    this.sucursal = {};
  }

  ngOnInit() {
    this.cargar_form();
    this.buscar();
  }

  cargar_form() {
    this._baseService.ver('sucursal', this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.sucursal = response.modelo;
      } else {
        this.showToast('error', 'Error Interno', response.mensaje);
      }
    }, error => {
      this.showToast('error', 'Error Interno', error);
    });
  }

  buscar() {
    this._sucursalService.lista_asignados(this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.ejecutivos = response.lista;
      } else {
        this.showToast('error', 'Error Interno', response.mensaje);
      }
    }, error => {
      this.showToast('error', 'Error Interno', error);
    });
  }

  submit() {
    this._sucursalService.asignar_ejecutivo(this.ejecutivos, this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this._router.navigate(['pages/sucursal/lista']);
      } else {
        this.showToast('error', 'Error Interno', response.mensaje);
      }
    }, error => {
      this.showToast('error', 'Error Interno', error);
    });

  }

  editar_ejecutivo(obj) {
    this._router.navigate(['pages/usuario/editar/' + obj.id_usuario]);
  }

  agregar_ejecutivo() {
    this._router.navigate(['pages/usuario/agregar']);
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/sucursal/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}