import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SucursalService {

    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista_asignados(id_sucursal): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + 'sucursal/lista_asignados/' + id_sucursal, data, { headers: headerss });

    }

    asignar_ejecutivo(lista, id_sucursal): Observable<any> {
        let headerss = this.getHeaders();
        let data = { ejecutivos: lista };

        return this._http.post(this.base_url + 'sucursal/asignar_ejecutivos/' + id_sucursal, data, { headers: headerss });

    }
}