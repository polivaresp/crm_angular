import { Component } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { VentasService } from "../../services/ventas_service";
import * as moment from 'moment';

@Component({
  selector: 'ngx-modal',
  templateUrl: './ver_agenda.modal.html',
  providers: [VentasService]
})
export class VerAgendaModalComponent {

  objeto: any;
  etiquetas: any;
  telefonos: any;
  emails: any;
  usuario;
  comentario: any;
  confirma_accion: boolean;


  constructor(private activeModal: NbDialogRef<VerAgendaModalComponent>, private _toasterService: ToasterService, private _ventaService: VentasService) {
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.comentario = "";
    this.confirma_accion = false;
  }

  ngOnInit() {
    this.telefonos = this.objeto.telefonos.split(',');
    this.emails = this.objeto.emails.split(',');
    this._ventaService.marcar_lectura(this.objeto, this.usuario).subscribe(res => {
      if (res.resultado == "ACK") {

      } else {
        this.showToast("error", "Error", res.mensaje);
      }
    }, error => {
      this.showToast("error", "Error Index", error.statusErr);
    });
  }

  confirmar_agenda(cv) {
    console.log(cv);
    this._ventaService.confirmar_visita(this.objeto, cv, this.comentario, this.usuario).subscribe(res => {
      if (res.resultado == "ACK") {
        this.showToast("success", "Guardado", res.mensaje);
        this.closeModal();
      } else {
        this.showToast("error", "Error", res.mensaje);
      }
    }, error => {
      this.showToast("error", "Error Index", error.statusErr);
    });
  }


  closeModal() {
    this.activeModal.close();
  }

  getDate(fecha, format) {
    if (fecha) {
      if (fecha != '') {
        let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
        return x;
      }
    }
    return '-';

  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
