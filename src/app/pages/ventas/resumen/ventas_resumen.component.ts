import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { VentasService } from "../services/ventas_service";
import { BaseService } from '../../../services/base.service';


@Component({
    selector: 'ngx-ventas-resumen',
    styleUrls: ['./ventas_resumen.component.scss'],
    templateUrl: './ventas_resumen.component.html',
    providers: [VentasService, BaseService] //aqui se importan los servicios
})
export class VentasResumenComponent {

    filtros;
    filtrado;
    ejecutivos;
    user: any;
    etiquetas: any;
    ejecutivo: string;
    periodos: any[];
    funnel: any;
    gestion: any;
    periodo_seleccionado: any;
    cargando: number;

    constructor(private _router: Router, private _toasterService: ToasterService,
        private _ventaService: VentasService, private _baseService: BaseService) {
        this.ejecutivos = [];
        this.funnel = { total_visitas: 0, total_asistieron: 0, total_compro: 0 };
        this.gestion = { total_visitas: 0, total_asistieron: 0, total_compro: 0 };
        this.periodos = [];
        this.filtros = [];
        this.filtros["id_ejecutivo"] = '';
        this.filtros["periodo"] = '';
        this.filtrado = 0;
        this.cargando = 0;
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    }

    ngOnInit() {
        this._ventaService.ejecutivos(this.user.id_usuario, this.user.id_perfil).subscribe(res => {
            if (res.resultado == "ACK") {
                this.ejecutivos = res.lista;
                this.filtros["id_ejecutivo"] = this.user.id_usuario ? this.user.id_usuario : '';
                //this.cambio_ejecutivo(this.user.id_usuario);
                this.submit();
            } else {
                this.showToast("error", "Error", res.mensaje);
            }
        }, error => {
            this.showToast("error", "Error Index", error.statusErr);
        });
        this._baseService.periodos("cotizacion").subscribe(response => {
            if (response.resultado == "ACK") {
                this.periodos = response.lista;
                this.submit();
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error Index", error.statusErr);
        });
    }

    submit(reinicio = 1) {
        this.cargando = 1;
        let ejecutivos = this.ejecutivos;
        let nombre = 'Todos';
        ejecutivos.forEach(element => {
            if (element.id_usuario == this.filtros["id_ejecutivo"]) {
                nombre = element.nombre_completo;
            }
        });
        this.ejecutivo = nombre;
        this.periodo_seleccionado = this.filtros["periodo"];

        let filtros = { "id_ejecutivo": this.filtros["id_ejecutivo"], "periodo": this.filtros["periodo"] };
        let data = { "id": this.user.id_usuario, "perfil": this.user.id_perfil };
        this._ventaService.resumen(filtros, data).subscribe(response => {
            if (response.resultado == "ACK") {
                this.funnel = response.funnel;
                this.gestion = response.gestion;
                this.cargando = 0;
            } else {
                this.showToast("error", "Error", response.mensaje);
                this.cargando = 0;
            }
        }, error => {
            this.showToast("error", "Error Index", error.statusErr);
            this.cargando = 0;
        });

    }

    limpiar_filtros() {
        if (this.user.id_perfil == 7) {
            this.filtros["id_ejecutivo"] = '';
        }
        this.filtros["periodo"] = '';
        this.submit();
    }

    private showToast(type: string, title: string, body: string) { //funcion para mostrar notificaciones de alertas
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}