import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class VentasService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    ejecutivos(id, id_perfil): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id: id, perfil: id_perfil };

        return this._http.post(this.base_url + 'ventas/ejecutivos', data, { headers: headerss });

    }

    agendamientos(filtros, datos, pagina_actual): Observable<any> {
        let headerss = this.getHeaders();
        let data = { filtros: filtros, post: datos };

        return this._http.post(this.base_url + 'ventas/agendamientos/' + pagina_actual, data, { headers: headerss });

    }

    resumen(filtros, datos): Observable<any> {
        let headerss = this.getHeaders();
        let data = { filtros: filtros, post: datos };

        return this._http.post(this.base_url + 'ventas/resumen', data, { headers: headerss });

    }

    marcar_lectura(obj, user): Observable<any> {
        let headerss = this.getHeaders();
        let data = { cotizacion: obj, usuario: user };

        return this._http.post(this.base_url + 'ventas/marcar_lectura', data, { headers: headerss });

    }

    confirmar_visita(obj, cv, comentario, user): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'cotizacion': obj, 'confirmacion': { 'comentario': comentario, 'cv': cv }, 'usuario': user };

        return this._http.post(this.base_url + 'ventas/confirmar_visita', data, { headers: headerss });

    }

}