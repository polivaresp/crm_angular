import { Component } from '@angular/core';

@Component({
  selector: 'ngx-ventas',
  template: `<router-outlet></router-outlet>`
})
export class VentasComponent {
}
