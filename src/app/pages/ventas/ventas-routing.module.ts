import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VentasComponent } from './ventas.component';
import { VentasListaComponent } from './lista/ventas_lista.component';
import { VerAgendaModalComponent } from './modals/ver_agenda/ver_agenda.modal';
import { VentasResumenComponent } from './resumen/ventas_resumen.component';

const routes: Routes = [
  {
    path: '',
    component: VentasComponent,
    children: [
      {
        path: 'lista',
        component: VentasListaComponent,
      },
      {
        path: 'resumen',
        component: VentasResumenComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VentasRoutingModule { }

export const routedComponents = [
  VentasComponent,
  VentasListaComponent,
  VerAgendaModalComponent,
  VentasResumenComponent
];
