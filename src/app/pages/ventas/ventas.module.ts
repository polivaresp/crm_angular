import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { VentasRoutingModule, routedComponents } from './ventas-routing.module';
import { VerAgendaModalComponent } from './modals/ver_agenda/ver_agenda.modal';
import { NgbPaginationModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    ThemeModule,
    VentasRoutingModule,
    NgbPaginationModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule,
    NgbDatepickerModule,
    NbDialogModule.forChild()
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
  entryComponents: [
    VerAgendaModalComponent
  ]
})
export class VentasModule { }
