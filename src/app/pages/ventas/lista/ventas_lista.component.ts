import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { VentasService } from '../services/ventas_service';
import { VerAgendaModalComponent } from '../modals/ver_agenda/ver_agenda.modal';
import { NbDialogService } from '@nebular/theme';
import * as moment from 'moment';

@Component({
    selector: 'ngx-ventas-lista',
    styleUrls: ['./ventas_lista.component.scss'],
    templateUrl: './ventas_lista.component.html',
    providers: [VentasService] //aqui se importan los servicios
})
export class VentasListaComponent {

    lista;
    filtros;
    filtrado;
    ejecutivos;
    now;
    minDate: { year: any; month: any; day: any; };
    maxDate: { year: any; month: any; day: any; };
    user: any;
    fecha_desde: { year: any; month: any; day: any; };
    fecha_hasta: { year: any; month: any; day: any; };
    pagina_actual: any;
    cantidad: number;


    hoy: any;
    rango: any;
    semana: any;
    mes: any;
    etiquetas: any;
    ejecutivo: string;
    fecha_sel_desde: any;
    fecha_sel_hasta: any;
    cargando: number;
    previusPage: number;
    cantidad_pagina: any;

    constructor(private _route: ActivatedRoute, private _router: Router, private _toasterService: ToasterService,
        private _ventaService: VentasService, private _modalService: NbDialogService) {
        this.lista = [];
        this.pagina_actual = 1;

        this.ejecutivos = [];
        this.hoy = 0;
        this.rango = 0;
        this.semana = 0;
        this.mes = 0;
        this.now = new Date();
        //console.log(this.now.getDate());
        /* this.minDate = { year: '', month: '', day: '' };
        this.maxDate = { year: '', month: '', day: '' }; */
        this.filtros = [];
        this.filtros["id_ejecutivo"] = '';
        this.filtros["rut"] = '';
        this.filtros["prospecto"] = '';
        this.filtros["id_etapa"] = '';
        this.filtrado = 0;
        this.fecha_desde = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        /* let last_day = new Date(this.now.getFullYear(), this.now.getMonth() + 1, 0); */
        this.fecha_hasta = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        this.cambio_fecha_desde();
        this.cambio_fecha_hasta();
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.cargando = 0;
    }

    ngOnInit() {
        this._ventaService.ejecutivos(this.user.id_usuario, this.user.id_perfil).subscribe(res => {
            if (res.resultado == "ACK") {
                this.ejecutivos = res.lista;
                this.filtros["id_ejecutivo"] = this.user.id_usuario ? this.user.id_usuario : '';
                //this.cambio_ejecutivo(this.user.id_usuario);
                this.submit(1);
            } else {
                this.showToast("error", "Error", res.mensaje);
            }
        }, error => {
            this.showToast("error", "Error Index", error.statusErr);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    /* cambio_ejecutivo(ejecutivo = 0){
        let e = (document.getElementById("id_ejecutivo")) as HTMLSelectElement;
        let a = this.ejecutivos.findIndex(
            eje => eje.id_usuario == ejecutivo
        )
        let sel = e.selectedIndex;
        let seleccion = a > 0 ? a : sel;
        console.log(seleccion);
        let opt = this.ejecutivos[seleccion];
        console.log(opt);
        let curText = (opt).nombre_completo;
        let curValue = (opt).id_usuario;
        console.log(curText);
        this.ejecutivo = curText;
    } */

    ver_hoy() {
        this.fecha_desde = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        this.fecha_hasta = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        this.cambio_fecha_desde();
        this.cambio_fecha_hasta();
        this.submit(1);
    }

    ver_semana() {
        let dt = new Date(); // current date of week
        let currentWeekDay = dt.getDay();
        let lessDays = currentWeekDay == 0 ? 6 : currentWeekDay - 1;
        let wkStart = new Date(new Date(dt).setDate(dt.getDate() - lessDays));
        let wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate() + 6));

        this.fecha_desde = { year: wkStart.getFullYear(), month: wkStart.getMonth() + 1, day: wkStart.getDate() };
        this.fecha_hasta = { year: wkEnd.getFullYear(), month: wkEnd.getMonth() + 1, day: wkEnd.getDate() };
        this.cambio_fecha_desde();
        this.cambio_fecha_hasta();
        this.submit(1);
    }

    ver_mes() {
        let last_day = new Date(this.now.getFullYear(), this.now.getMonth() + 1, 0);
        this.fecha_desde = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: 1 };
        this.fecha_hasta = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: last_day.getDate() };
        this.cambio_fecha_desde();
        this.cambio_fecha_hasta();
        this.submit(1);
    }

    submit(reiniciar_pagina = 0) {
        this.cargando = 1;
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this.lista = [];
        let ejecutivos = this.ejecutivos;
        let nombre = 'Todos';
        ejecutivos.forEach(element => {
            if (element.id_usuario == this.filtros["id_ejecutivo"]) {
                nombre = element.nombre_completo;
            }
        });
        this.ejecutivo = nombre;
        this.fecha_sel_desde = this.filtros["fecha_desde"];
        this.fecha_sel_hasta = this.filtros["fecha_hasta"];
        let filtros = {
            "id_ejecutivo": this.filtros["id_ejecutivo"], "fecha_desde": this.filtros["fecha_desde"],
            "fecha_hasta": this.filtros["fecha_hasta"], "rut": this.filtros["rut"],
            "nombre_completo": this.filtros["prospecto"], "id_etapa": this.filtros["id_etapa"]
        };
        let data = { "id": this.user.id_usuario, "perfil": this.user.id_perfil };
        this._ventaService.agendamientos(filtros, data, this.pagina_actual).subscribe(res => {
            if (res.resultado == "ACK") {
                this.hoy = res.hoy;
                this.semana = res.semana;
                this.mes = res.mes;
                this.cantidad = res.lista.length > 0 ? res.cantidad : 0;
                this.lista = res.lista.length > 0 ? res.lista : [];
                this.cantidad_pagina = res.cantidad_pagina > 0 ? res.cantidad_pagina : 10;
                this.cargando = 0;
            } else {
                this.showToast("error", "Error", res.mensaje);
                this.cargando = 0;
            }
        }, error => {
            this.showToast("error", "Error Index", error.statusErr);
            this.cargando = 0;
        });
    }

    onDateSelected() {
        console.log("cambio Select");
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    cambio_fecha_desde(event = null, a = null) {
        let fecha = event ? event : this.fecha_desde;
        //console.log(fecha);
        if (fecha.year != "") {
            //console.log("cambio");
            this.filtros["fecha_desde"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
        } else {
            //console.log("no cambio");
            this.filtros["fecha_desde"] = '';
        }
    }

    cambio_fecha_hasta(event = null, a = null) {
        let fecha = event ? event : this.fecha_hasta;
        //console.log(fecha);
        if (fecha.year != "") {
            //console.log("cambio");
            this.filtros["fecha_hasta"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
        } else {
            //console.log("no cambio");
            this.filtros["fecha_hasta"] = '';
        }
    }

    limpiar_filtros() {
        if (this.user.id_perfil == 7) {
            this.filtros["id_ejecutivo"] = '';
        }
        this.filtros["rut"] = '';
        this.filtros["prospecto"] = '';
        this.filtros["id_etapa"] = '';
        this.fecha_desde = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        //let last_day = new Date(this.now.getFullYear(), this.now.getMonth() + 1, 0);
        this.fecha_hasta = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        this.cambio_fecha_desde();
        this.cambio_fecha_hasta();
        this.submit(1);
    }

    ver_agenda(obj) {
        const activeModal = this._modalService.open(VerAgendaModalComponent, {
            context: {
                objeto: obj,
                usuario: this.user
            }
        });
        activeModal.onClose.subscribe(result => {
            this.submit(1);
            console.log(result);
        }, (reason) => {
            console.log(reason);
        });
    }

    private showToast(type: string, title: string, body: string) { //funcion para mostrar notificaciones de alertas
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}