import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ItemService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    nivel1(): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + "item/nivel1", data, { headers: headerss });

    }

    nivel2(nivel1): Observable<any> {
        let headerss = this.getHeaders();
        let data = { nivel1: nivel1 };

        return this._http.post(this.base_url + "item/nivel2", data, { headers: headerss });

    }
}