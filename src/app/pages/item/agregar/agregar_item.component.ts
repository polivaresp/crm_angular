import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { ItemService } from '../services/item.service';


@Component({
  selector: 'ngx-agregar-item',
  templateUrl: './agregar_item.component.html',
  providers: [BaseService, ItemService]
})
export class AgregarItemComponent {


  form: FormGroup;
  items1: any[];
  items2: any[];
  etiquetas: any;
  /* plantillas: any[]; */
  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _itemService: ItemService) {
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.form = fb.group({
      glosa_1: ['', Validators.required],
      glosa_2: '',
      glosa_3: '',
      alias_excel: '',
      activo: true,
      firma: '',
      /* id_plantilla:'' */
    });
    this.items1 = [];
    this.items2 = [];
  }

  ngOnInit() {
    this._itemService.nivel1().subscribe((response) => {
      if (response.resultado == "ACK") {
        this.items1 = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    });
    /* this._baseService.lista_simple('plantilla',{id_tipo:8},['orden', 'asc']).subscribe(response => {
      if (response.resultado == "ACK") {
          this.plantillas = response.lista;
      } else {
          console.log("error a cargar plantilla");
      }
    }, err => {
        this.showToast("error","Error Interno",err);
    }); */
  }

  nivel2() {
    this._itemService.nivel2(this.form.value.glosa_1).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.items2 = response.lista;
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  submit() {

    let form = [];
    form["glosa_item"] = this.form.value.glosa_1;
    if (this.form.value.glosa_2 != '') {
      form["glosa_item"] += ">>>" + this.form.value.glosa_2;
      if (this.form.value.glosa_3 != '') {
        form["glosa_item"] += ">>>" + this.form.value.glosa_3
      }
    }
    form["activo"] = this.form.value.activo;
    form["alias_excel"] = this.form.value.alias_excel;
    form["firma"] = this.form.value.firma;
    /* form["id_plantilla"] = this.form.value.id_plantilla; */
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.agregar('item', formulario).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/item/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/item/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}