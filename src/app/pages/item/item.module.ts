import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ItemRoutingModule, routedComponents } from './item-routing.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    NgbPaginationModule,
    NbCardModule,
    ItemRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class ItemModule { }
