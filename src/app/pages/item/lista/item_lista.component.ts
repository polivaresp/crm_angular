import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';

import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import * as moment from 'moment';
import { NbDialogService } from '@nebular/theme';

@Component({
    selector: 'ngx-item-lista',
    templateUrl: './item_lista.component.html',
    providers: [BaseService]
})
export class ItemListaComponent {

    cantidad: number;
    item;


    pagina_actual: number;
    lista;
    items;
    anular_item = [];
    habilitar_item = [];
    acciones: any;
    etiquetas: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _modalService: NbDialogService) {
        this.lista = [];

        this.pagina_actual = 1;

        this.cantidad = 0;
        this.item = "";
        this.items = [];
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    }

    ngOnInit() {
        this.submit(1);
    }

    anular(obj) {
        this._baseService.desactivar('item', obj.id_item).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_item = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar('item', obj.id_item).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_item = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/item/editar/' + obj.id_item]);
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'item',
                obj: obj
            }
        });

        activeModal.onClose.subscribe(result => {
            if (result) {
                this.submit();
            } else {
                console.log("closed");
            }
        });
    }

    agregar() {
        this._router.navigate(['pages/item/agregar']);
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._baseService.lista('item', {}, ["orden", "ASC"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {

                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;

            } else {
                //                this.showToast("error","Error",response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}