import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemComponent } from './item.component';
import { ItemListaComponent } from './lista/item_lista.component';
import { EditarItemComponent } from './editar/editar_item.component';
import { AgregarItemComponent } from './agregar/agregar_item.component';


const routes: Routes = [
  {
    path: '',
    component: ItemComponent,
    children: [
      {
        path: 'lista',
        component: ItemListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarItemComponent,
      },
      {
        path: 'editar/:id',
        component: EditarItemComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ItemRoutingModule { }

export const routedComponents = [
  ItemComponent,
  ItemListaComponent,
  EditarItemComponent,
  AgregarItemComponent
];
