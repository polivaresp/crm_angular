import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
    selector: 'ngx-gestion-lista',
    templateUrl: './gestion_lista.component.html',
})
export class GestionListaComponent {

    titulo = "Gestion Lista";
    id_cotizacion;
    id_prospecto;

    constructor(private _route: ActivatedRoute, private _router: Router) {

    }

    ngOnInit() {
        this._route.params.forEach((params: Params) => {
            this.id_prospecto = params['id_prospecto'];
            this.id_cotizacion = params['id_cotizacion'];
        });
    }

}
