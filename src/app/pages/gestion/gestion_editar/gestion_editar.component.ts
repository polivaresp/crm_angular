import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BaseService } from '../../../services/index';
import { CotizacionService } from '../../cotizacion/services/cotizacion.service';
import { GestionService } from '../services/gestion.service';
import * as moment from 'moment';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ProspectoService } from '../../prospecto/services/prospecto.service';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { GestionListaModalComponent } from '../modals/gestion_lista.modal'
import { BloqueHorarioModalComponent } from '../modals/bloque_horario.modal';
import { DuplicadasModalComponent } from '../../cotizacion/modals/duplicadas/duplicadas.modal';
import { NbDialogService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'ngx-gestion-editar',
    templateUrl: './gestion_editar.component.html',
    providers: [BaseService, CotizacionService, GestionService, ProspectoService]
})
export class GestionEditarComponent {

    estados_completos: any;
    fecha_reserva: any;
    fecha_ultima_compra: any;
    fecha_inicio_gestion: Date;
    fecha_agendamiento_visita;
    fecha_minima;
    user;


    titulo = "Gestion Editar";
    cotizacion;
    id_cotizacion;
    id_prospecto;
    origenes: Array<any>;
    items: Array<any>;
    sucursales: Array<any>;
    estados: Array<any>;
    medios_contacto: Array<any>;
    motivos: Array<any>;
    regiones_prospecto: Array<any>;
    comunas_prospecto: Array<any>;
    regiones: Array<any>;
    comunas: Array<any>;
    ejecutivos: Array<any>;
    compro: Array<any>;
    form_compro: Array<any>;
    btnDisabled: boolean;

    formGestion: FormGroup;
    fecha_agendamiento;
    fecha_no_molestar;
    fecha_recordatorio;
    nombre_ejecutivo;
    formProspecto: FormGroup;
    telefonos_prospecto;
    emails_prospecto: Array<any>;

    camposvariables;

    tieneRecordatorio;
    etiquetas: any;
    duplicadas;
    info_entregada: boolean;
    max_hoy: { year: number; month: number; day: number; };
    hora_agenda;

    constructor(private _route: ActivatedRoute, private _router: Router, private location: Location,
        private _baseService: BaseService, private _cotizacionService: CotizacionService,
        private _gestionService: GestionService, private _prospectoService: ProspectoService,
        private _modalMaterialService: NgbModal, private fb: FormBuilder,
        private _modalService: NbDialogService, private _toasterService: ToasterService) {
        this.cotizacion = { id_estado: '', id_region: '', id_comuna: '', id_sucursal: '' };
        this.origenes = [];
        this.items = [];
        this.sucursales = [];
        this.estados = [];
        this.medios_contacto = [];
        this.motivos = [];
        this.regiones_prospecto = [];
        this.comunas_prospecto = [];
        this.regiones = [];
        this.comunas = [];
        this.ejecutivos = [];
        this.compro = [];
        this.form_compro = [];
        this.camposvariables = [];
        this.duplicadas = [];

        this.telefonos_prospecto = [];
        this.emails_prospecto = [];
        this._route.params.forEach((params: Params) => {
            this.id_prospecto = params['id_prospecto'];
            this.id_cotizacion = params['id_cotizacion'];
        });
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.formGestion = fb.group({
            id_cotizacion: this.id_cotizacion,
            fecha_inicio_gestion: moment().format('YYYY-MM-DD HH:mm:ss'),
            fecha_termino_gestion: '',
            id_usuario: this.user.id_usuario,
            id_item: ['', Validators.required],
            id_origen: ['', Validators.required],
            id_estado: '',
            comentario: '',
            antecedentes: '',
            id_medio_contacto: ['', Validators.required],
            agendar: ['', Validators.required],
            id_motivo: '',
            id_compro: '',
            id_visita: '',
            fecha_agendamiento: '',
            fecha_agendamiento_visita: '',
            id_region: '',
            id_comuna: '',
            id_sucursal: '',
            id_ejecutivo: '',
            id_recordatorio: '',
            bloque: '',
            generar_cotizacion: false,
            correo_ejecutivo: true,
            fecha_ultima_compra: '',
            fecha_reserva: '',
            informacion_entregada: '',
        });
        this.formProspecto = fb.group({
            rut: ['', Validators.required],
            nombre: ['', Validators.required],
            apellido_paterno: ['', Validators.required],
            apellido_materno: '',
            id_region: '',
            id_comuna: '',
            direccion: '',
            emails: this.emails_prospecto,
            telefonos: '',
            dato_0: '',
            dato_1: '',
            dato_2: '',
            dato_3: '',
            dato_4: '',
            dato_5: '',
            dato_6: '',
            dato_7: '',
            dato_8: '',
            dato_9: '',
        });
        this.fecha_inicio_gestion = new Date();
        this.max_hoy = { year: this.fecha_inicio_gestion.getFullYear(), month: this.fecha_inicio_gestion.getMonth() + 1, day: this.fecha_inicio_gestion.getDate() };
        this.fecha_minima = new Date();
        this.fecha_minima.setDate(this.fecha_minima.getDate() - 1);
        this.info_entregada = false;
        this.hora_agenda = { hour: this.fecha_inicio_gestion.getHours() + 1, minute: 0, seconds: 0 };
    }

    ngOnInit() {
        this._cotizacionService.ver(this.id_cotizacion, {}).subscribe(res => {
            if (res.resultado == "ACK") {
                this.cotizacion = res.cotizacion;
                this.pre_cargar_selectores();
                this.cargar_formularios();
            } else {
                this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
                location.reload();
                this.showToast("error", "Error", "Cotizacion no encontrada");
            }
        }, error => {
            this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
            location.reload();
            this.showToast("error", "Error", "Error interno al cargar cotizacion");
        });
        this._cotizacionService.lista_duplicadas(this.id_cotizacion, this.id_prospecto).subscribe(res => {
            if (res.resultado == "ACK") {
                this.duplicadas = res.lista;
            } else {
                this.showToast("error", "Error", "Duplicadas no encontrada");
            }
        }, error => {
            this.showToast("error", "Error", "Error interno al cargar duplicadas");
        });
        this.formGestion.get('id_visita').valueChanges.subscribe((confirma) => {
            let aux = [];
            if (confirma == 1) {
                let array_estados = [9, 12, 15, 17, 19];
                this.estados_completos.forEach(function (ele, i) {
                    if (array_estados.indexOf(ele.id) >= 0) {
                        aux.push(ele);
                    }
                });
            }
            if (confirma == 2) {
                let array_estados = [3, 4, 7, 12, 14];
                this.estados_completos.forEach(function (ele, i) {
                    if (array_estados.indexOf(ele.id) >= 0) {
                        aux.push(ele);
                    }
                });
            }
            this.estados = aux;
            this.formGestion.controls['id_estado'].setValue('');
            if (confirma == 1 || confirma == 2) {
                this.formGestion.get('id_estado').setValidators([Validators.required]);
                this.formGestion.get('id_estado').updateValueAndValidity();
            } else {
                this.formGestion.get('id_estado').setValidators([]);
                this.formGestion.get('id_estado').updateValueAndValidity();
            }
        });
        this.formGestion.get('id_estado').valueChanges.subscribe((estado) => {
            this.formGestion.controls['agendar'].setValue('');
            this.formGestion.controls['bloque'].setValue('');
            this.formGestion.controls['fecha_agendamiento'].setValue('');
            this.formGestion.controls['fecha_agendamiento_visita'].setValue('');
            this.formGestion.controls['id_motivo'].setValue('');
            this.formGestion.controls['id_compro'].setValue('');
            this.formGestion.controls['id_region'].setValue('');
            this.formGestion.controls['id_comuna'].setValue('');
            this.formGestion.controls['id_sucursal'].setValue('');
            this.formGestion.controls['id_ejecutivo'].setValue('');
            this.formGestion.controls['fecha_ultima_compra'].setValue('');
            this.formGestion.controls['fecha_reserva'].setValue('');
            this.form_compro = [];
            this.compro = [];
            this.formGestion.get('bloque').setValidators([]);
            this.formGestion.get('fecha_agendamiento').setValidators([]);
            this.formGestion.get('fecha_agendamiento_visita').setValidators([]);
            this.formGestion.get('id_motivo').setValidators([]);
            this.formGestion.get('id_compro').setValidators([]);
            this.formGestion.get('id_region').setValidators([]);
            this.formGestion.get('id_comuna').setValidators([]);
            this.formGestion.get('id_sucursal').setValidators([]);
            this.formGestion.get('antecedentes').setValidators([]);
            this.formGestion.get('id_ejecutivo').setValidators([]);
            this.formGestion.get('fecha_ultima_compra').setValidators([]);
            this.formGestion.get('fecha_reserva').setValidators([]);
            if (estado == 7 || estado == 10 || estado == 12 || estado == 13 || estado == 15) {
                //cargar motivo
                this._baseService.lista_simple('motivo', { id_estado: estado })
                    .subscribe(response => {
                        if (response.resultado == "ACK") {
                            this.motivos = response.lista;
                        } else {
                            this.showToast("error", "Error", "error a cargar motivos");
                        }
                    }, (err) => {
                        this.showToast("error", "Error", "motivos")
                    });
                this.formGestion.get('id_motivo').setValidators([Validators.required]);

            } else if (estado == 4 || estado == 14) {
                this.formGestion.get('id_region').setValidators([Validators.required]);
                this.formGestion.get('id_comuna').setValidators([Validators.required]);
                this.formGestion.get('id_sucursal').setValidators([Validators.required]);
                this.formGestion.get('id_ejecutivo').setValidators([Validators.required]);
                this.formGestion.get('fecha_agendamiento_visita').setValidators([Validators.required]);
                this.formGestion.controls["agendar"].setValue('no_agendar');
            } else if (estado == 9) {
                this._baseService.lista_simple('compro', { id_padre: null })
                    .subscribe(response => {
                        if (response.resultado == "ACK") {
                            this.compro.push(response.lista);
                            this.form_compro[0] = "";
                        } else {
                            this.showToast("error", "Error", "error a cargar compro");
                        }
                    }, (err) => {
                        this.showToast("error", "Error", "compro");
                    });
                this.formGestion.get('id_compro').setValidators([Validators.required]);
                this.formGestion.get('fecha_ultima_compra').setValidators([Validators.required]);

            } else if (estado == 2 || estado == 3 || estado == 7) {
                this.formGestion.get('fecha_agendamiento').setValidators([Validators.required]);
            } else if (estado == 17) {
                this.formGestion.get('fecha_reserva').setValidators([Validators.required]);
            } else if (estado == 4 || estado == 13 || estado == 15) {
                this.formGestion.get('antecedentes').setValidators([Validators.required]);
            } else if (estado == 16 || estado == 15) {
                this.formGestion.controls["agendar"].setValue('no_agendar');
            }
            //actualiza validadores
            this.formGestion.get('bloque').updateValueAndValidity();
            this.formGestion.get('antecedentes').updateValueAndValidity();
            this.formGestion.get('antecedentes').updateValueAndValidity();
            this.formGestion.get('id_motivo').updateValueAndValidity();
            this.formGestion.get('id_region').updateValueAndValidity();
            this.formGestion.get('id_comuna').updateValueAndValidity();
            this.formGestion.get('id_sucursal').updateValueAndValidity();
            this.formGestion.get('id_ejecutivo').updateValueAndValidity();
            this.formGestion.get('id_compro').updateValueAndValidity();
            this.formGestion.get('fecha_agendamiento_visita').updateValueAndValidity();
            this.formGestion.get('fecha_agendamiento').updateValueAndValidity();
            this.formGestion.get('fecha_reserva').updateValueAndValidity();
            this.formGestion.get('fecha_ultima_compra').updateValueAndValidity();

        });
        window.scrollTo(0, 0);
    }

    asesorado() {
        if (!this.info_entregada) {
            this.formGestion.controls['informacion_entregada'].setValue('');
        } else {
            this.formGestion.controls['informacion_entregada'].setValue(moment().format("YYYY-MM-DD HH:mm:ss"));
        }
    }

    // Selectores y pre inicialización de datos

    cargar_formularios() {
        this._baseService.ver('cotizacion', this.id_cotizacion).subscribe(res => {
            if (res.resultado == "ACK") {
                let cot = res.modelo;
                this.formGestion.controls['id_origen'].setValue(cot.id_origen ? cot.id_origen : '');
                this.formGestion.controls['id_item'].setValue(cot.id_item ? cot.id_item : '');
                this.formGestion.controls['antecedentes'].setValue(cot.antecedentes ? cot.antecedentes : '');
                this.formGestion.controls['id_medio_contacto'].setValue(cot.id_medio_contacto ? cot.id_medio_contacto : '');
                this.formGestion.controls['id_region'].setValue(cot.id_region ? cot.id_region : '');
                this.formGestion.controls['informacion_entregada'].setValue(cot.informacion_entregada ? cot.informacion_entregada : '');
                this.cambio_region_cot();
                this.formGestion.controls['id_comuna'].setValue(cot.id_comuna ? cot.id_comuna : '');
                this.cambio_comuna_cot();
                this.formGestion.controls['id_sucursal'].setValue(cot.id_sucursal ? cot.id_sucursal : '');
            } else {
                this.showToast("error", "Error", "Error al cargar cotizacion para formulario");
                this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
                location.reload();
            }
        }, error => {
            this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
            location.reload();
            this.showToast("error", "Error", "Error interno al cargar cotizacion");
        });
        this._baseService.ver('prospecto', this.id_prospecto).subscribe(res => {
            if (res.resultado == "ACK") {
                let pros = res.modelo;
                this.formProspecto.controls['rut'].setValue(pros.rut ? pros.rut : "");
                this.formProspecto.controls['nombre'].setValue(pros.nombre ? pros.nombre : "");
                this.formProspecto.controls['apellido_paterno'].setValue(pros.apellido_paterno ? pros.apellido_paterno : "");
                this.formProspecto.controls['apellido_materno'].setValue(pros.apellido_materno ? pros.apellido_materno : "");
                this.formProspecto.controls['id_region'].setValue(pros.id_region ? pros.id_region : "");
                this.cambio_region_prospecto();
                this.formProspecto.controls['id_comuna'].setValue(pros.id_comuna ? pros.id_comuna : "");
                this.formProspecto.controls['direccion'].setValue(pros.direccion ? pros.direccion : "");
                this.formProspecto.controls['dato_0'].setValue(pros.dato_0 ? pros.dato_0 : "");
                this.formProspecto.controls['dato_1'].setValue(pros.dato_1 ? pros.dato_1 : "");
                this.formProspecto.controls['dato_2'].setValue(pros.dato_2 ? pros.dato_2 : "");
                this.formProspecto.controls['dato_3'].setValue(pros.dato_3 ? pros.dato_3 : "");
                this.formProspecto.controls['dato_4'].setValue(pros.dato_4 ? pros.dato_4 : "");
                this.formProspecto.controls['dato_5'].setValue(pros.dato_5 ? pros.dato_5 : "");
                this.formProspecto.controls['dato_6'].setValue(pros.dato_6 ? pros.dato_6 : "");
                this.formProspecto.controls['dato_7'].setValue(pros.dato_7 ? pros.dato_7 : "");
                this.formProspecto.controls['dato_8'].setValue(pros.dato_8 ? pros.dato_8 : "");
                this.formProspecto.controls['dato_9'].setValue(pros.dato_9 ? pros.dato_9 : "");


                this._baseService.lista_simple('telefonoprospecto', { id_prospecto: this.id_prospecto, activo: 1 })
                    .subscribe(response => {
                        if (response.resultado == "ACK") {
                            let arraytelefonos = [];
                            response.lista.forEach(function (e, i) {
                                arraytelefonos.push({ glosa: e.glosa, id: e.id, activo: e.activo });
                            });
                            this.telefonos_prospecto = arraytelefonos
                        } else {
                            this.showToast("error", "Error", "error al cargar telefonos prospecto");
                        }
                    }, (err) => {
                        this.showToast("error", "Error", "telefonos")
                    });
                this._baseService.lista_simple('emailprospecto', { id_prospecto: this.id_prospecto, activo: 1 })
                    .subscribe(response => {
                        if (response.resultado == "ACK") {
                            let arrayemails = [];
                            response.lista.forEach(function (e, i) {
                                arrayemails.push({ glosa: e.glosa, id: e.id, activo: e.activo });
                            });
                            this.emails_prospecto = arrayemails;
                        } else {
                            this.showToast("error", "Error", "error emails prospecto");
                        }
                    }, (err) => {
                        this.showToast("error", "Error", "emails")
                    });
            } else {
                this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
                location.reload();
                this.showToast("error", "Error", "Error al cargar prospecto para formulario");
            }
        }, error => {
            this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
            location.reload();
            this.showToast("error", "Error", "Error interno para cargar prospecto");
        });
    }

    pre_cargar_selectores() {
        //cargar camposvariables
        this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.camposvariables = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar camposvariables");
                }
            }, (err) => {
                this.showToast("error", "Error", "camposvariables")
            });
        //cargar origen
        this._baseService.lista_simple('origen', { activo: 1 })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.origenes = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar origen");
                }
            }, (err) => {
                this.showToast("error", "Error", "origen")
            });
        //cargar item
        this._baseService.lista_simple('item', { activo: 1 }, ["glosa_item", "asc"])
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.items = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar item");
                }
            }, (err) => {
                this.showToast("error", "Error", "item")
            });
        //cargar medio_contacto
        this._baseService.lista_simple('medio_contacto', { activo: 1 })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.medios_contacto = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar medio_contacto");
                }
            }, (err) => {
                this.showToast("error", "Error", "medio_contacto")
            });
        //cargar estados
        this._gestionService.estados(this.cotizacion.id_estado)
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.estados = response.lista;
                    this.estados_completos = this.estados;
                } else {
                    this.showToast("error", "Error", "error a cargar estado");
                }
            }, (err) => {
                this.showToast("error", "Error", "estados")
            });
        //cargar region
        this._baseService.lista_simple('region_sucursal', {})
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.regiones = response.lista;
                    this.formGestion.controls['id_region'].setValue(this.cotizacion.id_region ? this.cotizacion.id_region : '');
                    this.cambio_region_cot();
                } else {
                    this.showToast("error", "Error", "error a cargar regiones");
                }
            }, (err) => {
                this.showToast("error", "Error", "region")
            });
        this._baseService.lista_simple('region', {})
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.regiones_prospecto = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar regiones");
                }
            }, (err) => {
                this.showToast("error", "Error", "region")
            });
    }

    //fechas
    cambio_agendar() {
        this.formGestion.controls['fecha_agendamiento'].setValue('');
        if (this.formGestion.value.agendar == "en_30_min") {
            this.formGestion.controls['fecha_agendamiento'].setValue(moment().add(30, 'minutes').format("YYYY-MM-DD HH:mm:00"));
        } else if (this.formGestion.value.agendar == "en_una_hora") {
            this.formGestion.controls['fecha_agendamiento'].setValue(moment().add(1, 'hours').format("YYYY-MM-DD HH:mm:00"));
        } else if (this.formGestion.value.agendar == "mañana") {
            this.formGestion.controls['fecha_agendamiento'].setValue(moment().add(1, 'days').format("YYYY-MM-DD 09:00:00"));
        }
    }

    change_fecha_agendamiento(event) {
        /* let fecha = event ? event : this.fecha_agendamiento;
        if (fecha.year) {
            this.formGestion.controls['fecha_agendamiento'].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
        } else {
            this.formGestion.controls['fecha_agendamiento'].setValue('');
        } */
        //this.formGestion.controls['fecha_agendamiento'].setValue(moment(this.fecha_agendamiento).format('YYYY-MM-DD HH:mm:00'));
    }

    cambio_hora_agenda(event) {
        let fecha = this.fecha_agendamiento;
        let tiempo = event ? event : this.hora_agenda;
        if (fecha.year && tiempo != null) {
            this.formGestion.controls['fecha_agendamiento'].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day + " " + tiempo.hour + ":" + tiempo.minute + ":00");
        } else {
            this.formGestion.controls['fecha_agendamiento'].setValue('');
        }
    }

    /* change_fecha_agendamiento_visita(event) {
        this.formGestion.controls['fecha_agendamiento_visita'].setValue(moment(this.fecha_agendamiento_visita).format('YYYY-MM-DD HH:mm:00'));
    } */

    change_fecha_visita(event) {
        let fecha = event ? event : this.fecha_agendamiento_visita;
        if (fecha.year) {
            this.formGestion.controls['fecha_agendamiento_visita'].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
        } else {
            this.formGestion.controls['fecha_agendamiento_visita'].setValue('');
        }
        //this.formGestion.controls['fecha_agendamiento_visita'].setValue(moment(this.fecha_agendamiento_visita).format('YYYY-MM-DD HH:mm:00'));
    }

    change_fecha_reserva(event) {
        let fecha = event ? event : this.fecha_reserva;
        if (fecha.year) {
            this.formGestion.controls['fecha_reserva'].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
        } else {
            this.formGestion.controls['fecha_reserva'].setValue('');
        }
        this.formGestion.controls['fecha_reserva'].setValue(moment(this.fecha_reserva).format('YYYY-MM-DD HH:mm:00'));
    }

    change_fecha_ultima_compra(event) {
        let fecha = event ? event : this.fecha_ultima_compra;
        if (fecha.year) {
            this.formGestion.controls['fecha_ultima_compra'].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
        } else {
            this.formGestion.controls['fecha_ultima_compra'].setValue('');
        }
        //this.formGestion.controls['fecha_ultima_compra'].setValue(moment(this.fecha_ultima_compra).format('YYYY-MM-DD HH:mm:00'));
    }

    //Region , Comunas

    cambio_region_prospecto() {
        this._baseService.lista_simple('comuna', { id_region: this.formProspecto.value.id_region })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.comunas_prospecto = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar comuna prospecto");
                }
            }, (err) => {
                this.showToast("error", "Error", "comuna")
            });
    }

    cambio_region_cot() {
        this._baseService.lista_simple('comuna_sucursal', { id_region: this.formGestion.value.id_region, activo: 1 })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.comunas = response.lista;
                    this.formGestion.controls['id_comuna'].setValue(this.cotizacion.id_comuna ? this.cotizacion.id_comuna : '');
                    this.cambio_comuna_cot();
                } else {
                    this.showToast("error", "Error", "error a cargar comuna prospecto");
                }
            }, (err) => {
                this.showToast("error", "Error", "comuna prospecto")
            });
    }

    cambio_comuna_cot() {
        this._baseService.lista_simple('sucursal', { id_comuna: this.formGestion.value.id_comuna, activo: 1 })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.sucursales = response.lista;
                    this.formGestion.controls['id_sucursal'].setValue(this.cotizacion.id_sucursal ? this.cotizacion.id_sucursal : '');
                    this.cambio_sucursal();
                } else {
                    this.showToast("error", "Error", "error a cargar sucursal cotizacion");
                }
            }, (err) => {
                this.showToast("error", "Error", "sucursal")
            });
    }

    //sucursal
    cambio_sucursal() {
        this._gestionService.ejecutivos(this.formGestion.value.id_sucursal)
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.ejecutivos = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar ejecutivos");
                }
            }, (err) => {
                this.showToast("error", "Error", "ejecutivos")
            });
    }

    //compro

    cambio_compro(index) {
        let valor = this.form_compro[index];
        this.formGestion.controls["id_compro"].setValue("");
        this.compro.splice(index + 1, this.form_compro.length - index);
        this.form_compro.splice(index + 1, this.form_compro.length - index);
        this._baseService.lista_simple('compro', { id_padre: valor })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    if (response.lista.length > 0) {
                        this.compro.push(response.lista);
                        this.form_compro[index + 1] = "";
                    } else {
                        this.formGestion.controls["id_compro"].setValue(valor);
                    }
                } else {
                    this.showToast("error", "Error", "error a cargar hijos compro");
                }
            }, (err) => {
                this.showToast("error", "Error", "hijos compro")
            });
    }

    //Telefonos y emails

    agregar_telefono() {
        if (this.telefonos_prospecto[this.telefonos_prospecto.length - 1].glosa != '') {
            this.telefonos_prospecto.push({ glosa: '', id: this.telefonos_prospecto.length, activo: 1 });
        }
    }

    cambio_telefono() {
        var x = [];
        this.telefonos_prospecto.forEach(function (phone, i) {
            x.push(phone.glosa);
        });
        this.formProspecto.controls["telefonos"].setValue(x.join(','));
    }

    anular_telefono(telefono, index) {
        if (this.telefonos_prospecto[index].glosa == '') {
            this.telefonos_prospecto.splice(index, 1);
        } else {
            this.telefonos_prospecto[index].activo = 0;
        }
    }

    activar_telefono(telefono, index) {
        this.telefonos_prospecto[index].activo = 1;
    }

    agregar_email() {
        if (this.emails_prospecto[this.emails_prospecto.length - 1].glosa != '') {
            this.emails_prospecto.push({ glosa: '', id: this.emails_prospecto.length, activo: 1 });
        }
    }

    cambio_email() {
        var x = [];
        this.emails_prospecto.forEach(function (email, i) {
            x.push(email.glosa);
        });
        this.formProspecto.controls["emails"].setValue(x.join(','));
    }

    anular_email(email, index) {
        if (this.emails_prospecto[index].glosa == '') {
            this.emails_prospecto.splice(index, 1);
        } else {
            this.emails_prospecto[index].activo = 0;
        }
    }

    activar_email(email, index) {
        this.emails_prospecto[index].activo = 1;
    }

    //recordatorio
    cambio_recordatorio() {
        if (this.formGestion.value.id_recordatorio == 1) {
            var fecha = moment(this.cotizacion.fecha_ultimo_agendamiento).add(2, 'hours').format('YYYY-MM-DD HH:mm:ss');
            this.formGestion.controls["id_estado"].setValue(5);
            this.formGestion.controls["fecha_agendamiento"].setValue(fecha);
            this.formGestion.controls["agendar"].setValue('no_agendar');
        }
    }

    //guardar y salir
    submit() {
        this.btnDisabled = true;
        //let rutValido = this.arreglar_rut(this.formProspecto.value.rut);
        //if(rutValido){
        this._prospectoService.editar(this.formProspecto.value, this.telefonos_prospecto, this.emails_prospecto, this.id_prospecto)
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.formGestion.controls['fecha_termino_gestion'].setValue(moment().format("YYYY-MM-DD HH:mm:ss"));
                    let now = moment();
                    let ini = moment(this.formGestion.value.fecha_inicio_gestion, 'YYYY-MM-DD HH:mm:ss');
                    let dif_min = now.diff(ini, 'minutes');
                    let dif_sec = now.diff(ini, 'seconds');
                    let div_sec = dif_sec / 60;
                    let sec = (div_sec - dif_min) * 60;
                    this._gestionService.agregar(this.formGestion.value, this.id_cotizacion, this.cotizacion.id_estado)
                        .subscribe(response => {
                            if (response.resultado == "ACK") {
                                //this.formGestion.reset();
                                this.showToast("success", "Gestion Guardada Exitosamente", "Duración: " + dif_min + " min. " + sec.toFixed(0) + " seg.");
                                if (this.formGestion.value.generar_cotizacion == true) {
                                    this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + response.nueva_cotizacion]);
                                    location.reload();
                                } else {
                                    this._router.navigate(['pages/cotizacion/lista']);
                                }
                            } else {
                                this.showToast("error", "Error", response.mensaje);
                                this.btnDisabled = false;
                            }
                        }, err => {
                            this.showToast("error", "Error", "error al guardar gestion");
                            this.btnDisabled = false;
                        })
                } else {
                    this.showToast("error", "Error", response.mensaje);
                    this.btnDisabled = false;
                }
            }, err => {
                this.btnDisabled = false;
                this.showToast("error", "Error", err);
            })
        //}else{
        //    this.btnDisabled = false;
        //    this.showToast('error',"Rut NO valido", "el rut ingresado no es valido");
        //}
        this.btnDisabled = false;
    }

    //guardar y continuar gestionando
    continuar() {
        /* this.btnDisabled = true;
        let rutValido = this.arreglar_rut(this.formProspecto.value.rut);
        if(rutValido){ */
        this._prospectoService.editar(this.formProspecto.value, this.telefonos_prospecto, this.emails_prospecto, this.id_prospecto)
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.formGestion.controls['fecha_termino_gestion'].setValue(moment().format("YYYY-MM-DD HH:mm:ss"));
                    let now = moment();
                    let ini = moment(this.formGestion.value.fecha_inicio_gestion, 'YYYY-MM-DD HH:mm:ss');
                    let dif_min = now.diff(ini, 'minutes');
                    let dif_sec = now.diff(ini, 'seconds');
                    this._gestionService.agregar(this.formGestion.value, this.id_cotizacion, this.cotizacion.id_estado)
                        .subscribe(response => {
                            if (response.resultado == "ACK") {
                                //this.formGestion.reset();
                                this.showToast("success", "Gestion Guardada Exitosamente", "Duración: " + dif_min + " min. " + dif_sec + " seg.");
                                if (this.formGestion.value.generar_cotizacion == true) {
                                    this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + response.nueva_cotizacion]);
                                    location.reload();
                                } else {
                                    this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
                                    location.reload();
                                }
                            } else {
                                this.showToast("error", "Error", response.mensaje);
                                this.btnDisabled = false;
                            }
                        }, err => {
                            this.showToast("error", "Error", err);
                            this.btnDisabled = false;
                        })
                } else {
                    this.showToast("error", "Error", response.mensaje);
                    this.btnDisabled = false;
                }
            }, err => {
                this.btnDisabled = false;
                this.showToast("error", "Error", err);
            });
        /* }else{
            this.btnDisabled = false;
            this.showToast('error',"Rut NO valido", "el rut ingresado no es valido");
        } */
    }

    salir_sin_guardar() {
        let data = { id_cotizacion: this.id_cotizacion, id_usuario: this.user.id_usuario, id_perfil: this.user.id_perfil };
        this._cotizacionService.desactivar_gestion(data).subscribe(response => {
            if (response.resultado == "ACK") {
                this._router.navigate(['pages/cotizacion/lista']);
            } else {
                this.showToast("error", "Error", "problemas en desactivar gestion");
            }
        }, err => {
            this.showToast("error", "Error", "SOS no salimos");
        });
    }

    //modulos

    showGestiones() {
        const modalRef = this._modalMaterialService.open(GestionListaModalComponent, { scrollable: true, size: 'lg' });
        modalRef.componentInstance.id_cotizacion = this.id_cotizacion;
    }

    showDuplicadas() {
        const activeModal = this._modalService.open(DuplicadasModalComponent, {
            context: {
                modalHeader: 'Duplicadas, Prospecto: ' + this.id_prospecto,
                id_cotizacion: this.id_cotizacion,
                id_prospecto: this.id_prospecto,
            }
        });
    }

    showBloques() {
        /* const activeModal = this._modalService.open(BloqueHorarioModalComponent, {
            context: {
                modalHeader: 'Agendar Visita',
                id_sucursal: this.formGestion.value.id_sucursal,
                id_cotizacion: this.id_cotizacion,
                id_usuario: this.user.id_usuario,

            }
        }); */
        const activeModal = this._modalMaterialService.open(BloqueHorarioModalComponent, { size: 'lg' });
        activeModal.componentInstance.id_sucursal = this.formGestion.value.id_sucursal;
        activeModal.componentInstance.id_cotizacion = this.id_cotizacion;
        activeModal.componentInstance.id_usuario = this.user.id_usuario;
        activeModal.result
            .then((result) => {
                if (result) {
                    this.formGestion.controls["id_ejecutivo"].setValue(result.id_ejecutivo ? result.id_ejecutivo : '');
                    this.formGestion.controls["bloque"].setValue(result.bloque ? result.bloque : '');
                    this.formGestion.controls["fecha_agendamiento_visita"].setValue(result.fecha_agendamiento_visita ? result.fecha_agendamiento_visita : '');
                    this.nombre_ejecutivo = result.nombre_ejecutivo ? result.nombre_ejecutivo : '';
                }
            }, (reason) => {
            });
    }

    validar_rut(rut) {
        var x_rut = rut.split('.').join('');
        x_rut = x_rut.split('-').join('');
        x_rut = x_rut.split(',').join('');
        x_rut = x_rut.split('/').join('');
        x_rut = x_rut.split('_').join('');
        x_rut = x_rut.split('—').join('');
        x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
        var rut_split = x_rut.split('-');
        var k = 2;
        var total = 0;
        for (let i = rut_split[0].length - 1; i >= 0; i--) {
            if (k > 7) {
                k = 2;
            }
            total += (k * rut_split[0][i]);
            k += 1;
        }
        var resto = total % 11;
        var verificador = 11 - resto;
        var x_verificador = verificador == 10 ? 'K' : verificador == 11 ? 0 : verificador;
        if (rut_split[1].toUpperCase() == x_verificador) {
            return x_rut;
        } else {
            return false;
        }
    }

    arreglar_rut(rut) {
        var x_rut = rut.split('.').join('');
        x_rut = x_rut.split('-').join('');
        x_rut = x_rut.split(',').join('');
        x_rut = x_rut.split('/').join('');
        x_rut = x_rut.split('_').join('');
        x_rut = x_rut.split('—').join('');
        x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
        return x_rut;
    }


    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    private showToast(type: string, title: string, body: string) {
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}
