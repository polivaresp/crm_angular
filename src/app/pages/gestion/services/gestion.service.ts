import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs';

@Injectable()
export class GestionService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    estados(estado): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + "estados/" + estado, data, { headers: headerss });
    }

    ejecutivos(id_sucursal): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_sucursal: id_sucursal };

        return this._http.post(this.base_url + "ejecutivos/lista_simple_cupos", data, { headers: headerss });
    }

    ejecutivos_sc(id_sucursal): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_sucursal: id_sucursal };

        return this._http.post(this.base_url + "ejecutivos/lista_simple_sc", data, { headers: headerss });
    }

    agregar(gestion, id, estado_actual): Observable<any> {
        let headerss = this.getHeaders();
        let data = { gestion: gestion, id_cotizacion: id, estado_actual: estado_actual };

        return this._http.post(this.base_url + "gestion/agregar", data, { headers: headerss });
    }

    lista_simple(id_cotizacion): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_cotizacion: id_cotizacion };

        return this._http.post(this.base_url + "gestion/lista_simple", data, { headers: headerss });
    }

    bloques_horarios(sucursal, ejecutivo, fecha): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_sucursal: sucursal, id_ejecutivo: ejecutivo, fecha: fecha };

        return this._http.post(this.base_url + "ejecutivos/bloques_horarios/lista", data, { headers: headerss });
    }

    reservar_bloque(modulo, id_cotizacion, id_usuario): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_reserva: modulo.id_reserva, id_cotizacion: id_cotizacion, id_usuario: id_usuario };

        return this._http.post(this.base_url + "ejecutivos/bloques_horarios/reservar", data, { headers: headerss });
    }

    agregar_horario(horario): Observable<any> {
        let headerss = this.getHeaders();
        let data = { horario: horario };

        return this._http.post(this.base_url + "ejecutivos/bloques_horarios/agregar", data, { headers: headerss });
    }

}