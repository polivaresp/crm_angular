import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GestionComponent } from './gestion.component';
import { GestionEditarComponent } from './gestion_editar/gestion_editar.component';
import { GestionListaComponent } from './gestion_lista/gestion_lista.component';
import { BloqueHorarioModalComponent } from './modals/bloque_horario.modal';
import { DuplicadasModalComponent } from '../cotizacion/modals/duplicadas/duplicadas.modal';


const routes: Routes = [
  {
    path: '',
    component: GestionComponent,
    children: [
      {
        path: 'lista/:id_prospecto/:id_cotizacion',
        component: GestionListaComponent,
      },
      {
        path: 'editar/:id_prospecto/:id_cotizacion',
        component: GestionEditarComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GestionRoutingModule { }

export const routedComponents = [
  GestionComponent,
  GestionEditarComponent,
  GestionListaComponent,
  BloqueHorarioModalComponent,
  DuplicadasModalComponent
];
