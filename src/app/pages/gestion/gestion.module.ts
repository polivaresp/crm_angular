import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { GestionRoutingModule, routedComponents } from './gestion-routing.module';

import { BloqueHorarioModalComponent } from './modals/bloque_horario.modal';
import { DuplicadasModalComponent } from '../cotizacion/modals/duplicadas/duplicadas.modal';
import { NbCardModule, NbCheckboxModule, NbButtonModule, NbDialogModule, NbListModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    ThemeModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NbCardModule,
    NbCheckboxModule,
    GestionRoutingModule,
    NbListModule,
    NbDialogModule.forChild(),
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
  entryComponents: [
    BloqueHorarioModalComponent,
    DuplicadasModalComponent
  ],
})
export class GestionModule { }
