import { Component, Input } from '@angular/core';

import * as moment from 'moment';
import { GestionService } from '../services/gestion.service';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-modal',
  templateUrl: './gestion_lista.modal.html',
  providers: [GestionService]
})
export class GestionListaModalComponent {

  @Input() id_cotizacion: number;
  @Input() gestiones = [];


  constructor(private activeModal: NgbActiveModal, private _gestionService: GestionService, private _toasterService: ToasterService) { }

  ngOnInit() {
    this._gestionService.lista_simple(this.id_cotizacion).subscribe(res => {
      if (res.resultado == "ACK") {
        this.gestiones = res.lista;
      } else {
        //this.showToast('error', 'Error!', 'Problemas al desbloquear gestion');
        this.showToast("error", "Error", "Error al traer gestiones");
      }
    }, error => {
      this.showToast("error", "Error", "Error interno");
    });
  }


  closeModal() {
    this.activeModal.close();
  }

  getDate(fecha, format) {
    if (fecha) {
      if (fecha != '') {
        let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
        return x;
      }
    }
    return '-';
  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
