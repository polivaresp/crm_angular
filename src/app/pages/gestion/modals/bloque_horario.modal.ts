import { Component } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';
import { GestionService } from '../services/gestion.service';
import * as moment from 'moment';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-modal',
  templateUrl: './bloque_horario.modal.html',
  providers: [GestionService]
})
export class BloqueHorarioModalComponent {

  min;
  total_horarios: number;
  modalHeader: string;
  id_cotizacion: number;
  id_usuario: number;
  id_sucursal: number;
  bloques = [];
  bloque;
  nombre_ejecutivo: string;
  ejecutivos = [];
  horarios = [];
  fecha_agendamiento_visita;
  fecha;
  formAgendamiento: FormGroup;
  data: any;
  now: Date;

  constructor(private activeModal: NgbActiveModal, private _gestionService: GestionService, private fb: FormBuilder, private _toasterService: ToasterService) {
    this.formAgendamiento = fb.group({
      fecha: ['', Validators.required],
      id_ejecutivo: ['', Validators.required]
    });
    this.now = new Date();
    this.min = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
  }

  ngOnInit() {
    this._gestionService.ejecutivos(this.id_sucursal)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.ejecutivos = response.lista;
        } else {
          this.showToast("error", "Error", "error a cargar ejecutivos");
        }
      }, function (err) {
        this.showToast("error", "Error", err)
      });
  }

  submit() {
    this.horarios = [];
    this.total_horarios = 0;
    this._gestionService.bloques_horarios(this.id_sucursal, this.formAgendamiento.value.id_ejecutivo, this.formAgendamiento.value.fecha)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.horarios = response.lista;
          this.total_horarios = this.horarios.length;
        } else {
          this.showToast("error", "Error", "error a cargar horarios");
        }
      }, function (err) {
        this.showToast("error", "Error", err)
      });
  }

  reservar_bloque(modulo) {
    this._gestionService.reservar_bloque(modulo, this.id_cotizacion, this.id_usuario)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.bloque = modulo.bloque;
          let id_ejecutivo = this.formAgendamiento.value.id_ejecutivo;
          let nombre = '';
          this.ejecutivos.forEach(function (e, i) {
            if (e.id_ejecutivo = id_ejecutivo) {
              nombre = e.nombre_completo;
            }
          });
          this.nombre_ejecutivo = nombre;
          this.fecha_agendamiento_visita = this.formAgendamiento.value.fecha;
          let aux_bloque = this.bloque.split('-');
          var data = {
            id_ejecutivo: this.formAgendamiento.value.id_ejecutivo,
            nombre_ejecutivo: this.nombre_ejecutivo,
            bloque: this.bloque,
            fecha_agendamiento_visita: this.formAgendamiento.value.fecha + ' ' + aux_bloque[0].replace(/\s/g, "") + ':00'
          }
          this.data = data;
          this.closeModal();
        } else {
          this.showToast("error", "Error", "error a reservar bloque");
        }
      }, function (err) {
        this.showToast("error", "Error", err)
      });
  }

  change_fecha_agendamiento_visita(event) {
    let fecha = event ? event : this.fecha_agendamiento_visita;
    if (fecha.year) {
      this.formAgendamiento.controls["fecha"].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
    } else {
      this.formAgendamiento.controls["fecha"].setValue('');
    }
    //this.formAgendamiento.controls["fecha"].setValue(moment(this.fecha_agendamiento_visita).format('YYYY-MM-DD'));
  }


  closeModal() {
    this.activeModal.close(this.data);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}
