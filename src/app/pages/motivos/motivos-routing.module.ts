import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MotivosComponent } from './motivos.component';
import { MotivosListaComponent } from './lista/motivos_lista.component';
import { EditarMotivoComponent } from './editar/editar_motivo.component';
import { AgregarMotivoComponent } from './agregar/agregar_motivo.component';

const routes: Routes = [
  {
    path: '',
    component: MotivosComponent,
    children: [
      {
        path: 'lista',
        component: MotivosListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarMotivoComponent,
      },
      {
        path: 'editar/:id',
        component: EditarMotivoComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotivosRoutingModule { }

export const routedComponents = [
  MotivosComponent,
  MotivosListaComponent,
  EditarMotivoComponent,
  AgregarMotivoComponent
];
