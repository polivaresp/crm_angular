import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { MotivoService } from '../services/motivo.service';


@Component({
  selector: 'ngx-editar-motivo',
  templateUrl: './editar_motivo.component.html',
  providers: [BaseService, MotivoService]
})
export class EditarMotivoComponent {

  estados: { id: string; glosa: string; }[];
  form: FormGroup;
  id: any;
  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _motivoService: MotivoService) {
    this._route.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.form = fb.group({
      id_estado: [{ value: '', disabled: true }, Validators.required],
      glosa_motivo: ['', Validators.required],
      activo: true,
    });
    this.estados = [];
  }

  ngOnInit() {
    this._motivoService.estados_para_motivo().subscribe(response => {
      if (response.resultado == "ACK") {
        this.estados = response.lista;
        this.cargar_form();
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });

  }

  cargar_form() {
    this._baseService.ver('motivo', this.id).subscribe(response => {
      if (response.resultado == "ACK") {
        this.form.controls['id_estado'].setValue(response.modelo.id_estado);
        this.form.controls['glosa_motivo'].setValue(response.modelo.glosa_motivo);
        this.form.controls['activo'].setValue(response.modelo.activo);
        //this.showToast("success","Exito",response.mensaje);
      } else {
        //this.showToast("error","Error",response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  submit() {

    let form = [];
    form["glosa_motivo"] = this.form.value.glosa_motivo;
    form["activo"] = this.form.value.activo;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.editar('motivo', formulario, this.id).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/motivos/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    });
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/motivos/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}