import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { MotivoService } from '../services/motivo.service';

import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import * as moment from 'moment';
import { NbDialogService } from '@nebular/theme';

@Component({
    selector: 'ngx-motivos-lista',
    templateUrl: './motivos_lista.component.html',
    providers: [BaseService, MotivoService]
})
export class MotivosListaComponent {

    cantidad: number;
    form;
    estado;
    pagina_actual: number;
    lista;
    estados;
    anular_motivo = [];
    habilitar_motivo = [];
    acciones: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _motivoService: MotivoService, private _modalService: NbDialogService) {
        this.lista = [];

        this.pagina_actual = 1;

        this.cantidad = 0;
        this.form = [];
        this.form["estado"] = "";
        this.estado = "";
        this.estados = [];
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
    }

    ngOnInit() {
        this._motivoService.estados_para_motivo().subscribe(response => {
            if (response.resultado == "ACK") {
                this.estados = response.lista;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    anular(obj) {
        this._baseService.desactivar('motivo', obj.id_motivo).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_motivo = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar('motivo', obj.id_motivo).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_motivo = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/motivos/editar/' + obj.id_motivo]);
    }

    agregar() {
        this._router.navigate(['pages/motivos/agregar']);
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'motivo',
                obj: obj
            }
        });
        activeModal.onClose.subscribe(result => {
            if (result) {
                this.submit();
            } else {
                console.log("closed");
            }
        });
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        this.estado = this.form["estado"];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._motivoService.lista_motivos(this.form["estado"], ["orden", "asc"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}