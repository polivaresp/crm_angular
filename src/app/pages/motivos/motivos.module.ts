import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { MotivosRoutingModule, routedComponents } from './motivos-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbCardModule, NbCheckboxModule, NbButtonModule } from '@nebular/theme';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    ThemeModule,
    MotivosRoutingModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NgbPaginationModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class MotivosModule { }
