import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { MotivoService } from '../services/motivo.service';


@Component({
  selector: 'ngx-agregar-motivo',
  templateUrl: './agregar_motivo.component.html',
  providers: [BaseService, MotivoService]
})
export class AgregarMotivoComponent {

  estados: { id: string; glosa: string; }[];
  form: FormGroup;
  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _motivoService: MotivoService) {
    this.form = fb.group({
      id_estado: ['', Validators.required],
      glosa_motivo: ['', Validators.required],
      activo: true,
    });
    this.estados = [{ "id": '7', "glosa": "Posterga" }, { "id": '10', "glosa": "Inubicable" }, { "id": '12', "glosa": "Desiste" }, { "id": '15', "glosa": "Alerta de Atención" }];
  }

  ngOnInit() {

  }

  submit() {

    let form = [];
    form["id_estado"] = this.form.value.id_estado;
    form["glosa_motivo"] = this.form.value.glosa_motivo;
    form["activo"] = this.form.value.activo;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.agregar('motivo', formulario).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/motivos/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    });
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/motivos/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}