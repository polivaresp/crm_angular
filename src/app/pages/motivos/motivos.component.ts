import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'ngx-motivos',
  template: `<router-outlet></router-outlet>`
})
export class MotivosComponent {
}
