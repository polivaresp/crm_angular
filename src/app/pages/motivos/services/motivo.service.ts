import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { APP_CONFIG} from '../../../services/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MotivoService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders(){
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    estados_para_motivo() : Observable<any>{
        let headerss = this.getHeaders();
        return this._http.post(this.base_url +"motivo/estados", {}, {headers:headerss});
        
    }

    lista_motivos(estado, orden = [], pagina) : Observable<any>{
        let headerss = this.getHeaders();
        let post = {id_estado: estado, orden: orden};
        return this._http.post(this.base_url +"motivo/lista/"+pagina,post, {headers:headerss});
        
    }

}