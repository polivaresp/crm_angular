import { Component } from '@angular/core';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
    `,
})
export class PagesComponent {
  public menu;
  public usuario;
  public permisos;
  public modulos;
  public etiquetas;
  perfil: any;

  constructor() {
    this.usuario = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.perfil = JSON.parse(localStorage.getItem('usr_login')).perfil;
    this.permisos = JSON.parse(localStorage.getItem('permisos'));
    this.modulos = JSON.parse(localStorage.getItem('modulos'));
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
  }

  ngOnInit() {
    this.generarMenu();
  }

  private generarMenu() {
    let menu = [];
    this.modulos.forEach(mod => {
      let submodulos = []
      this.permisos.forEach(per => {
        if (mod.id_modulo == per.modulo) {
          let titulo = per.titulo;
          if (per.id_permiso == 5) {//sucursal
            titulo = this.etiquetas.sucursal;
          } else if (per.id_permiso == 6) {//item
            titulo = this.etiquetas.item;
          } else if (per.id_permiso == 10) {//compro
            titulo = this.etiquetas.compro;
          }
          submodulos.push({
            title: titulo,
            link: per.link,
            children: per.id_permiso != 8 ? null :
              [
                { title: "Definición", link: per.link },
                { title: "Asignación", link: "email/asignacion" }
              ]
          });
        }
      });
      if (submodulos.length > 0) {
        menu.push({
          title: mod.glosa,
          icon: mod.icon,
          link: mod.link,
          home: this.perfil.home == mod.link,
          children: submodulos
        });
      }
    });
    this.menu = menu;
  }
}
