import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'cotizacion',
      loadChildren: './cotizacion/cotizacion.module#CotizacionModule',
    },
    {
      path: 'gestion',
      loadChildren: './gestion/gestion.module#GestionModule',
    },
    {
      path: 'prospecto',
      loadChildren: './prospecto/prospecto.module#ProspectoModule',
    },
    {
      path: 'usuario',
      loadChildren: './usuario/usuario.module#UsuarioModule',
    },
    {
      path: 'sucursal',
      loadChildren: './sucursal/sucursal.module#SucursalModule',
    },
    {
      path: 'horarios',
      loadChildren: './horario/horario.module#HorarioModule',
    },
    {
      path: 'reporte',
      loadChildren: './reporte/reporte.module#ReporteModule',
    },
    {
      path: 'mantenedor',
      loadChildren: './mantenedor/mantenedor.module#MantenedorModule',
    },
    {
      path: 'motivos',
      loadChildren: './motivos/motivos.module#MotivosModule',
    },
    {
      path: 'estado',
      loadChildren: './estado/estado.module#EstadoModule',
    },
    {
      path: 'item',
      loadChildren: './item/item.module#ItemModule',
    },
    {
      path: 'compro',
      loadChildren: './compro/compro.module#ComproModule',
    },
    {
      path: 'email',
      loadChildren: './email/email.module#EmailModule',
    },
    {
      path: 'despachador',
      loadChildren: './despachador/despachador.module#DespachadorModule',
    },
    {
      path: 'cargas',
      loadChildren: './cargas/cargas.module#CargasModule',
    },
    {
      path: 'campovariable',
      loadChildren: './campovariable/campovariable.module#CampovariableModule',
    },
    {
      path: 'perfil',
      loadChildren: './perfil/perfil.module#PerfilModule',
    },
    {
      path: 'config',
      loadChildren: './config/config.module#ConfigModule',
    },
    {
      path: 'ventas',
      loadChildren: './ventas/ventas.module#VentasModule',
    },
    {
      path: 'miscellaneous',
      loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
    },
    {
      path: '',
      redirectTo: 'miscellaneous',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
