import { Component } from '@angular/core';

import { CotizacionService } from '../services/cotizacion.service';
import { BaseService } from '../../../services/base.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../../usuario/services/usuario_service';
import { ReasignarModalComponent } from '../modals/reasignar/reasignar.modal';
/* 
import { FormGroup } from '@angular/forms/src/model'; */
import { FormBuilder } from '@angular/forms';
import { FiltrosModalComponent } from '../modals/filtros/filtros.modal';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-reasignar-cotizacion',
  templateUrl: './reasignar_cotizacion.component.html',
  providers: [CotizacionService, BaseService, UsuarioService]
})
export class ReasignarCotizacionComponent {

  user;
  isSeleccionados: boolean;

  cotizaciones; //lista
  seleccionados;

  tiene_filtro: any;
  lista;
  etiquetas: any;
  formSelTodos: any;

  pagina_actual: any = 1;
  cantidad: any;
  cantidad_pagina: number = 10;
  previusPage: any;

  constructor(private _cotizacionService: CotizacionService, private _modalService: NbDialogService, private _baseService: BaseService, private router: Router, private _usuarioService: UsuarioService, private fb: FormBuilder) {
    this.pagina_actual = 1;
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.cotizaciones = [];
    this.seleccionados = [];
    this.isSeleccionados = false;
    this.formSelTodos = fb.group({
      seleccionar_todo: false,
    });
    this.tiene_filtro = 0;
  }

  ngOnInit() {
    this.formSelTodos.get('seleccionar_todo').valueChanges.subscribe((sel) => {
      this.seleccionados = [];
      this.cotizaciones.forEach(function (obj, indx) {
        obj.seleccionado = sel;
      });
      this.validar_seleccionados();
    });

    this.submit(1);
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }

  submit(reiniciar_pagina = 0) {
    if (reiniciar_pagina == 1) {
      this.pagina_actual = 1;
    }
    this.cotizaciones = [];

    let filtros = localStorage.getItem('filtros-reasignar') ? JSON.parse(localStorage.getItem('filtros-reasignar')) : null;
    this.tiene_filtro = localStorage.getItem('tiene-filtro-reasignar') ? localStorage.getItem('tiene-filtro-reasignar') : 0;

    this._cotizacionService.lista_reasignar(filtros, this.pagina_actual).subscribe(response => {
      if (response.resultado == "ACK") {
        this.cantidad = response.lista.length > 0 ? response.cantidad : 0;
        this.cotizaciones = response.lista.length > 0 ? response.lista : [];
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
      } else {
        console.log("problemas en cargar lista");
      }
    }, err => {
      console.log("Eingg");
    });

  }

  validar_seleccionados() {
    let seleccionadas = [];
    this.cotizaciones.forEach(function (obj, ind) {
      if (obj.seleccionado == true) {
        seleccionadas.push(obj.id_cotizacion);
      } else {
        let pos = seleccionadas.indexOf(obj.id_cotizacion);
        if (pos >= 0) {
          seleccionadas.splice(pos, 1);
        }
      }
    });
    this.seleccionados = seleccionadas;
  }

  showReasignar() {
    this.validar_seleccionados();
    const activeModal = this._modalService.open(ReasignarModalComponent, {
      context: {
        modalHeader: 'Reasignar Cotizaciones',
        lista_seleccionadas: this.seleccionados
      }
    });
    activeModal.onClose.subscribe(result => {
      if (result) {
        this.formSelTodos.controls["seleccionar_todo"].setValue(false);
        this.seleccionados = [];
        this.submit(1);
      } else {
        console.log("closed");
      }
    });
  }

  filtrosModal() {
    const activeModal2 = this._modalService.open(FiltrosModalComponent, {
      context: {
        panel: 'reasignar',
      }
    });
    activeModal2.onClose.subscribe(result => {
      if (result) {
        this.submit(1);
      } else {
        console.log('closed');
      }
    });
  }

}
