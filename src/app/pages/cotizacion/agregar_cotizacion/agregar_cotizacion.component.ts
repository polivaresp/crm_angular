import { Component } from '@angular/core';
import { BaseService } from '../../../services/base.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ProspectoService } from '../../prospecto/services/prospecto.service';
import { CotizacionService } from '../services/cotizacion.service';

@Component({
  selector: 'ngx-agregar-cotizacion',
  templateUrl: './agregar_cotizacion.component.html',
  providers: [BaseService, ProspectoService, CotizacionService]
})
export class AgregarCotizacionComponent {

  camposvariables: any[];
  comunas_prospectos: any[];
  regiones_prospectos: any[];
  sucursales: any[];
  items: any[];
  campanias: any[];
  origenes: any[];
  emails_prospecto: any;
  telefonos_prospecto: any;
  emails;
  telefonos: any;
  public user: any;
  public clientes;
  public formProspecto: FormGroup;
  public formCotizacion: FormGroup;
  regiones;
  comunas;
  etiquetas: any;

  constructor(private _router: Router, private fb: FormBuilder, private _baseService: BaseService, private _prospectoService: ProspectoService,
    private _toasterService: ToasterService, private _cotizacionService: CotizacionService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.emails_prospecto = [];
    this.emails_prospecto.push({ glosa: '', id: 1, activo: 1 });
    this.telefonos_prospecto = [];
    this.telefonos_prospecto.push({ glosa: '', id: 1, activo: 1 });
    this.formProspecto = fb.group({
      id_prospecto: '',
      rut: ['', Validators.required],
      nombre: ['', Validators.required],
      apellido_paterno: ['', Validators.required],
      emails: ['', Validators.required],
      telefonos: ['', Validators.required],
      apellido_materno: '',
      id_region: '',
      id_comuna: '',
      direccion: '',
      dato_0: '',
      dato_1: '',
      dato_2: '',
      dato_3: '',
      dato_4: '',
      dato_5: '',
      dato_6: '',
      dato_7: '',
      dato_8: '',
      dato_9: '',
    });
    this.formCotizacion = fb.group({
      id_campania: ['', Validators.required],
      id_origen: ['', Validators.required],
      id_item: ['', Validators.required],
      id_region: '',
      id_comuna: '',
      id_sucursal: '',
      comentario: ['', Validators.required],
    });
    this.origenes = []; //origenes
    this.campanias = [];// camapañas
    this.items = [];// items
    this.sucursales = [];//sucursales
    this.regiones = [];//regiones
    this.regiones_prospectos = [];//regiones
    this.comunas = [];//comunas
    this.comunas_prospectos = [];//comunas
  }

  ngOnInit() {
    //cargar camposvariables
    this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.camposvariables = response.lista;
        } else {
          this.showToast("error", "Error", response.mensaje);
        }
      }, (err) => {
        this.showToast("error", "Error", err);
      });
    this.formProspecto.get('rut').valueChanges.subscribe((rut) => {
      //limpiar form prospecto
      this.formProspecto.controls["id_prospecto"].setValue('');
      this.formProspecto.controls["nombre"].setValue('');
      this.formProspecto.controls["apellido_paterno"].setValue('');
      this.formProspecto.controls["apellido_materno"].setValue('');
      this.formProspecto.controls["id_region"].setValue('');
      this.cambiar_region_prospecto();
      this.formProspecto.controls["id_comuna"].setValue('');
      this.formProspecto.controls["direccion"].setValue('');
      this.formProspecto.controls["dato_0"].setValue('');
      this.formProspecto.controls["dato_1"].setValue('');
      this.formProspecto.controls["dato_2"].setValue('');
      this.formProspecto.controls["dato_3"].setValue('');
      this.formProspecto.controls["dato_4"].setValue('');
      this.formProspecto.controls["dato_5"].setValue('');
      this.formProspecto.controls["dato_6"].setValue('');
      this.formProspecto.controls["dato_7"].setValue('');
      this.formProspecto.controls["dato_8"].setValue('');
      this.formProspecto.controls["dato_9"].setValue('');
      this.emails_prospecto = []; this.emails_prospecto.push({ glosa: '' }); this.cambio_email();
      this.telefonos_prospecto = []; this.telefonos_prospecto.push({ glosa: '' }); this.cambio_telefono();
      //limpiar rut     
      let rut_r = this.arreglar_rut(rut);

      if (rut_r && rut_r != '' && rut_r != '-') {
        //cargar datos prospecto
        this._prospectoService.ver_rut(rut_r).subscribe(response => {
          if (response.resultado == "ACK") {
            this.formProspecto.controls["id_prospecto"].setValue(response.prospecto.id_prospecto);
            this.formProspecto.controls["nombre"].setValue(response.prospecto.nombre);
            this.formProspecto.controls["apellido_paterno"].setValue(response.prospecto.apellido_paterno);
            this.formProspecto.controls["apellido_materno"].setValue(response.prospecto.apellido_materno);
            this.formProspecto.controls["id_region"].setValue(response.prospecto.id_region);
            this.cambiar_region_prospecto();
            this.formProspecto.controls["id_comuna"].setValue(response.prospecto.id_comuna);
            this.formProspecto.controls["direccion"].setValue(response.prospecto.direccion);
            this.formProspecto.controls["dato_0"].setValue(response.prospecto.dato_0);
            this.formProspecto.controls["dato_1"].setValue(response.prospecto.dato_1);
            this.formProspecto.controls["dato_2"].setValue(response.prospecto.dato_2);
            this.formProspecto.controls["dato_3"].setValue(response.prospecto.dato_3);
            this.formProspecto.controls["dato_4"].setValue(response.prospecto.dato_4);
            this.formProspecto.controls["dato_5"].setValue(response.prospecto.dato_5);
            this.formProspecto.controls["dato_6"].setValue(response.prospecto.dato_6);
            this.formProspecto.controls["dato_7"].setValue(response.prospecto.dato_7);
            this.formProspecto.controls["dato_8"].setValue(response.prospecto.dato_8);
            this.formProspecto.controls["dato_9"].setValue(response.prospecto.dato_9);
            this._baseService.lista_simple('telefonoprospecto', { id_prospecto: response.prospecto.id_prospecto, activo: 1 })
              .subscribe(response => {
                if (response.resultado == "ACK") {
                  let arraytelefonos = [];
                  response.lista.forEach(function (e, i) {
                    arraytelefonos.push({ glosa: e.glosa, id: e.id, activo: e.activo });
                  });
                  this.telefonos_prospecto = arraytelefonos
                  this.cambio_telefono();
                } else {
                  this.showToast("error", "Error", "error al cargar telefonos prospecto");
                }
              }, (err) => {
                this.showToast("error", "Error", "  telefonos")
              });
            this._baseService.lista_simple('emailprospecto', { id_prospecto: response.prospecto.id_prospecto, activo: 1 })
              .subscribe(response => {
                if (response.resultado == "ACK") {
                  let arrayemails = [];
                  response.lista.forEach(function (e, i) {
                    arrayemails.push({ glosa: e.glosa, id: e.id, activo: e.activo });
                  });
                  this.emails_prospecto = arrayemails;
                  this.cambio_email();
                } else {
                  this.showToast("error", "Error", "error emails prospecto");
                }
              }, (err) => {
                this.showToast("error", "Error", "  emails")
              });
          }
        }, error => {
          this.showToast("error", "Error", error);
        });
      }
    });
    this.prellenar_selects();
  }

  prellenar_selects() {
    //cargar campañas
    this._baseService.lista_simple('campania')
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.campanias = response.lista;
        } else {
          console.log("error a cargar campaña");
        }
      }, err => {
        this.showToast("error", "Error Interno", err);
      });

    this._baseService.lista_simple('region')
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.regiones = response.lista;
        } else {
          console.log("error a cargar regiones");
        }
      }, err => {
        this.showToast("error", "Error Interno", err);
      });

    this._baseService.lista_simple('region')
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.regiones_prospectos = response.lista;
        } else {
          console.log("error a cargar regiones_prospectos");
        }
      }, err => {
        this.showToast("error", "Error Interno", err);
      });

    this._baseService.lista_simple('origen')
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.origenes = response.lista;
        } else {
          console.log("error a cargar origenes");
        }
      }, err => {
        this.showToast("error", "Error Interno", err);
      });

    this._baseService.lista_simple('item')
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.items = response.lista;
        } else {
          console.log("error a cargar items");
        }
      }, err => {
        this.showToast("error", "Error Interno", err);
      });
  }

  cambiar_region_prospecto() {
    let data = { id_region: this.formProspecto.value.id_region };
    this._baseService.lista_simple('comuna', data)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.comunas_prospectos = response.lista;
        } else {
          console.log("error a cargar comuna");
        }
      }, err => {
        this.showToast("error", "Error Interno", err);
      });
  }

  cambiar_region() {
    let data = { id_region: this.formCotizacion.value.id_region, activo: 1 };
    this._baseService.lista_simple('comuna', data)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.comunas = response.lista;
        } else {
          console.log("error a cargar comuna");
        }
      }, err => {
        this.showToast("error", "Error Interno", err)
      });
  }

  cambiar_comuna() {
    let data = { id_comuna: this.formCotizacion.value.id_comuna, activo: 1 };
    this._baseService.lista_simple('sucursal', data)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.sucursales = response.lista;
        } else {
          console.log("error a cargar sucursal");
        }
      }, err => {
        this.showToast("error", "Error Interno", err)
      });
  }

  agregar_telefono() {
    if (this.telefonos_prospecto[this.telefonos_prospecto.length - 1].glosa != '') {
      this.telefonos_prospecto.push({ glosa: '', id: this.telefonos_prospecto.length + 1, activo: 1 });
    }
  }

  cambio_telefono() {
    var x = [];
    this.telefonos_prospecto.forEach(function (phone, i) {
      x.push(phone.glosa);
    });
    this.formProspecto.controls["telefonos"].setValue(x.join(','));
    if (this.formProspecto.value.telefonos != '') {
      this.formProspecto.get('emails').setValidators([]);
    } else {
      this.formProspecto.get('emails').setValidators([Validators.required]);
    }
    this.formProspecto.get('emails').updateValueAndValidity();

  }

  anular_telefono(telefono, index) {
    if (index > 0) {
      this.telefonos_prospecto.splice(index, 1);
    }
    this.cambio_telefono();
  }

  agregar_email() {
    if (this.emails_prospecto[this.emails_prospecto.length - 1].glosa != '') {
      this.emails_prospecto.push({ glosa: '', id: this.emails_prospecto.length + 1, activo: 1 });
    }
  }

  cambio_email() {
    var x = [];
    this.emails_prospecto.forEach(function (email, i) {
      x.push(email.glosa);
    });
    this.formProspecto.controls["emails"].setValue(x.join(','));
    if (this.formProspecto.value.telefonos != '') {
      this.formProspecto.get('telefonos').setValidators([]);
    } else {
      this.formProspecto.get('telefonos').setValidators([Validators.required]);
    }
    this.formProspecto.get('telefonos').updateValueAndValidity();
  }

  anular_email(email, index) {
    if (index > 0) {
      this.emails_prospecto.splice(index, 1);
    }
    this.cambio_email();
  }

  submit() {
    let prospecto = this.formProspecto.value;
    //prospecto.rut = this.arreglar_rut(prospecto.rut);
    if (!prospecto.rut) {
      this.showToast("error", "Error", "Rut Invalido");
    } else {
      prospecto.telefonos = this.telefonos_prospecto;
      prospecto.emails = this.emails_prospecto;
      let cotizacion = this.formCotizacion.value;
      this._cotizacionService.agregar(prospecto, cotizacion, this.user).subscribe(response => {
        if (response.resultado == "ACK") {
          this.showToast("success", "Exito", response.mensaje);
          this._router.navigate(['pages/cotizacion/lista']);
        } else {
          this.showToast("error", "Error", response.mensaje);
        }
      }, error => {
        this.showToast("error", "Error", error);
      });
    }

  }

  salir_sin_guardar() {
    this._router.navigate(['pages/cotizacion/lista']);
  }

  validar_rut(rut) {
    var x_rut = rut.split('.').join('');
    x_rut = x_rut.split('-').join('');
    x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
    var rut_split = x_rut.split('-');
    var k = 2;
    var total = 0;
    for (let i = rut_split[0].length - 1; i >= 0; i--) {
      if (k > 7) {
        k = 2;
      }
      total += (k * rut_split[0][i]);
      k += 1;
    }
    var resto = total % 11;
    var verificador = 11 - resto;
    var x_verificador = verificador == 10 ? 'k' : verificador == 11 ? 0 : verificador;
    if (rut_split[1] == x_verificador) {
      return x_rut;
    } else {
      return false;
    }
  }

  arreglar_rut(rut) {
    var x_rut = rut.split('.').join('');
    x_rut = x_rut.split('-').join('');
    x_rut = x_rut.split(',').join('');
    x_rut = x_rut.split('/').join('');
    x_rut = x_rut.split('_').join('');
    x_rut = x_rut.split('—').join('');
    x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
    return x_rut;
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
