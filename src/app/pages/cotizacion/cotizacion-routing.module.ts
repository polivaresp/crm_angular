import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CotizacionComponent } from './cotizacion.component';
import { CotizacionListaComponent } from './cotizacion_lista/cotizacion_lista.component';
import { ReasignarCotizacionComponent } from './reasignar_cotizacion/reasignar_cotizacion.component';
import { AgregarCotizacionComponent } from './agregar_cotizacion/agregar_cotizacion.component';
import { ReasignarModalComponent } from './modals/reasignar/reasignar.modal';
import { GestionListaModalComponent } from '../gestion/modals/gestion_lista.modal';
import { FiltrosModalComponent } from './modals/filtros/filtros.modal';
import { ConfirmarDuplicadaModalComponent } from './modals/duplicar_cotizacion/confirmar_duplicada.modal';
import { VerCotizacionComponent } from './ver/ver_cotizacion.component';


const routes: Routes = [
  {
    path: '',
    component: CotizacionComponent,
    children: [
      {
        path: 'lista',
        component: CotizacionListaComponent,
      },
      {
        path: 'reasignar',
        component: ReasignarCotizacionComponent,
      },
      {
        path: 'agregar',
        component: AgregarCotizacionComponent,
      },
      {
        path: 'ver/:id_prospecto/:id_cotizacion',
        component: VerCotizacionComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CotizacionRoutingModule { }

export const routedComponents = [
  CotizacionComponent,
  CotizacionListaComponent,
  ReasignarCotizacionComponent,
  AgregarCotizacionComponent,
  ReasignarModalComponent,
  FiltrosModalComponent,
  ConfirmarDuplicadaModalComponent,
  VerCotizacionComponent
];
