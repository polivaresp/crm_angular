import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CotizacionService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient, private _httpClient: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista(filtros, lista, pagina, user): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'filtros': filtros, id_usuario: user.id_usuario, id_perfil: user.id_perfil };
        if (lista == 1) {
            return this._http.post(this.base_url + "cotizacion/lista1/" + pagina, data, { headers: headerss });
        } else if (lista == 2) {
            return this._http.post(this.base_url + "cotizacion/lista2/" + pagina, data, { headers: headerss });
        } else if (lista == 3) {
            return this._http.post(this.base_url + "cotizacion/lista3/" + pagina, data, { headers: headerss });
        } else {
            return this._http.post(this.base_url + "cotizacion/lista1/" + pagina, data, { headers: headerss });
        }
    }

    lista_excel(filtros, user, pagina): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'filtros': filtros, id_usuario: user.id_usuario, id_perfil: user.id_perfil };
        return this._http.post(this.base_url + "cotizacion/lista_excel/" + pagina, data, { headers: headerss });
    }

    excel_lista(filtros, user) {
        interface Lista {
            lista: any[];
            resutado: string;
            mensaje: string;
        };
        interface Cuenta {
            total: number;
            resutado: string;
            mensaje: string;
        };
        let headerss = new HttpHeaders();
        headerss.append('tkn-cliente', this.codigo);
        let data = { 'filtros': filtros, id_usuario: user.id_usuario, id_perfil: user.id_perfil };
        const sequence$ = this._httpClient.post<Cuenta>(this.base_url + "cotizacion/contar", data, { headers: headerss.set('tkn-cliente', this.codigo) })
            .switchMap(cuenta => {
                let total = cuenta.total;
                let paginas = Math.ceil(total / 10000);
                const aux = [];
                for (let index = 1; index <= paginas; index++) {
                    let req = this._httpClient.post<Lista>(this.base_url + "cotizacion/lista_excel/" + index, data, { headers: headerss.set('tkn-cliente', this.codigo) });
                    aux.push(req);
                }
                return aux;
            });
        return sequence$;
    }

    contar(filtros, user): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'filtros': filtros, id_usuario: user.id_usuario, id_perfil: user.id_perfil };
        return this._http.post(this.base_url + "cotizacion/contar", data, { headers: headerss });
    }

    lista_reasignar(filtros, pagina): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'filtros': filtros };
        return this._http.post(this.base_url + "cotizacion/lista_reasignar/" + pagina, data, { headers: headerss });
    }

    lista_duplicadas(id_cotizacion, id_prospecto): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'id_cotizacion': id_cotizacion, 'id_prospecto': id_prospecto };
        return this._http.post(this.base_url + "cotizacion/lista_duplicadas", data, { headers: headerss });
    }

    activar_gestion(data): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "cotizacion/activar", data, { headers: headerss });
    }

    desactivar_gestion(data): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "cotizacion/desbloquear", data, { headers: headerss });
    }

    ver(id, data): Observable<any> {
        let headerss = this.getHeaders();


        return this._http.post(this.base_url + 'cotizacion/ver/' + id, JSON.stringify(data), { headers: headerss });
    }

    reasignar(lista_cotizaciones, id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { 'cotizaciones': lista_cotizaciones, id_operador: id };

        return this._http.post(this.base_url + 'cotizacion/reasignar', JSON.stringify(data), { headers: headerss });
    }

    agregar(prosp, cot, user): Observable<any> {
        let headerss = this.getHeaders();
        let data = { cotizacion: cot, prospecto: prosp, usuario: user.id_usuario, id_perfil: user.id_perfil };

        return this._http.post(this.base_url + 'cotizacion/agregar', JSON.stringify(data), { headers: headerss });
    }

    duplicar(id, id_usuario): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "id_cotizacion": id, "id_usuario": id_usuario }
        return this._http.post(this.base_url + "cotizacion/duplicar", data, { headers: headerss });
    }

    recordar_visita(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { "id_cotizacion": id }
        return this._http.post(this.base_url + "cotizacion/recordar_visita", data, { headers: headerss });
    }
}