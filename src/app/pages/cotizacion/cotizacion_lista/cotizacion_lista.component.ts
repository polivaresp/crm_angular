import { Component } from '@angular/core';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import * as moment from 'moment';

import { CotizacionService } from '../services/cotizacion.service';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { GestionListaModalComponent } from '../../gestion/modals/gestion_lista.modal';
import { DespachadorService } from '../../despachador/services/despachador.service';
import { ConfirmarDuplicadaModalComponent } from '../modals/duplicar_cotizacion/confirmar_duplicada.modal';
import { ExcelService } from 'src/app/services/excel.service';
import { FiltrosModalComponent } from '../modals/filtros/filtros.modal';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'ngx-cotizacion_lista',
  templateUrl: './cotizacion_lista.component.html',
  /* styleUrls: ['../../pages.component.scss'], */
  providers: [CotizacionService, ExcelService, DespachadorService]
})
export class CotizacionListaComponent {

  user: any;
  acciones: any;
  etiquetas: any;

  cotizaciones: Array<any> = [];
  previusPage: any;
  pagina_actual: number = 1;
  cantidad: number = 0;
  cantidad_pagina: number = 0;
  lista: number;

  lista_excel: Array<any> = [];
  total_excel: number = 0;
  excel_listo: number = 0;
  export_loading: number = 0;
  excel_loading: number = 0;
  tiene_filtro: any = 0;


  constructor(private _cotizacionService: CotizacionService, private router: Router,
    private _modalService: NbDialogService, private _modalMaterialService: NgbModal,
    private _toasterService: ToasterService, private _excelService: ExcelService,
    private _despachadorService: DespachadorService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.acciones = JSON.parse(localStorage.getItem('acciones'));
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }

  exportar_excel(tipo = "xlsx") {
    this.export_loading = 1;
    let array = [];
    this.lista_excel.forEach(ele => {
      ele.forEach(e => {
        let antec = e.antecedentes;
        let com_c = e.comentario_cotizacion;
        let com_g = e.comentario_gestion;
        if (antec && antec != "" && antec != null) {
          antec = antec.replace(/\n/gi, ">");
        }
        if (com_c && com_c != "" && com_c != null) {
          com_c = com_c.replace(/\n/gi, ">");
        }
        if (com_g && com_g != "" && com_g != null) {
          com_g = com_g.replace(/\n/gi, ">");
        }
        e.antecedentes = antec;
        e.comentario_cotizacion = com_c;
        e.comentario_gestion = com_g;
        array.push(e);
      });
    });
    if (tipo == "xlsx") {
      this._excelService.exportAsExcelFile(array, "Cotizaciones");
    } else if (tipo == "csv") {
      this._excelService.exportAsCsvFile(array, "Cotizaciones");
    }
    this.excel_listo = 0;
    this.export_loading = 0;
  }

  exportar_lista() {
    let filtros = localStorage.getItem('filtros-ctz') ? JSON.parse(localStorage.getItem('filtros-ctz')) : null;
    this.lista_excel = [];
    this.excel_listo = 0;
    this.excel_loading = 1;
    var total = 0;
    this._cotizacionService.excel_lista(filtros, this.user).subscribe(resultado => {
      resultado.forEach(element => {
        if (element.lista) {
          this.lista_excel[element.pagina - 1] = element.lista;
          total = total + 1;
        }
        this.excel_listo = element.total_pag == total ? 1 : 0;
        this.excel_loading = element.total_pag == total ? 0 : 1;
      });
    });
  }

  agregar() {
    this.router.navigate(['pages/cotizacion/agregar']);
  }

  submit(reiniciar_pagina = 0) {
    this.excel_listo = 0;
    this.export_loading = 0;
    if (reiniciar_pagina == 1) {
      this.pagina_actual = 1;
    }
    this.cotizaciones = [];

    let filtros = localStorage.getItem('filtros-ctz') ? JSON.parse(localStorage.getItem('filtros-ctz')) : null;
    this.tiene_filtro = localStorage.getItem('tiene-filtro-lista') ? localStorage.getItem('tiene-filtro-lista') : 0;

    this._cotizacionService.lista(filtros, this.lista, this.pagina_actual, this.user).subscribe(response => {
      if (response.resultado == "ACK") {
        this.cantidad = response.lista.length > 0 ? response.cantidad : 0;
        this.cotizaciones = response.lista.length > 0 ? response.lista : [];
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
      } else {
        this.showToast('warning', 'Problemas!', response.mensaje);
      }
    }, err => {
      this.showToast('error', 'Error!', 'Error Interno');
    });

  }

  getDate(fecha, format) {
    if (fecha) {
      if (fecha != '') {
        let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
        return x;
      }
    }
    return '-';
  }

  changeLista($event) {
    let titulo = $event.tabTitle;
    if (titulo == "En Proceso") {
      this.lista = 1;
    } else if (titulo == "Agendamientos") {
      this.lista = 2;
    } else if (titulo == "Terminada") {
      this.lista = 3;
    } else {
      this.lista = 1;
    }
    this.submit(1);
  }

  gestionar(cot) {
    let data = { id_cotizacion: cot.id_cotizacion, id_usuario: this.user.id_usuario, id_perfil: this.user.id_perfil };
    this._cotizacionService.activar_gestion(data).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast('success', 'Exitoso', 'Inicio de Gestión');
        this.router.navigate(['pages/gestion/editar/' + cot.id_prospecto + '/' + cot.id_cotizacion]);
      } else {
        this.showToast('error', 'Error!', 'No se pudo activar la gestion');
      }
    }, err => {
      this.showToast('error', 'Error!', 'No se pudo activar la gestion');
    });
  }

  ver(cot) {
    this.router.navigate(['pages/cotizacion/ver/' + cot.id_prospecto + '/' + cot.id_cotizacion]);
  }

  desbloquear_gestion(cot) {
    let data = { id_cotizacion: cot.id_cotizacion, id_usuario: this.user.id_usuario, id_perfil: this.user.id_perfil };
    this._cotizacionService.desactivar_gestion(data).subscribe(response => {
      if (response.resultado == "ACK") {
        this.submit();
        this.showToast('success', 'Exitoso!', response.mensaje);
      } else {
        this.showToast('error', 'Error!', response.mensaje);
      }
    }, err => {
      this.showToast('error', 'Error!', 'Problemas al desbloquear gestion');
    });
  }

  enviar_no_contacto(obj) {
    this._despachadorService.no_contacto(obj.id_cotizacion, this.user.id_usuario).subscribe(response => {
      if (response.resultado == "ACK") {
        this.submit();
        this.showToast('success', 'Exitoso!', response.mensaje);
      } else {
        this.showToast('error', 'Error!', response.mensaje);
      }
    }, err => {
      this.showToast('error', 'Error!', err);
    });
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

  showGestiones(cot) {
    const modalRef = this._modalMaterialService.open(GestionListaModalComponent, { scrollable: true, size: 'lg' });
    modalRef.componentInstance.id_cotizacion = cot.id_cotizacion;
  }

  recordar_visita(cot) {
    this._cotizacionService.recordar_visita(cot.id_cotizacion).subscribe(res => {
      if (res.resultado == "ACK") {
        this.showToast('success', 'Recordatorio Activado', res.mensaje);
        this.submit(1);
      } else {
        this.showToast('error', 'Error', res.mensaje);
      }
    }, err => {
      this.showToast('error', 'Error Interno', err);
    })
  }

  confirmar_duplicada(cot) {
    const activeModal = this._modalService.open(ConfirmarDuplicadaModalComponent, {
      context: {
        modalHeader: 'Duplicar Cotizacion: ' + cot.id_cotizacion,
        id: cot.id_cotizacion,
        id_usuario: this.user.id_usuario
      }
    });
    activeModal.onClose.subscribe(result => {
      if (result) {
        this.showToast('success', 'Registro Duplicado', 'Espere mientras comienza la gestión');
        console.log(result);
        this.router.navigate(['pages/gestion/editar/' + result.id_prospecto + '/' + result.id_cotizacion]);
      } else {
        console.log("closed")
      }
    });
  }

  despachos(obj) {
    this.router.navigate(['pages/despachador/lista/' + obj.id_cotizacion]);
  }

  filtrosModal() {
    const activeModal2 = this._modalService.open(FiltrosModalComponent, {
      context: {
        panel: "lista"
      }
    });
    activeModal2.onClose.subscribe(result => {
      if (result) {
        this.submit(1);
      } else {
        console.log("closed");
      }
    });
  }

}
