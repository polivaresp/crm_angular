import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BaseService } from '../../../services/index';
import { CotizacionService } from '../../cotizacion/services/cotizacion.service';
import { GestionService } from '../../gestion/services/gestion.service';
import { ProspectoService } from '../../prospecto/services/prospecto.service';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import { GestionListaModalComponent } from '../../gestion/modals/gestion_lista.modal'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'ngx-ver-cotizacion',
    templateUrl: './ver_cotizacion.component.html',
    providers: [BaseService, CotizacionService, GestionService, ProspectoService]
})
export class VerCotizacionComponent {
    id_prospecto: any;
    id_cotizacion: any;
    user: any;
    cotizacion: any;
    prospecto: any;
    telefonos_prospecto: any[];
    emails_prospecto: any[];
    camposvariables: any;
    etiquetas: any;

    constructor(private _route: ActivatedRoute, private _router: Router, 
        private _cotizacionService: CotizacionService, private _prospectoService: ProspectoService,
        private _modalMaterialService: NgbModal, private _toasterService: ToasterService) {

        this._route.params.forEach((params: Params) => {
            this.id_prospecto = params['id_prospecto'];
            this.id_cotizacion = params['id_cotizacion'];
        });
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.cotizacion = {};
        this.prospecto = {};
        this.camposvariables = [];

        this.telefonos_prospecto = [];
        this.emails_prospecto = [];
    }

    ngOnInit() {
        this._cotizacionService.ver(this.id_cotizacion, {}).subscribe(res => {
            if (res.resultado == "ACK") {
                this.cotizacion = res.cotizacion;
            } else {
                location.reload();
                this.showToast("error", "Error", "Cotizacion no encontrada");
            }
        }, error => {
            location.reload();
            this.showToast("error", "Error", "Error interno al cargar cotizacion");
        });
        this._prospectoService.ver_completo(this.id_prospecto).subscribe(res => {
            if (res.resultado == "ACK") {
                this.prospecto = res.prospecto;
                let telefonos = res.prospecto.telefonos != "" ? res.prospecto.telefonos : '';
                let emails = res.prospecto.emails != "" ? res.prospecto.emails : '';
                this.telefonos_prospecto = telefonos.split(',');
                this.emails_prospecto = emails.split(',');
            } else {
                this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
                location.reload();
                this.showToast("error", "Error", "Error al cargar prospecto para formulario");
            }
        }, error => {
            this._router.navigate(['pages/gestion/editar/' + this.id_prospecto + '/' + this.id_cotizacion]);
            location.reload();
            this.showToast("error", "Error", "Error interno para cargar prospecto");
        });
        this.pre_cargar_selectores();
        window.scrollTo(0, 0);
    }

    pre_cargar_selectores() {
        //cargar camposvariables
        this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.camposvariables = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar camposvariables");
                }
            }, (err) => {
                this.showToast("error", "Error", "camposvariables")
            });
    }

    salir_sin_guardar() {
        this._router.navigate(['pages/cotizacion/lista']);
    }

    //modulos
    showGestiones() {
        const modalRef = this._modalMaterialService.open(GestionListaModalComponent, { scrollable: true, size:'lg' });
        modalRef.componentInstance.id_cotizacion = this.id_cotizacion;
    }

    private showToast(type: string, title: string, body: string) {
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}
