import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CotizacionRoutingModule, routedComponents } from './cotizacion-routing.module';

import { ReasignarModalComponent } from './modals/reasignar/reasignar.modal';
import { FiltrosModalComponent } from './modals/filtros/filtros.modal';
import { ConfirmarDuplicadaModalComponent } from './modals/duplicar_cotizacion/confirmar_duplicada.modal';
import { NbCardModule, NbTabsetModule, NbCheckboxModule, NbButtonModule, NbDialogModule, NbListModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    CotizacionRoutingModule,
    NgbPaginationModule,
    NbTabsetModule,
    NbCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule,
    NgbModule,
    NbDialogModule.forChild()
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
  entryComponents: [
    FiltrosModalComponent,
    ReasignarModalComponent,
    ConfirmarDuplicadaModalComponent
  ]
})
export class CotizacionModule { }
