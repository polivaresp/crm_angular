import { Component } from '@angular/core';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import * as moment from 'moment';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from '../../../usuario/services/usuario_service';
import { CotizacionService } from '../../services/cotizacion.service';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-modal',
  templateUrl: './reasignar.modal.html',
  providers: [UsuarioService, CotizacionService]
})
export class ReasignarModalComponent {

  cotizaciones;
  modalHeader: string;
  id_usuario: number;
  formReasigna: FormGroup;
  operadores;
  lista_seleccionadas;

  data: any;


  constructor(private activeModal: NbDialogRef<ReasignarModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
    private _usuarioService: UsuarioService, private _cotizacionService: CotizacionService) {
    this.formReasigna = fb.group({
      id_operador: ['', Validators.required]
    });
    this.data = {};
    this.operadores = [];
    this.lista_seleccionadas = [];
  }

  ngOnInit() {
    //operadores
    this._usuarioService.lista_gestionables([2, 3])
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.operadores = response.lista;
        } else {
          console.log("error a cargar operadores");
        }
      }, function (err) {
        console.log(err)
      });
  }

  reasignar() {
    this._cotizacionService.reasignar(this.lista_seleccionadas, this.formReasigna.value.id_operador).subscribe(response => {
      if (response.resultado == "ACK") {
        this.data = response;
        this.closeModal(1);
      } else {
        console.log(response.mensaje);
      }
    }, err => {
      console.log(err);
    });
  }


  closeModal(tipo = 2) {
    if (tipo == 1) {
      this.activeModal.close(this.data);
    } else {
      this.activeModal.close();
    }
  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
