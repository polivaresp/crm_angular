import { Component } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { CotizacionService } from '../../services/cotizacion.service';

@Component({
  selector: 'ngx-modal',
  templateUrl: './duplicadas.modal.html',
  providers: [CotizacionService]
})
export class DuplicadasModalComponent {

  modalHeader: string;
  public id_cotizacion: number;
  public id_prospecto: number;
  public duplicadas = [];
  etiquetas: any;


  constructor(private activeModal: NbDialogRef<DuplicadasModalComponent>, private _cotizacionService: CotizacionService, private _toasterService: ToasterService) {
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.duplicadas = [];
  }

  ngOnInit() {
    this._cotizacionService.lista_duplicadas(this.id_cotizacion, this.id_prospecto).subscribe(res => {
      if (res.resultado == "ACK") {
        this.duplicadas = res.lista;
      } else {
        this.showToast("error", "Error", "Error al traer duplicadas");
      }
    }, error => {
      this.showToast("error", "Error", "Error interno");
    });
  }


  closeModal() {
    this.activeModal.close();
  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
