import { Component } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { FormBuilder } from '@angular/forms';
import { BaseService } from '../../../../services/base.service';
import { UsuarioService } from '../../../usuario/services/usuario_service';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-filtros-modal',
  templateUrl: './filtros.modal.html',
  providers: [BaseService, UsuarioService]
})
export class FiltrosModalComponent {

  panel;
  tiene_filtro: any;
  data;
  fecha_termino;
  fecha_inicio;
  filtros;
  user: any;
  operadores;
  estados;
  etapas;
  sucursales;
  items;
  origenes;
  campanias;
  etiquetas: any;
  fromDate: any;
  toDate: any;
  ultima_gestion: any;
  primer_contacto: any;

  constructor(private activeModal: NbDialogRef<FiltrosModalComponent>, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService, private _usuarioService: UsuarioService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.filtros = []; // filtros
    this.origenes = []; //origenes
    this.campanias = [];// camapañas
    this.items = [];// items
    this.sucursales = [];
    this.etapas = [];
    this.estados = [];
    this.tiene_filtro = 0;
  }

  ngOnInit() {
    this.prellenar_filtros();
    this.cargar_filtros();
  }

  cargar_filtros() {
    let filtros;
    let now = new Date();
    if (this.panel == "lista") {
      filtros = localStorage.getItem('filtros-ctz') ? JSON.parse(localStorage.getItem('filtros-ctz')) : null;
    } else if (this.panel == "reasignar") {
      filtros = localStorage.getItem('filtros-reasignar') ? JSON.parse(localStorage.getItem('filtros-reasignar')) : null;
    }
    if (filtros != null) {
      if (filtros.fecha_inicio != "") {
        let f_ini = new Date(filtros.fecha_inicio.split('-'));
        this.fecha_inicio = { year: f_ini.getFullYear(), month: f_ini.getMonth() + 1, day: f_ini.getDate() };
      } /* else {
        let f_ini = new Date();
        this.fecha_inicio = { year: f_ini.getFullYear(), month: f_ini.getMonth() + 1, day: 1 };
      } */
      if (filtros.fecha_termino != "") {
        let f_fin = new Date(filtros.fecha_termino.split('-'));
        this.fecha_termino = { year: f_fin.getFullYear(), month: f_fin.getMonth() + 1, day: f_fin.getDate() };
      } /* else {
        let f_fin = new Date();
        this.fecha_termino = { year: f_fin.getFullYear(), month: f_fin.getMonth() + 1, day: f_fin.getDate() };
      } */
      if (filtros.ultima_gestion != "") {
        let f_gestion = new Date(filtros.ultima_gestion.split('-'));
        this.ultima_gestion = { year: f_gestion.getFullYear(), month: f_gestion.getMonth() + 1, day: f_gestion.getDate() };
      }
      if (filtros.primer_contacto != "") {
        let f_contacto = new Date(filtros.primer_contacto.split('-'));
        this.primer_contacto = { year: f_contacto.getFullYear(), month: f_contacto.getMonth() + 1, day: f_contacto.getDate() };
      }
      this.filtros["fecha_inicio"] = filtros.fecha_inicio != "" ? filtros.fecha_inicio : "";
      this.filtros["fecha_termino"] = filtros.fecha_termino != "" ? filtros.fecha_termino : "";
      this.filtros["ultima_gestion"] = filtros.ultima_gestion != "" ? filtros.ultima_gestion : "";
      this.filtros["primer_contacto"] = filtros.primer_contacto != "" ? filtros.primer_contacto : "";
      /* this.change_fecha_ini();
      this.change_fecha_fin(); */
      this.filtros["id_cotizacion"] = filtros.id_cotizacion ? filtros.id_cotizacion : "";
      this.filtros["rut"] = filtros.rut ? filtros.rut : "";
      this.filtros["nombre"] = filtros.nombre ? filtros.nombre : "";
      this.filtros["apellido"] = filtros.apellido ? filtros.apellido : "";
      this.filtros["emails"] = filtros.emails ? filtros.emails : "";
      this.filtros["telefonos"] = filtros.telefonos ? filtros.telefonos : "";
      this.filtros["origen"] = filtros.origen ? filtros.origen : "";
      this.filtros["campania"] = filtros.campania ? filtros.campania : "";
      this.filtros["item"] = filtros.item ? filtros.item : "";
      this.filtros["operador"] = filtros.operador ? filtros.operador : "";
      this.filtros["sucursal"] = filtros.sucursal ? filtros.sucursal : "";
      this.filtros["categoria"] = filtros.categoria ? filtros.categoria : "";
      this.filtros["etapa"] = filtros.etapa ? filtros.etapa : "";
      this.filtros["estado"] = filtros.estado ? filtros.estado : "";
    } else {
      /* this.fecha_inicio = { year: now.getFullYear(), month: now.getMonth() + 1, day: 1 };
      this.fecha_termino = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }; */
      this.ultima_gestion = "";
      this.primer_contacto = "";
      this.filtros["fecha_inicio"] = "";
      this.filtros["fecha_termino"] = "";
      this.filtros["ultima_gestion"] = "";
      this.filtros["primer_contacto"] = "";
      this.filtros["id_cotizacion"] = "";
      this.filtros["rut"] = "";
      this.filtros["nombre"] = "";
      this.filtros["apellido"] = "";
      this.filtros["emails"] = "";
      this.filtros["telefonos"] = "";
      this.filtros["origen"] = "";
      this.filtros["campania"] = "";
      this.filtros["item"] = "";
      this.filtros["operador"] = "";
      this.filtros["sucursal"] = "";
      this.filtros["categoria"] = "";
      this.filtros["etapa"] = "";
      this.filtros["estado"] = "";
      /* this.change_fecha_ini();
      this.change_fecha_fin(); */
    }
  }

  limpiar_filtros() {
    let now = new Date();
    /* this.fecha_inicio = { year: now.getFullYear(), month: now.getMonth() + 1, day: 1 };
    this.change_fecha_ini();
    this.fecha_termino = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.change_fecha_fin(); */
    this.filtros["fecha_inicio"] = "";
    this.filtros["fecha_termino"] = "";
    this.ultima_gestion = "";
    this.primer_contacto = "";
    this.filtros["ultima_gestion"] = "";
    this.filtros["primer_contacto"] = "";
    this.filtros["id_cotizacion"] = "";
    this.filtros["rut"] = "";
    this.filtros["nombre"] = "";
    this.filtros["apellido"] = "";
    this.filtros["emails"] = "";
    this.filtros["telefonos"] = "";
    this.filtros["origen"] = "";
    this.filtros["campania"] = "";
    this.filtros["item"] = "";
    this.filtros["sucursal"] = "";
    this.filtros["operador"] = "";
    this.filtros["categoria"] = "";
    this.filtros["etapa"] = "";
    this.filtros["estado"] = "";
    this.submit();
  }

  submit() {
    let filtros = {
      fecha_inicio: this.filtros["fecha_inicio"] != "" ? this.filtros["fecha_inicio"] : "",
      fecha_termino: this.filtros["fecha_termino"] != "" ? this.filtros["fecha_termino"] : "",
      ultima_gestion: this.filtros["ultima_gestion"] != "" ? this.filtros["ultima_gestion"] : "",
      primer_contacto: this.filtros["primer_contacto"] != "" ? this.filtros["primer_contacto"] : "",
      id_cotizacion: this.filtros["id_cotizacion"],
      origen: this.filtros["origen"] != "" ? this.filtros["origen"] : "",
      campania: this.filtros["campania"] != "" ? this.filtros["campania"] : "",
      item: this.filtros["item"] != "" ? this.filtros["item"] : "",
      sucursal: this.filtros["sucursal"] != "" ? this.filtros["sucursal"] : "",
      operador: this.filtros["operador"] != "" ? this.filtros["operador"] : "",
      categoria: this.filtros["categoria"] != "" ? this.filtros["categoria"] : "",
      etapa: this.filtros["etapa"] != "" ? this.filtros["etapa"] : "",
      estado: this.filtros["estado"] != "" ? this.filtros["estado"] : "",
      rut: this.filtros["rut"],
      nombre: this.filtros["nombre"],
      apellido: this.filtros["apellido"],
      emails: this.filtros["emails"],
      telefonos: this.filtros["telefonos"],
    };

    this.tiene_filtro = this.filtros["fecha_inicio"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["fecha_termino"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["ultima_gestion"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["primer_contacto"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["id_cotizacion"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["origen"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["campania"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["item"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["sucursal"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["operador"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["categoria"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["etapa"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["estado"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["rut"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["nombre"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["apellido"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["emails"] != "" ? 1 : this.tiene_filtro;
    this.tiene_filtro = this.filtros["telefonos"] != "" ? 1 : this.tiene_filtro;


    if (this.panel == "lista") {
      localStorage.setItem('filtros-ctz', JSON.stringify(filtros));
      localStorage.setItem('tiene-filtro-lista', JSON.stringify(this.tiene_filtro));
    } else if (this.panel == "reasignar") {
      localStorage.setItem('filtros-reasignar', JSON.stringify(filtros));
      localStorage.setItem('tiene-filtro-reasignar', JSON.stringify(this.tiene_filtro));
    } else {
      localStorage.setItem('filtros-ctz', JSON.stringify({}));
      localStorage.setItem('tiene-filtro-lista', JSON.stringify(0));
      localStorage.setItem('filtros-reasignar', JSON.stringify({}));
      localStorage.setItem('tiene-filtro-reasignar', JSON.stringify(0));
    }
    this.data = filtros;
    this.closeModal(1);
  }

  prellenar_filtros() {
    //cargar campañas
    this._baseService.lista_simple('campania', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.campanias = response.lista;
        } else {
          console.log("error a cargar campaña");
        }
      }, function (err) {
        console.log(err)
      });
    //cargar origen
    this._baseService.lista_simple('origen', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.origenes = response.lista;
        } else {
          console.log("error a cargar origen");
        }
      }, function (err) {
        console.log(err)
      });
    //cargar item
    this._baseService.lista_simple('item', { activo: 1 }, ["glosa_item", "asc"])
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.items = response.lista;
        } else {
          console.log("error a cargar item");
        }
      }, function (err) {
        console.log(err)
      });
    //cargar sucursal
    this._baseService.lista_simple('sucursal', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.sucursales = response.lista;
        } else {
          console.log("error a cargar sucursal");
        }
      }, function (err) {
        console.log(err)
      });
    //operadores
    this._usuarioService.lista_gestionables([2, 3])
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.operadores = response.lista;
        } else {
          console.log("error a cargar operadores");
        }
      }, function (err) {
        console.log(err)
      });
    //cargar etapa
    this._baseService.lista_simple('etapa', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.etapas = response.lista;
        } else {
          console.log("error a cargar etapas");
        }
      }, function (err) {
        console.log("ayayaisnotify");

      });
    //cargar sucursal
    this._baseService.lista_simple('estado', { activo: 1 })
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.estados = response.lista;
        } else {
          console.log("error a cargar estado");
        }
      }, function (err) {
        console.log(err)
      });
  }

  change_fecha_ini(event = null) {
    let fecha = event ? event : this.fecha_inicio;
    if (fecha.year) {
      this.filtros["fecha_inicio"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      this.filtros["fecha_inicio"] = '';
    }
  }

  change_fecha_fin(event = null) {
    let fecha = event ? event : this.fecha_termino;
    if (fecha.year) {
      this.filtros["fecha_termino"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      this.filtros["fecha_termino"] = '';
    }
  }

  change_ultima_gestion(event = null) {
    let fecha = event ? event : this.ultima_gestion;
    if (fecha.year) {
      this.filtros["ultima_gestion"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      this.filtros["ultima_gestion"] = '';
    }
  }

  change_primer_contacto(event = null) {
    let fecha = event ? event : this.primer_contacto;
    if (fecha.year) {
      this.filtros["primer_contacto"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      this.filtros["primer_contacto"] = '';
    }
  }

  closeModal(tipo = 2) {
    if (tipo == 1) {
      this.activeModal.close(this.data);
    } else {
      this.activeModal.close();
    }
  }

}