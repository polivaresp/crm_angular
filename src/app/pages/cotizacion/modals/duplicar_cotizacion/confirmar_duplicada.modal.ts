import { Component } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CotizacionService } from '../../services/cotizacion.service';

@Component({
    selector: 'ngx-modal',
    templateUrl: './confirmar_duplicada.modal.html',
    providers: [CotizacionService]
})
export class ConfirmarDuplicadaModalComponent {

    data: any;
    id;
    id_usuario;
    modalHeader: string;


    constructor(private activeModal: NbDialogRef<ConfirmarDuplicadaModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
        private _cotizacionService: CotizacionService) {
        this.data = {};
    }

    ngOnInit() { }

    submit() {
        this._cotizacionService.duplicar(this.id, this.id_usuario).subscribe(res => {
            if (res.resultado == "ACK") {
                this.data = res.cotizacion;
                this.closeModal(1);
            } else {
                this.showToast('error', 'Error', res.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error Interno', err);
        });
    }

    closeModal(tipo = 2) {
        if (tipo == 1) {
            this.activeModal.close(this.data);
        } else {
            this.activeModal.close();
        }
    }

    private showToast(type: string, title: string, body: string) {
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}