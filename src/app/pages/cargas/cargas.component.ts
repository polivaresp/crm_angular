import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'ngx-cargas',
  template: `<router-outlet></router-outlet>`
})
export class CargasComponent {
}
