import { Component } from '@angular/core';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BaseService } from '../../../services';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CargasService } from '../services/cargas.service';
import { ProspectoService } from '../../prospecto/services/prospecto.service';

@Component({
    selector: 'ngx-editar-cargas',
    templateUrl: './editar_cargas.component.html',
    providers: [CargasService, ProspectoService, BaseService]
})
export class EditarCargasComponent {

    user: any;
    campanias: any[];
    origenes: any;
    camposvariables: any;
    form: FormGroup;
    id: any;

    constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _toasterService: ToasterService,
        private _cargasService: CargasService, private _baseService: BaseService, private _prospectoService: ProspectoService) {
        this._route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.form = fb.group({
            origen: [{ value: '', disabled: true }, Validators.required],
            campania: ['', Validators.required],
            rut: ['', Validators.required],
            nombre: ['', Validators.required],
            apellidos: ['', Validators.required],
            telefono: '',
            correo: '',
            renta: '',
            interes: ['', Validators.required],
            comentario: '',
            dato_0: '',
            dato_1: '',
            dato_2: '',
            dato_3: '',
            dato_4: '',
            dato_5: '',
            dato_6: '',
            dato_7: '',
            dato_8: '',
            dato_9: '',
        });
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    }

    ngOnInit() {
        //cargar camposvariables
        this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.camposvariables = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar camposvariables");
                }
            }, (err) => {
                this.showToast("error", "Error", "camposvariables")
            });
        //cargar origen
        this._baseService.lista_simple('origen', { activo: 1 })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.origenes = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar origen");
                }
            }, (err) => {
                this.showToast("error", "Error", "origen")
            });
        //cargar campania
        this._baseService.lista_simple('campania', { activo: 1 })
            .subscribe(response => {
                if (response.resultado == "ACK") {
                    this.campanias = response.lista;
                } else {
                    this.showToast("error", "Error", "error a cargar campania");
                }
            }, (err) => {
                this.showToast("error", "Error", "campania")
            });
        this.cargar_form();
    }

    cargar_form() {
        this._cargasService.ver(this.id).subscribe(response => {
            if (response.resultado == "ACK") {
                this.form.controls['origen'].setValue(response.modelo["origen"]);
                this.form.controls['campania'].setValue(response.modelo["campania"]);
                this.form.controls['rut'].setValue(response.modelo["rut"]);
                this.form.controls['nombre'].setValue(response.modelo["nombre"]);
                this.form.controls['apellidos'].setValue(response.modelo["apellidos"]);
                this.form.controls['telefono'].setValue(response.modelo["telefono"]);
                this.form.controls['correo'].setValue(response.modelo["correo"]);
                this.form.controls['renta'].setValue(response.modelo["renta"]);
                this.form.controls['interes'].setValue(response.modelo["interes"]);
                this.form.controls['comentario'].setValue(response.modelo["comentario"]);
                this.form.controls['dato_0'].setValue(response.modelo["dato_0"] ? response.modelo["dato_0"] : '');
                this.form.controls['dato_1'].setValue(response.modelo["dato_1"] ? response.modelo["dato_1"] : '');
                this.form.controls['dato_2'].setValue(response.modelo["dato_2"] ? response.modelo["dato_2"] : '');
                this.form.controls['dato_3'].setValue(response.modelo["dato_3"] ? response.modelo["dato_3"] : '');
                this.form.controls['dato_4'].setValue(response.modelo["dato_4"] ? response.modelo["dato_4"] : '');
                this.form.controls['dato_5'].setValue(response.modelo["dato_5"] ? response.modelo["dato_5"] : '');
                this.form.controls['dato_6'].setValue(response.modelo["dato_6"] ? response.modelo["dato_6"] : '');
                this.form.controls['dato_7'].setValue(response.modelo["dato_7"] ? response.modelo["dato_7"] : '');
                this.form.controls['dato_8'].setValue(response.modelo["dato_8"] ? response.modelo["dato_8"] : '');
                this.form.controls['dato_9'].setValue(response.modelo["dato_9"] ? response.modelo["dato_9"] : '');
                //this.showToast("success","Exito",response.mensaje);
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    submit() {
        let campos = this.form.value;
        /* campos.rut = this.validar_rut(this.form.value.rut);
        if(campos.rut){ */
        this._cargasService.editar(campos, this.id, this.user.id_usuario).subscribe((response) => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Guardado", response.mensaje);
                this._router.navigate(['pages/cargas/lista']);
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, (error) => {
            this.showToast("error", "Error", error);
        })
        /* }else{
            this.showToast("error", "Error de Rut", "Rut NO valido");
        } */
    }

    salir_sin_guardar() {
        this._router.navigate(['pages/cargas/lista']);
    }

    validar_rut(rut) {
        var x_rut = rut.split('.').join('');
        x_rut = x_rut.split('-').join('');
        x_rut = x_rut.split(',').join('');
        x_rut = x_rut.split('/').join('');
        x_rut = x_rut.split('_').join('');
        x_rut = x_rut.split('—').join('');
        x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
        var rut_split = x_rut.split('-');
        var k = 2;
        var total = 0;
        for (let i = rut_split[0].length - 1; i >= 0; i--) {
            if (k > 7) {
                k = 2;
            }
            total += (k * rut_split[0][i]);
            k += 1;
        }
        var resto = total % 11;
        var verificador = 11 - resto;
        var x_verificador = verificador == 10 ? 'k' : verificador == 11 ? 0 : verificador;
        if (rut_split[1] == x_verificador) {
            return x_rut;
        } else {
            return false;
        }
    }

    arreglar_rut(rut) {
        var x_rut = rut.split('.').join('');
        x_rut = x_rut.split('-').join('');
        x_rut = x_rut.split(',').join('');
        x_rut = x_rut.split('/').join('');
        x_rut = x_rut.split('_').join('');
        x_rut = x_rut.split('—').join('');
        x_rut = [x_rut.slice(0, x_rut.length - 1), '-', x_rut.slice(x_rut.length - 1)].join('');
        return x_rut;
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }
}