import { Component } from '@angular/core';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { BaseService } from '../../../services';
import { CargasService } from '../services/cargas.service';

@Component({
    selector: 'ngx-cargas-lista',
    templateUrl: './cargas_lista.component.html',
    providers: [CargasService]
})
export class CargasListaComponent {

    cantidad: number;
    pagina_actual: number;
    lista;
    form;
    acciones: any;
    cantidad_pagina: any;
    previusPage: number;


    constructor(private _router: Router, private _toasterService: ToasterService, private _cargasService: CargasService) {
        this.lista = [];
        this.form = [];
        this.form["id"] = '';
        this.form["rut"] = '';
        this.form["nombre"] = '';
        this.form["procesado"] = "0";
        this.pagina_actual = 1;
        this.cantidad = 0;
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
    }

    ngOnInit() {
        this.submit();
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        let form = {};
        Object.assign(form, this.form);
        this._cargasService.lista(form, ["id", "ASC"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
            } else {
                this.showToast('error', 'Error Interno', response.mensaje);
            }
        }, error => {
            this.showToast('error', 'Error Interno', error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/cargas/editar/' + obj.id]);
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }
}