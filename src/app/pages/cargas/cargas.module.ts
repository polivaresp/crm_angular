import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CargasRoutingModule, routedComponents } from './cargas-routing.module';
import { SelectorCamposModalComponent } from './modals/selector_campos/selector_campos.modal';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbButtonModule, NbCheckboxModule, NbDialogModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    ThemeModule,
    CargasRoutingModule,
    NgbPaginationModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule,
    NbDialogModule.forChild()
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
  entryComponents: [
    SelectorCamposModalComponent
  ],
})
export class CargasModule { }
