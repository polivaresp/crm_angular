import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CargasComponent } from './cargas.component';
import { CargasListaComponent } from './lista/cargas_lista.component';
import { EditarCargasComponent } from './editar/editar_cargas.component';
import { CargaMasivaComponent } from './carga_masiva/carga_masiva.component';
import { SelectorCamposModalComponent } from './modals/selector_campos/selector_campos.modal';
const routes: Routes = [
  {
    path: '',
    component: CargasComponent,
    children: [
      {
        path: 'lista',
        component: CargasListaComponent,
      },
      {
        path: 'editar/:id',
        component: EditarCargasComponent,
      },
      {
        path: 'masiva',
        component: CargaMasivaComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CargasRoutingModule { }

export const routedComponents = [
  CargasComponent,
  CargasListaComponent,
  EditarCargasComponent,
  CargaMasivaComponent,
  SelectorCamposModalComponent
];
