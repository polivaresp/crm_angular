import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CargasService {

    public base_url: string;
    public codigo: string;
    public config;
    user;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }


    lista(post = {}, orden = ['orden', 'asc'], pagina): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "cargas/lista/" + pagina, { post, orden }, { headers: headerss });
    }

    ver(id): Observable<any> {
        let headerss = this.getHeaders();

        return this._http.post(this.base_url + 'cargas/ver/' + id, JSON.stringify({}), { headers: headerss });
    }

    editar(form, id, id_usuario): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'campos': form, 'id_usuario': id_usuario };

        return this._http.post(this.base_url + 'cargas/editar/' + id, data, { headers: headerss });
    }

    verificar(array): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'data': array };

        return this._http.post(this.base_url + 'cargas/validar', data, { headers: headerss });
    }

    subir(headers, array, id_campania): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'cabeceras': headers, 'data': array, 'campania': id_campania, 'id_usuario': this.user.id_usuario };

        return this._http.post(this.base_url + 'cargas/subir', data, { headers: headerss });
    }

    exportar_ejemplo(): Observable<any> {
        let headerss = this.getHeaders();

        let data = {};

        return this._http.post(this.base_url + 'cargas/exportar_ejemplo', data, { headers: headerss });
    }
}