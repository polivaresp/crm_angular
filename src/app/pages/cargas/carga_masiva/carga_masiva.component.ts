import { Component } from '@angular/core';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as XLSX from 'xlsx';
import { CargasService } from '../services/cargas.service';
import { ExcelService } from '../../../services/excel.service';
import { BaseService } from '../../../services';
import { SelectorCamposModalComponent } from '../modals/selector_campos/selector_campos.modal';
import { NbDialogService } from '@nebular/theme';


@Component({
    selector: 'ngx-carga-masiva',
    templateUrl: './carga_masiva.component.html',
    providers: [CargasService, ExcelService, BaseService]
})
export class CargaMasivaComponent {

    headers;
    columnas_lista: any;
    cabeceras: any;
    arrayBuffer: any;
    file: File;
    array_archivo;
    verificado;
    validos;
    invalidos;
    archivo;
    campanias;
    campania;
    subiendo;

    constructor(private _route: ActivatedRoute, private _router: Router, private _toasterService: ToasterService,
        private _cargasService: CargasService, private _excelService: ExcelService, private _baseService: BaseService,
        private _modalService: NbDialogService) {
        this.verificado = 0;
        this.validos = [];
        this.invalidos = [];
        this.archivo = '';
        this.campanias = [];
        this.campania = '';
        this.subiendo = 0;
    }

    ngOnInit() {
        this._baseService.lista_simple('campania').subscribe(response => {
            if (response.resultado == "ACK") {
                this.campanias = response.lista;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    incomingfile(event) {
        this.verificado = 0;
        this.validos = [];
        this.invalidos = [];
        if (event.target.files[0]) {
            this.file = event.target.files[0];
            this.generar_array();
        }
    }

    descargar_ejemplo() {
        this._cargasService.exportar_ejemplo().subscribe(response => {
            if (response.resultado == "ACK") {
                let aux = response.lista
                this._excelService.exportAsExcelFile(aux, 'Carga Masiva Ejemplo');
            }
        }, err => {
            this.showToast('error', 'Error!', 'Error Interno');
        });
    }

    export_validos() {
        this._excelService.exportAsExcelFile(this.validos, 'Cargas Validas');
    }

    export_invalidos() {
        this._excelService.exportAsExcelFile(this.invalidos, 'Cargas Invalidas');
    }


    verificar() {
        this.verificado = 0;
        this.validos = [];
        this.invalidos = [];
        this._cargasService.verificar(this.array_archivo).subscribe(response => {
            if (response.resultado == "ACK") {
                this.verificado = 1;
                this.validos = response.validos;
                this.invalidos = response.invalidos;
                this.cabeceras = response.cabeceras;
                this.columnas_lista = response.lista_columnas;
                this.showToast('success', 'Validado', response.mensaje);
            } else {
                this.showToast('error', 'Error', response.mensaje);
                this.invalidos = response.invalidos ? response.invalidos : [];
                this.verificado = 0;
            }
        }, err => {
            this.showToast('error', 'Error!', err);
            this.verificado = 0;
        });
    }

    generar_array() {
        let fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, { type: "binary" });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            this.array_archivo = XLSX.utils.sheet_to_json(worksheet, { raw: false, defval: '' });
        }
        fileReader.readAsArrayBuffer(this.file);
    }

    upload() {
        this.subiendo = 1;
        this._cargasService.subir(this.headers, this.validos, this.campania).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast('success', 'Exito', response.mensaje);
                this.archivo = '';
                this.campania = '';
                this.verificado = 0;
                this.validos = [];
                this.invalidos = [];
                this.subiendo = 0;
            } else {
                this.showToast('error', 'Error', response.mensaje);
                this.subiendo = 1;
            }
        }, err => {
            this.subiendo = 1;
            this.showToast('error', 'Error!', err);
        });
    }

    showModal() {
        const activeModal = this._modalService.open(SelectorCamposModalComponent, {
            context: {
                modalHeader: 'Seleccione los campos correspondientes',
                columnas: this.cabeceras,
                columnas_lista: this.columnas_lista,
            }
        });
        activeModal.onClose.subscribe(result => {
            if (result) {
                console.log(result);
                this.headers = result;
                this.upload();
            }
        });
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }
}