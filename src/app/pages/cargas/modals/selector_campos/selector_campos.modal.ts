import { Component } from '@angular/core';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import * as moment from 'moment';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProspectoService } from '../../../prospecto/services/prospecto.service';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-modal',
  templateUrl: './selector_campos.modal.html',
  providers: [ProspectoService]
})
export class SelectorCamposModalComponent {

  modalHeader;
  id_usuario: number;
  campos: FormGroup;
  data: any;
  camposvariables;
  columnas: any;
  columnas_lista;
  aux;


  constructor(private activeModal: NbDialogRef<SelectorCamposModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
    private _prospectoService: ProspectoService) {
    this.campos = fb.group({
      rut: ['', Validators.required],
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      telefono: ['', Validators.required],
      correo: ['', Validators.required],
      interes: ['', Validators.required],
      comentario: '',
      dato_0: '',
      dato_1: '',
      dato_2: '',
      dato_3: '',
      dato_4: '',
      dato_5: '',
      dato_6: '',
      dato_7: '',
      dato_8: '',
      dato_9: ''
    });
    this.data = {};
    this.aux = ["1", "2", "3", "4"];
  }

  ngOnInit() {
    this.campos.patchValue(this.columnas)
    //cargar camposvariables
    this._prospectoService.camposvariables({ activo: 1 }, ["orden", "ASC"], "all")
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.camposvariables = response.lista;
        } else {
          this.showToast("error", "Error", "error a cargar camposvariables");
        }
      }, (err) => {
        this.showToast("error", "Error", "camposvariables")
      });
  }

  closeModal() {
    if (!this.isEmpty(this.data)) {
      this.activeModal.close(this.data);
    } else {
      this.activeModal.close(0);
    }
  }

  submit() {
    this.data = this.campos.value;
    this.closeModal();
  }

  private isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}
