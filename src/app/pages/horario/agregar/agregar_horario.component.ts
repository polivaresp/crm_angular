import { Component } from '@angular/core';

import * as moment from 'moment';
/* import * as md5 from 'md5'; */

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsuarioService } from '../../usuario/services/usuario_service';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services/index';
import { GestionService } from '../../gestion/services/gestion.service';


@Component({
  selector: 'ngx-agregar-horario',
  templateUrl: './agregar_horario.component.html',
  providers: [UsuarioService, BaseService, GestionService]
})
export class AgregarHorarioComponent {
  fecha: any;
  min: any;
  ejecutivos: any;

  user: any;
  formHorario: FormGroup;
  sucursales;
  etiquetas: any;

  constructor(private _router: Router, private fb: FormBuilder, private _userService: UsuarioService,
    private _toasterService: ToasterService, private _baseService: BaseService, private _gestionService: GestionService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.sucursales = [];
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.formHorario = fb.group({
      id_sucursal: ['', Validators.required],
      id_ejecutivo: ['', Validators.required],
      fecha: ['', Validators.required],
      bloque: ['', Validators.required],
    });
    let now = new Date();
    this.min = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
  }

  ngOnInit() {
    this._baseService.lista_simple('sucursal', { activo: 1 }).subscribe(response => {
      if (response.resultado == "ACK") {
        this.sucursales = response.lista;
      } else {
        this.showToast("error", "Error", "error a cargar sucursales");
      }
    }, error => {
      this.showToast("error", "Error", "error interno sucursales");
    })
  }

  cambio_sucursal() {
    this._gestionService.ejecutivos_sc(this.formHorario.value.id_sucursal)
      .subscribe(response => {
        if (response.resultado == "ACK") {
          this.ejecutivos = response.lista;
        } else {
          this.showToast("error", "Error", "error a cargar ejecutivos");
        }
      }, (err) => {
        this.showToast("error", "Error", "ayayai ejecutivos")
      });
  }

  change_fecha(event) {
    let fecha = event ? event : this.fecha;
    if (fecha.year) {
      this.formHorario.controls["fecha"].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
    } else {
      this.formHorario.controls["fecha"].setValue('');
    }
  }

  submit() {
    this._gestionService.agregar_horario(this.formHorario.value).subscribe(res => {
      if (res.resultado == "ACK") {
        this.showToast("success", "Exito", res.mensaje);
        this._router.navigate(['pages/horarios/lista']);
      } else {
        this.showToast("error", "Error", res.mensaje);
      }
    }, err => {

    });
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/horarios/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}