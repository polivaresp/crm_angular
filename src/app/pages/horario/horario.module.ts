import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { HorarioRoutingModule, routedComponents } from './horario-routing.module';
import { FilterSucursalPipe } from '../../services/filter_sucursal.pipe';
import { MatrizEditarModalComponent } from './modals/matriz_editar/matriz_editar.modal';
import { GenerarMatrizModalComponent } from './modals/generar/generar_matriz.modal';
import { NgbPaginationModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbButtonModule, NbDialogModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    HorarioRoutingModule,
    NgbPaginationModule,
    NbCardModule,
    NbDialogModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NbCheckboxModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
    FilterSucursalPipe
  ],
  providers: [],
  entryComponents: [
    MatrizEditarModalComponent,
    GenerarMatrizModalComponent
  ]
})
export class HorarioModule { }
