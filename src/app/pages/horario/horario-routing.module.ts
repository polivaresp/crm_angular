import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HorarioComponent } from './horario.component';
import { AgregarHorarioComponent } from './agregar/agregar_horario.component';
import { ListaBloquesComponent } from './lista/lista_bloques.component';
import { MatrizHorarioComponent } from './matriz/matriz_horario.component';
import { MatrizEditarModalComponent } from './modals/matriz_editar/matriz_editar.modal';
import { GenerarMatrizModalComponent } from './modals/generar/generar_matriz.modal';

const routes: Routes = [
  {
    path: '',
    component: HorarioComponent,
    children: [
      {
        path: 'agregar',
        component: AgregarHorarioComponent,
      },
      {
        path: 'lista',
        component: ListaBloquesComponent,
      },
      {
        path: 'matriz',
        component: MatrizHorarioComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HorarioRoutingModule { }

export const routedComponents = [
  HorarioComponent,
  ListaBloquesComponent,
  AgregarHorarioComponent,
  MatrizHorarioComponent,
  MatrizEditarModalComponent,
  GenerarMatrizModalComponent
];
