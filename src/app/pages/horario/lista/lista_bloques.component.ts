import { Component } from '@angular/core';

import * as moment from 'moment';
import { Router } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';
import { UsuarioService } from '../../usuario/services/usuario_service';
import { HorarioService } from '../services/horario.service';


@Component({
  selector: 'ngx-horario-lista',
  templateUrl: './lista_bloques.component.html',
  providers: [UsuarioService, BaseService, HorarioService]
})
export class ListaBloquesComponent {

  ejecutivos: any[];
  pagina_actual: number = 1;


  lista: Array<any> = [];
  cantidad: number = 0;
  anular_reserva = [];
  habilitar_reserva = [];
  filtros;
  fecha;
  acciones: any;
  etiquetas: any;

  cantidad_pagina: any = 0;
  previusPage: any;

  constructor(private router: Router, private _router: Router, private _usuarioService: UsuarioService,
    private _toasterService: ToasterService, private _baseService: BaseService, private _horarioService: HorarioService) {
    this.filtros = [];
    this.filtros["id_ejecutivo"] = '';
    this.filtros["fecha"] = '';
    this.filtros["disponible"] = null;
    this.acciones = JSON.parse(localStorage.getItem('acciones'));
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
  }

  ngOnInit() {
    this._usuarioService.lista_simple([5, 7]).subscribe(response => {
      if (response.resultado == "ACK") {
        this.ejecutivos = response.lista;
        this.submit(1);
      } else {
        this.showToast("error", "Error", "error a cargar ejecutivos");
      }
    }, error => {
      this.showToast("error", "Error", "error a cargar ejecutivos");
    });
  }

  cambio_pagina(page: number) {
    if (page !== this.previusPage) {
      this.previusPage = page;
      this.submit();
    }
  }

  agregar() {
    this.router.navigate(['pages/horarios/agregar']);
  }

  matriz() {
    this.router.navigate(['pages/horarios/matriz']);
  }

  editar(obj) {
    this.router.navigate(['pages/horarios/agregar']);
  }

  submit(reiniciar_pagina = 0) {
    if (reiniciar_pagina == 1) {
      this.pagina_actual = 1;
    }

    this.lista = [];
    let filtros = {
      id_ejecutivo: this.filtros["id_ejecutivo"] && this.filtros["id_ejecutivo"] != '' ? this.filtros["id_ejecutivo"] : null,
      fecha: this.filtros["fecha"] && this.filtros["fecha"] != '' ? this.filtros["fecha"] : null,
      disponible: this.filtros["disponible"] && this.filtros["disponible"] != '' ? this.filtros["disponible"] : null
    }
    this._horarioService.lista(filtros, ['fecha', 'asc'], this.pagina_actual).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.lista = response.lista;
        this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
        this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;


      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", 'Error interno al cargar reservas');
    });
  }

  limpiar_filtros() {
    this.filtros["id_ejecutivo"] = "";
    this.filtros["fecha"] = "";
    this.filtros["disponible"] = null;
    this.fecha = "";
    this.submit(1);
  }

  change_fecha(event) {
    let fecha = event ? event : this.fecha;
    if (fecha.year) {
      this.filtros["fecha"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      this.filtros["fecha"] = '';
    }
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

  anular(obj) {
    this._baseService.desactivar('reserva', obj.id_reserva).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this.submit(1);
        this.anular_reserva = [];
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

  getDate(fecha, format) {
    if (fecha) {
      if (fecha != '') {
        let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
        return x;
      }
    }
    return '-';
  }

  habilitar(obj) {
    this._baseService.habilitar('reserva', obj.id_reserva).subscribe(response => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Exito", response.mensaje);
        this.submit(1);
        this.habilitar_reserva = [];
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, error => {
      this.showToast("error", "Error", error);
    });
  }

}