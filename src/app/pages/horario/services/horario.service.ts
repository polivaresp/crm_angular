import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { APP_CONFIG } from '../../../services/config.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HorarioService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista(post, orden, pagina): Observable<any> {
        let headerss = this.getHeaders();
        let data = { post: post, orden: orden };

        return this._http.post(this.base_url + "ejecutivos/bloques_horarios/lista_reserva/" + pagina, data, { headers: headerss });

    }

    lista_matriz(): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + "ejecutivos/bloques_horarios/matriz_lista", data, { headers: headerss });

    }

    matriz_editar(form, id): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'matriz': form, "id": id };

        return this._http.post(this.base_url + 'ejecutivos/bloques_horarios/matriz_editar', data, { headers: headerss });

    }

    generar_matriz(definicion, matriz): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'definicion': definicion, "matriz": matriz };

        return this._http.post(this.base_url + 'ejecutivos/bloques_horarios/generar_matriz', data, { headers: headerss });

    }
}