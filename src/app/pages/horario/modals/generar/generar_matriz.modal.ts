import { Component } from '@angular/core';

import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HorarioService } from '../../services/horario.service';
import * as moment from 'moment';

@Component({
    selector: 'ngx-modal',
    templateUrl: './generar_matriz.modal.html',
    providers: [HorarioService]
})
export class GenerarMatrizModalComponent {

    data: any;
    desde;
    hasta;
    bloque;
    seleccionados;
    loading;


    constructor(private activeModal: NbDialogRef<GenerarMatrizModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
        private _horarioService: HorarioService) {
        this.data = {};
        this.loading = 0;
    }

    ngOnInit() { }

    submit() {
        this.loading = 1;
        let form = [];
        form["fecha_desde"] = this.desde;
        form["fecha_hasta"] = this.hasta;
        form["tamano_bloque"] = this.bloque;
        let formulario = {};
        Object.assign(formulario, form);
        this._horarioService.generar_matriz(formulario, this.seleccionados).subscribe(res => {
            if (res.resultado == "ACK") {
                this.showToast("success", "Horarios Generados", res.mensaje);
                this.closeModal();
                //this.formSelTodos.controls["seleccionar_todo"].setValue(false);
                this.loading = 0;
            } else {
                this.showToast("error", "Error", res.mensaje);
                this.loading = 0;
            }
        }, err => {
            this.showToast("error", "Error", err);
            this.loading = 0;
        });
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    closeModal(canc = 1) {
        let res = { data: this.data, cancelo: canc }
        this.activeModal.close(res);
    }

    private showToast(type: string, title: string, body: string) {
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}