import { Component } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import * as moment from 'moment';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HorarioService } from '../../services/horario.service';

@Component({
  selector: 'ngx-matriz-editar-modal',
  templateUrl: './matriz_editar.modal.html',
  providers: [HorarioService]
})
export class MatrizEditarModalComponent {

  data;
  user: any;
  etiquetas: any;
  obj: any;
  bloque: any;
  form: FormGroup;
  horario;

  constructor(private activeModal: NbDialogRef<MatrizEditarModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
    private _horarioService: HorarioService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.form = fb.group({
      lvami: '',
      lvamf: '',
      lvpmi: '',
      lvpmf: '',
      saami: '',
      saamf: '',
      sapmi: '',
      sapmf: '',
      doami: '',
      doamf: '',
      dopmi: '',
      dopmf: '',
    });
    this.horario = [];
  }

  ngOnInit() {
    for (let h = 0; h < 24; h++) {
      let m = 0;
      while (m < 60) {
        let dec = m / 60;
        let tiempo = h + dec;
        let min = m <= 0 ? "0" + m : m;
        let hora = h < 10 ? "0" + h : h;
        this.horario.push({ valor: tiempo, glosa: hora + ":" + min });
        m += 30/* this.bloque */;
      }
    }
    this.form.controls["lvami"].setValue(this.obj.lvami);
    this.form.controls["lvamf"].setValue(this.obj.lvamf);
    this.form.controls["lvpmi"].setValue(this.obj.lvpmi);
    this.form.controls["lvpmf"].setValue(this.obj.lvpmf);
    this.form.controls["saami"].setValue(this.obj.saami);
    this.form.controls["saamf"].setValue(this.obj.saamf);
    this.form.controls["sapmi"].setValue(this.obj.sapmi);
    this.form.controls["sapmf"].setValue(this.obj.sapmf);
    this.form.controls["doami"].setValue(this.obj.doami);
    this.form.controls["doamf"].setValue(this.obj.doamf);
    this.form.controls["dopmi"].setValue(this.obj.dopmi);
    this.form.controls["dopmf"].setValue(this.obj.dopmf);
  }

  submit() {
    let formulario = {};
    Object.assign(formulario, this.form.value);
    console.log(this.obj);
    this._horarioService.matriz_editar(formulario, this.obj.id_ejecutivo_sucursal).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this.closeModal();
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    });

  }

  closeModal() {
    this.activeModal.close(this.data);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}