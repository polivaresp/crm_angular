import { Component } from '@angular/core';

import * as moment from 'moment';

import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { HorarioService } from '../services/horario.service';
import { MatrizEditarModalComponent } from '../modals/matriz_editar/matriz_editar.modal';

import { GenerarMatrizModalComponent } from '../modals/generar/generar_matriz.modal';
import { NbDialogService } from '@nebular/theme';


@Component({
  selector: 'ngx-matriz-horario',
  templateUrl: './matriz_horario.component.html',
  providers: [HorarioService]
})
export class MatrizHorarioComponent {
  fecha: any;
  now;
  minDate: { year: any; month: any; day: any; };
  maxDate: { year: any; month: any; day: any; };
  definicion: any;

  searchText: any;
  user: any;
  etiquetas: any;
  matriz: any;
  fecha_desde: { year: any; month: any; day: any; };
  fecha_hasta: { year: any; month: any; day: any; };
  formSelTodos: FormGroup;
  seleccionados: any[];
  isSeleccionados: boolean;
  loading: number;

  constructor(private _router: Router, private fb: FormBuilder, private _modalService: NbDialogService,
    private _toasterService: ToasterService, private _horarioService: HorarioService) {
    this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
    this.matriz = [];
    this.definicion = [];
    this.definicion["tamano_bloque"] = 30;
    this.now = new Date();
    this.minDate = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
    this.fecha_desde = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
    this.fecha_hasta = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
    this.cambio_fecha_desde();
    this.cambio_fecha_hasta();
    this.seleccionados = [];
    this.isSeleccionados = false;
    this.formSelTodos = fb.group({
      seleccionar_todo: false,
    });
    this.loading = 0;
  }

  ngOnInit() {
    this.submit();
    this.formSelTodos.get('seleccionar_todo').valueChanges.subscribe((sel) => {
      this.seleccionados = [];
      this.matriz.forEach(function (obj, indx) {
        obj.seleccionado = sel;
      });
      this.validar_seleccionados();
    })
  }

  validar_seleccionados() {
    let seleccionadas = [];
    this.matriz.forEach(function (obj, ind) {
      if (obj.seleccionado == true) {
        seleccionadas.push(obj);
      } else {
        let pos = seleccionadas.indexOf(obj);
        if (pos >= 0) {
          seleccionadas.splice(pos, 1);
        }
      }
    });
    this.seleccionados = seleccionadas;
  }

  submit() {
    this._horarioService.lista_matriz().subscribe(res => {
      if (res.resultado == "ACK") {
        this.matriz = res.lista;
      } else {
        this.showToast("error", "Error", res.mensaje);
      }

    }, err => {
      this.showToast("error", "Error", err);
    });
  }

  generar_matriz() {
    this.loading = 1;
    //console.log(this.definicion);

    const activeModal2 = this._modalService.open(GenerarMatrizModalComponent, {
      context: {
        desde: this.definicion["fecha_desde"],
        hasta: this.definicion["fecha_hasta"],
        seleccionados: this.seleccionados,
        bloque: this.definicion["tamano_bloque"],
      }
    });

    activeModal2.onClose.subscribe((result) => {
      this.submit();
      this.formSelTodos.controls["seleccionar_todo"].setValue(false);
      this.loading = 0;
    }, (reason) => {
      console.log(reason);
    });
  }

  cambio_fecha_desde(event = null, a = null) {
    let fecha = event ? event : this.fecha_desde;
    //console.log(fecha);
    if (fecha.year != "") {
      //console.log("cambio");
      this.definicion["fecha_desde"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      //console.log("no cambio");
      this.definicion["fecha_desde"] = '';
    }
  }

  cambio_fecha_hasta(event = null, a = null) {
    let fecha = event ? event : this.fecha_hasta;
    //console.log(fecha);
    if (fecha.year != "") {
      //console.log("cambio");
      this.definicion["fecha_hasta"] = fecha.year + "-" + fecha.month + "-" + fecha.day;
    } else {
      //console.log("no cambio");
      this.definicion["fecha_hasta"] = '';
    }
  }

  getTime(val) {
    if (val) {
      let time = '';
      /* console.log(val); */
      /* console.log(val.toFixed(0));
      console.log(parseInt(val)); */
      let aux_dec = val - parseInt(val);
      let aux_ent = val - aux_dec;
      /* console.log(aux_ent);
      console.log(aux_dec); */
      let min_dec = parseInt((aux_dec * 60).toFixed(0));
      //console.log(min_dec)
      let h = aux_ent < 10 ? '0' + aux_ent : aux_ent;
      //console.log(h);
      let min = min_dec < 10 ? '0' + min_dec : min_dec;
      //console.log(min)
      time = h + ':' + min;
      //console.log(time);
      return time;
    } else {
      return '00:00'
    }

  }

  editar(obj) {
    const activeModal = this._modalService.open(MatrizEditarModalComponent, {
      context: {
        obj: obj,
        bloque: this.definicion["tamano_bloque"]
      }
    });

    activeModal.onClose.subscribe((result) => {
      this.submit();
    }, (reason) => {
      console.log(reason);
    });
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/horarios/lista']);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }
}