import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { NbCardModule, NbMenuModule, NbTabsetModule, NbCheckboxModule, NbButtonModule, NbDialogModule, NbListModule } from '@nebular/theme';
import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CotizacionModule } from './cotizacion/cotizacion.module';

const PAGES_COMPONENTS = [
  PagesComponent
];

@NgModule({
  imports: [
    ThemeModule,
    NbMenuModule,
    NgbModule,
    PagesRoutingModule,
    NbListModule

  ],
  declarations: [
    PAGES_COMPONENTS,
  ]
})
export class PagesModule {
}
