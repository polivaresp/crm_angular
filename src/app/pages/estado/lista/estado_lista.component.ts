import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import { NbDialogService } from '@nebular/theme';


@Component({
    selector: 'ngx-estado-lista',
    templateUrl: './estado_lista.component.html',
    providers: [BaseService]
})
export class EstadoListaComponent {

    cantidad: number;
    estado;
    
    
    pagina_actual: number;
    lista;
    estados;
    anular_estado = [];
    habilitar_estado = [];
    cantidad_pagina: any;
    previusPage: number;
    acciones;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _modalService: NbDialogService) {
        this.lista = [];
        
        this.pagina_actual = 1;
        
        this.cantidad = 0;
        this.estado = "";
        this.estados = [];
    }

    ngOnInit() {
        this.submit(1);
    }

    anular(obj) {
        this._baseService.desactivar('estado', obj.id_estado).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_estado = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar('estado', obj.id_estado).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_estado = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'estado',
                obj: obj,
            }
        });

        activeModal.onClose.subscribe(result=>{
            if(result){
                this.submit();
            }else{
                console.log("closed");
            }
        });
    }

    editar(obj) {
        this._router.navigate(['pages/estado/editar/' + obj.id_estado]);
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._baseService.lista('estado', {}, ["id_estado", "ASC"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista;
                this.cantidad = response.cantidad > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;

            } else {
                //                this.showToast("error","Error",response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}