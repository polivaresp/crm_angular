import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstadoComponent } from './estado.component';
import { EstadoListaComponent } from './lista/estado_lista.component';
import { EditarEstadoComponent } from './editar/editar_estado.component';
import { AgregarEstadoComponent } from './agregar/agregar_estado.component';


const routes: Routes = [
  {
    path: '',
    component: EstadoComponent,
    children: [
      {
        path: 'lista',
        component: EstadoListaComponent,
      },
      {
        path: 'agregar',
        component: AgregarEstadoComponent,
      },
      {
        path: 'editar/:id',
        component: EditarEstadoComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstadoRoutingModule { }

export const routedComponents = [
  EstadoComponent,
  EstadoListaComponent,
  EditarEstadoComponent,
  AgregarEstadoComponent
];
