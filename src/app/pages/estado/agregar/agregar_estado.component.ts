import { Component } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { BaseService } from '../../../services';


@Component({
  selector: 'ngx-agregar-estado',
  templateUrl: './agregar_estado.component.html',
  providers: [BaseService]
})
export class AgregarEstadoComponent {


  form: FormGroup;
  constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _baseService: BaseService,
    private _toasterService: ToasterService) {
    this.form = fb.group({
      glosa_estado: ['', Validators.required],
      activo: true,
      interno: false,
    });
  }

  ngOnInit() {

  }

  submit() {

    let form = [];
    form["glosa_estado"] = this.form.value.glosa_estado;
    form["interno"] = this.form.value.interno;
    form["activo"] = this.form.value.activo;
    let formulario = {};
    Object.assign(formulario, form);

    this._baseService.agregar('estado', formulario).subscribe((response) => {
      if (response.resultado == "ACK") {
        this.showToast("success", "Guardado", response.mensaje);
        this._router.navigate(['pages/estado/lista']);
      } else {
        this.showToast("error", "Error", response.mensaje);
      }
    }, (error) => {
      this.showToast("error", "Error", error);
    })
  }

  salir_sin_guardar() {
    this._router.navigate(['pages/estado/lista']);
  }
  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}