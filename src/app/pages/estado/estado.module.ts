import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { EstadoRoutingModule, routedComponents } from './estado-routing.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbButtonModule, NbCheckboxModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    ThemeModule,
    EstadoRoutingModule,
    NgbPaginationModule,
    NbCardModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [],
})
export class EstadoModule { }
