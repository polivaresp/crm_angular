import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampovariableComponent } from './campovariable.component';
import { CampovariableListaComponent } from './lista/campovariable_lista.component';
import { EditarCampovariableComponent } from './editar/editar_campovariable.component';


const routes: Routes = [
  {
    path: '',
    component: CampovariableComponent,
    children: [
      {
        path: 'lista',
        component: CampovariableListaComponent,
      },
      {
        path: 'editar/:id',
        component: EditarCampovariableComponent,
      },
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      }
    ],
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampovariableRoutingModule { }

export const routedComponents = [
  CampovariableComponent,
  CampovariableListaComponent,
  EditarCampovariableComponent
];
