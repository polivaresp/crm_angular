import { Injectable } from '@angular/core';
import { APP_CONFIG } from '../../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CampovariableService {

    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    ver(id): Observable<any> {
        let headerss = this.getHeaders();

        return this._http.post(this.base_url + 'campovariable/ver/' + id, JSON.stringify({}), { headers: headerss });
    }

    editar(form, id): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'campos': form };

        return this._http.post(this.base_url + 'campovariable/editar/' + id, data, { headers: headerss });
    }

    borrar_variable(obj, id): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'variable': obj, "id_campovariable": id };

        return this._http.post(this.base_url + 'campovariable/borrar_variable', data, { headers: headerss });
    }

    verificar_campo(id, valor): Observable<any> {
        let headerss = this.getHeaders();

        let data = { "id": id, 'nuevo_valor': valor };

        return this._http.post(this.base_url + 'campovariable/verificar_variable', data, { headers: headerss });
    }

    desactivar(id): Observable<any> {
        let headerss = this.getHeaders();
        let data = {};

        return this._http.post(this.base_url + 'campovariable/desactivar/' + id, data, { headers: headerss });
    }
}