import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CampovariableRoutingModule, routedComponents } from './campovariable-routing.module';
import { NbCardModule, NbButtonModule, NbCheckboxModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrdenModalComponent } from 'src/app/modals/orden/orden.modal';

@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NbCheckboxModule,
    CampovariableRoutingModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: []
})
export class CampovariableModule { }
