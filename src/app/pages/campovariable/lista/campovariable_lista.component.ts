import { Component } from '@angular/core';
import { BaseService } from '../../../services';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { CampovariableService } from '../services/campovariable.service';
import { OrdenModalComponent } from '../../../modals/orden/orden.modal';
import * as moment from 'moment';
import { NbDialogService } from '@nebular/theme';
@Component({
    selector: 'ngx-campovariable-lista',
    templateUrl: './campovariable_lista.component.html',
    providers: [BaseService, CampovariableService]
})
export class CampovariableListaComponent {

    cantidad: number;
    pagina_actual: number;
    lista;
    anular_campovariable = [];
    habilitar_campovariable = [];
    acciones: any;
    cantidad_pagina: any;
    previusPage: number;

    constructor(private _router: Router, private _toasterService: ToasterService, private _baseService: BaseService,
        private _campovariableService: CampovariableService, private _modalService: NbDialogService) {
        this.lista = [];
        this.pagina_actual = 1;
        this.cantidad = 0;
        this.acciones = JSON.parse(localStorage.getItem('acciones'));
    }

    ngOnInit() {
        this.submit(1);
    }

    anular(obj) {
        this._campovariableService.desactivar(obj.id_campovariable).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.anular_campovariable = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    habilitar(obj) {
        this._baseService.habilitar('campovariable', obj.id_campovariable).subscribe(response => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Exito", response.mensaje);
                this.submit(1);
                this.habilitar_campovariable = [];
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    editar(obj) {
        this._router.navigate(['pages/campovariable/editar/' + obj.id_campovariable]);
    }

    getDate(fecha, format) {
        if (fecha) {
            if (fecha != '') {
                let x = moment(fecha, 'YYYY-MM-DD HH:mm:ss').format(format);
                return x;
            }
        }
        return '-';
    }

    submit(reiniciar_pagina = 0) {
        this.lista = [];
        if (reiniciar_pagina == 1) {
            this.pagina_actual = 1;
        }
        this._baseService.lista('campovariable', {}, ["id_campovariable", "ASC"], this.pagina_actual).subscribe(response => {
            if (response.resultado == "ACK") {
                this.lista = response.lista.length > 0 ? response.lista : [];
                this.cantidad = response.lista.length > 0 ? response.cantidad : 0;
                this.cantidad_pagina = response.cantidad_pagina > 0 ? response.cantidad_pagina : 10;
            } else {
                //this.showToast("error","Error",response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_pagina(page: number) {
        if (page !== this.previusPage) {
            this.previusPage = page;
            this.submit();
        }
    }

    ordenar(obj, lista) {
        const activeModal = this._modalService.open(OrdenModalComponent, {
            context: {
                modulo: 'campovariable',
                obj: obj
            }
        });
        activeModal.onClose.subscribe(res => {
            if (res) {
                this.submit();
            } else {
                console.log("closed")
            }
        });
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}