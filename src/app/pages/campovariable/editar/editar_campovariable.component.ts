import { Component } from '@angular/core';
import { Toast, BodyOutputType, ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BaseService } from '../../../services';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CampovariableService } from '../services/campovariable.service';


@Component({
    selector: 'ngx-editar-campovariable',
    templateUrl: './editar_campovariable.component.html',
    providers: [BaseService, CampovariableService]
})
export class EditarCampovariableComponent {

    form: FormGroup;
    id: any;
    variables;

    constructor(private _route: ActivatedRoute, private _router: Router, private fb: FormBuilder, private _toasterService: ToasterService,
        private _baseService: BaseService, private _campovariableService: CampovariableService) {
        this._route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this.variables = [];
        this.form = fb.group({
            glosa_campovariable: ['', Validators.required],
            tipo: ['', Validators.required],
            variables: [],
            activo: '',
            alias_carga: ''
        });
    }

    ngOnInit() {
        this._campovariableService.ver(this.id).subscribe(response => {
            if (response.resultado == "ACK") {
                this.form.controls['glosa_campovariable'].setValue(response.modelo["glosa_campovariable"]);
                this.form.controls['tipo'].setValue(response.modelo["tipo"]);
                this.form.controls['activo'].setValue(response.modelo["activo"]);
                this.form.controls['variables'].setValue(response.variables);
                this.form.controls['alias_carga'].setValue(response.modelo.alias_carga);
                this.variables = response.variables;
                //this.showToast("success","Exito",response.mensaje);
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    change() {
        if (this.form.value.tipo != '') {
            this._campovariableService.verificar_campo(this.id, this.form.value.tipo).subscribe(res => {
                if (res.resultado == "ACK") {

                } else {
                    this.showToast('error', 'Tipo no modificable', res.mensaje);
                    this.form.controls["tipo"].setValue(res.tipo);
                }
            }, err => {
                this.showToast('error', 'Error Interno', err)
            });
        }
    }

    agregar_variable() {
        if (this.variables.length > 0) {
            if (this.variables[this.variables.length - 1].glosa != '') {
                this.variables.push({ glosa: '', id: this.variables.length + 1, activo: 1, existente: 0 });
            }
        } else {
            this.variables.push({ glosa: '', id: this.variables.length + 1, activo: 1, existente: 0 });
        }
        this.form.controls["variables"].setValue(this.variables);
    }

    anular_variable(variable, index) {
        if (this.variables[index].glosa == '') {
            if (this.variables[index].existente == 0) {
                this.variables.splice(index, 1);
            } else {
                this._campovariableService.borrar_variable(variable, this.id).subscribe(res => {
                    if (res.resultado == "ACK") {
                        this.variables.splice(index, 1);
                    } else {
                        this.showToast('error', 'Error', res.mensaje);
                    }
                });
            }
        } else {
            this.variables[index].activo = 0;
        }
    }

    activar_variable(variable, index) {
        this.variables[index].activo = 1;
    }

    submit() {
        let campos = this.form.value;
        this._campovariableService.editar(campos, this.id).subscribe((response) => {
            if (response.resultado == "ACK") {
                this.showToast("success", "Guardado", response.mensaje);
                this._router.navigate(['pages/campovariable/lista']);
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, (error) => {
            this.showToast("error", "Error", error);
        });
    }

    salir_sin_guardar() {
        this._router.navigate(['pages/campovariable/lista']);
    }

    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }
}