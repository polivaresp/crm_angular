import { Component } from '@angular/core';

import * as moment from 'moment';


import { Router } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ReporteService } from '../services/reporte.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseService } from '../../../services';
import { ExcelService } from '../../../services/excel.service';

@Component({
    selector: 'ngx-gestion-ventas',
    templateUrl: './gestion_ventas.component.html',
    providers: [ReporteService, BaseService, ExcelService]
})
export class GestionVentasComponent {

    disabled_detalle: number;
    disabled_resumen: number;
    formFiltros: FormGroup;
    lista;
    footer;
    periodos;
    entidad;
    entidad_titulo;
    etiquetas: any;
    reserva: any;
    export_loading: number;
    lista_export: any;
    detalle_preparado: number;
    excel_loading;
    cargando: number;
    
    constructor(private _toasterService: ToasterService, private _reporteService: ReporteService,
        private fb: FormBuilder, private _baseService: BaseService, private _excelService: ExcelService) {
        this.lista = [];
        this.footer = {};
        this.periodos = [];
        this.entidad = '';
        this.entidad_titulo = '';
        this.formFiltros = fb.group({
            periodo: '',
            entidad: ['', Validators.required]
        });
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.export_loading = 0;
        this.lista_export = [];
        this.detalle_preparado = 0;
        this.cargando = 0;
    }

    cambio_entidad() {
        let e = (document.getElementById("entidad")) as HTMLSelectElement;
        let sel = e.selectedIndex;
        let opt = e.options[sel];
        let curText = (opt).text;
        let curValue = (opt).value;
        this.entidad_titulo = curText;
    }

    ngOnInit() {
        this._baseService.periodos('cotizacion').subscribe(response => {
            if (response.resultado == "ACK") {
                this.periodos = response.lista;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
        this._baseService.ver('estado', 17).subscribe(response => {
            if (response.resultado == "ACK") {
                this.reserva = response.modelo.activo;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cargar_excel() {
        this.disabled_resumen = 1;
        this._reporteService.gestion_ventas_excel(this.formFiltros.value).subscribe(response => {
            if (response.resultado == "ACK") {
                //this.showToast("success","Exito",response.mensaje);
                let excel = response.lista;
                this._excelService.exportAsExcelFile(excel, 'Gestion_Ventas');
                this.disabled_resumen = 0;
            } else {
                this.showToast("error", "Error", response.mensaje);
                this.disabled_resumen = 0;
            }
        }, error => {
            this.showToast("error", "Error", error);
            this.disabled_resumen = 0;
        });
    }

    cargar_excel_detalle() {
        this.disabled_detalle = 1;
        this._reporteService.gestion_ventas_detalle(this.formFiltros.value).subscribe(response => {
            if (response.resultado == "ACK") {
                this._excelService.exportAsExcelFile(response.lista, 'Gestion_Ventas_Detalle');
                this.disabled_detalle = 0;
            } else {
                this.disabled_detalle = 0;
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.disabled_detalle = 0;
            this.showToast("error", "Error", error);
        });
    }

    exportar_detalle(tipo = "xlsx") {
        this.export_loading = 1;
        let aux = [];
        this.lista_export.forEach(ele => {
            ele.forEach(e => {
                aux.push(e);
            });
        });
        if (tipo == "xlsx") {
            this._excelService.exportAsExcelFile(aux, "Gestion_Ventas_Detalle");
        } else if (tipo == "csv") {
            this._excelService.exportAsCsvFile(aux, "Gestion_Ventas_Detalle");
        }
        this.detalle_preparado = 0;
        this.export_loading = 0;
    }

    preparar_excel_detalle() {
        this.lista_export = [];
        this.detalle_preparado = 0;
        this.disabled_detalle = 1;
        var total = 0;
        this._reporteService.excel_gestion_detalle(this.formFiltros.value).subscribe(resultado => {
            resultado.forEach(element => {
                if (element.lista) {
                    this.lista_export[element.pagina - 1] = element.lista;
                    total = total + 1;
                }
                this.detalle_preparado = element.total_pag == total ? 1 : 0;
                this.disabled_detalle = element.total_pag == total ? 0 : 1;
            });
        });
    }

    submit() {
        this.cargando = 1;
        this.detalle_preparado = 0;
        this.disabled_detalle = 0;
        this.lista = [];
        this._reporteService.gestion_ventas(this.formFiltros.value).subscribe(response => {
            if (response.resultado == "ACK") {
                //this.showToast("success","Exito",response.mensaje);
                this.lista = response.lista;
                this.footer = response.footer[0];
                this.entidad = this.formFiltros.value.entidad;
                this.cargando = 0;
            } else {
                this.showToast("error", "Error", response.mensaje);
                this.cargando = 0;
            }
        }, error => {
            this.showToast("error", "Error", error);
            this.cargando = 0;
        });
    }


    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }

}