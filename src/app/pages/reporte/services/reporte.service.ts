import { Injectable } from '@angular/core';
//import * as md5 from 'md5';
import { APP_CONFIG } from '../../../services/config.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ReporteService {
    public base_url: string;
    public codigo: string;
    public config;

    constructor(private _httpClient: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;;//localStorage.getItem('cod-cliente') ? localStorage.getItem('cod-cliente') : 'adm';
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    funnel_ventas(form): Observable<any> {
        let headerss = this.getHeaders();
        let data = form;

        return this._httpClient.post(this.base_url + "reporte/funnel_ventas", data, { headers: headerss });

    }

    funnel_ventas_excel(form): Observable<any> {
        let headerss = this.getHeaders();
        let data = form;

        return this._httpClient.post(this.base_url + "reporte/funnel_ventas/excel", data, { headers: headerss });

    }

    efectividad(fecha_ini, fecha_fin): Observable<any> {
        let headerss = this.getHeaders();
        let data = { fecha_gestion_ini: fecha_ini, fecha_gestion_fin: fecha_fin };

        return this._httpClient.post(this.base_url + "reporte/efectividad", data, { headers: headerss });

    }

    funnel_ventas_detalle(form): Observable<any> {
        let headerss = this.getHeaders();
        let data = form;

        return this._httpClient.post(this.base_url + "reporte/funnel_ventas_detalle", data, { headers: headerss });

    }

    excel_funnel_detalle(filtros) {
        interface Lista {
            lista: any[];
            resutado: string;
            mensaje: string;
        };
        interface Cuenta {
            total: number;
            resutado: string;
            mensaje: string;
        };
        let headerss = new HttpHeaders();
        headerss.append('tkn-cliente', this.codigo);
        let data = filtros;
        const sequence$ = this._httpClient.post<Cuenta>(this.base_url + "reporte/contar_funnel", data, { headers: headerss.set('tkn-cliente', this.codigo) })
            .switchMap(cuenta => {
                let total = cuenta.total;
                let paginas = Math.ceil(total / 10000);
                const aux = [];
                for (let index = 1; index <= paginas; index++) {
                    let req = this._httpClient.post<Lista>(this.base_url + "reporte/funnel_ventas_detalle/" + index, data, { headers: headerss.set('tkn-cliente', this.codigo) });
                    aux.push(req);
                }
                return aux;
            });
        return sequence$;
    }

    excel_gestion_detalle(filtros) {
        interface Lista {
            lista: any[];
            resutado: string;
            mensaje: string;
        };
        interface Cuenta {
            total: number;
            resutado: string;
            mensaje: string;
        };
        let headerss = new HttpHeaders();
        headerss.append('tkn-cliente', this.codigo);
        let data = filtros;
        const sequence$ = this._httpClient.post<Cuenta>(this.base_url + "reporte/contar_gestion", data, { headers: headerss.set('tkn-cliente', this.codigo) })
            .switchMap(cuenta => {
                let total = cuenta.total;
                let paginas = Math.ceil(total / 10000);
                const aux = [];
                for (let index = 1; index <= paginas; index++) {
                    let req = this._httpClient.post<Lista>(this.base_url + "reporte/gestion_ventas_detalle/" + index, data, { headers: headerss.set('tkn-cliente', this.codigo) });
                    aux.push(req);
                }
                return aux;
            });
        return sequence$;
    }

    gestion_ventas(form): Observable<any> {
        let headerss = this.getHeaders();
        let data = form;

        return this._httpClient.post(this.base_url + "reporte/gestion_ventas", data, { headers: headerss });

    }

    gestion_ventas_excel(form): Observable<any> {
        let headerss = this.getHeaders();
        let data = form;

        return this._httpClient.post(this.base_url + "reporte/gestion_ventas/excel", data, { headers: headerss });

    }

    gestion_ventas_detalle(form): Observable<any> {
        let headerss = this.getHeaders();
        let data = form;

        return this._httpClient.post(this.base_url + "reporte/gestion_ventas_detalle", data, { headers: headerss });

    }
}
