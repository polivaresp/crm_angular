import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunnelVentasComponent } from './funnel_ventas/funnel_ventas.component';
import { ReporteComponent } from './reporte.component';
import { ReporteEfectividadComponent } from './efectividad/efectividad.component';
import { GestionVentasComponent } from './gestion_ventas/gestion_ventas.component';

const routes: Routes = [{
  path: '',
  component: ReporteComponent,
  children: [
    {
      path: 'funnel_ventas',
      component: FunnelVentasComponent,
    },
    {
      path: 'efectividad',
      component: ReporteEfectividadComponent,
    },
    {
      path: 'gestion_ventas',
      component: GestionVentasComponent
    },
    {
      path: '',
      redirectTo: 'funnel_ventas',
      pathMatch: 'full'
    }
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReporteRoutingModule { }

export const routedComponents = [
  ReporteComponent,
  FunnelVentasComponent,
  ReporteEfectividadComponent,
  GestionVentasComponent
];