import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { ChartModule } from 'angular2-chartjs';
import { ReporteRoutingModule, routedComponents } from './reporte-routing.module';
import { NbCardModule, NbButtonModule } from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PorcentajePipe } from 'src/app/services/porcentaje.pipe';

@NgModule({
  imports: [
    ThemeModule,
    ReporteRoutingModule,
    NbCardModule,
    NgbModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    NbButtonModule
  ],
  declarations: [
    ...routedComponents,
    PorcentajePipe
  ],
  providers: [],
})
export class ReporteModule { }