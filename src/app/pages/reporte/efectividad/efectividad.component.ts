import { Component } from '@angular/core';

import * as moment from 'moment';


import { Router } from '@angular/router';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ReporteService } from '../services/reporte.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseService } from '../../../services';
import { ExcelService } from '../../../services/excel.service';

@Component({
    selector: 'ngx-reporte-efectividad',
    templateUrl: './efectividad.component.html',
    providers: [ReporteService, BaseService, ExcelService]
})
export class ReporteEfectividadComponent {


    footer;
    data: any;
    options: any;
    colspan;

    disabled_detalle: number;
    disabled_resumen: number;
    formFiltros: FormGroup;
    lista;
    periodos;
    etiquetas: any;
    fecha_gestion_ini: any;
    fecha_gestion_fin: any;
    cargando: number;

    constructor(private _toasterService: ToasterService, private _reporteService: ReporteService,
        private fb: FormBuilder, private _baseService: BaseService, private _excelService: ExcelService) {
        this.lista = [];
        this.footer = {};
        this.colspan = 2;
        this.periodos = [];
        this.formFiltros = fb.group({
            fecha_gestion_ini: ['', Validators.required],
            fecha_gestion_fin: ['', Validators.required]
        });
        this.etiquetas = JSON.parse(localStorage.getItem('etiquetas'));
        this.options = {
            responsive: true,
            maintainAspectRatio: false,
            elements: {
                rectangle: {
                    borderWidth: 2,
                },
            },
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            display: true,
                            color: "#cccccc",
                        },
                        ticks: {
                            fontColor: "#cccccc",
                            min: 0
                        },
                    },
                ],
                yAxes: [
                    {
                        gridLines: {
                            display: false,
                            color: "#cccccc",
                        },
                        ticks: {
                            fontColor: "#484848",
                        },
                    },
                ],
            },
            legend: {
                position: 'right',
                labels: {
                    fontColor: "#484848",
                },
            },
        };
        let now = new Date();
        this.fecha_gestion_ini = { year: now.getFullYear(), month: now.getMonth() + 1, day: 1 };
        this.fecha_gestion_fin = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        this.cambio_fecha_ini(null);
        this.cambio_fecha_fin(null);
        this.cargando = 0;
    }

    ngOnInit() {
        this._baseService.periodos('cotizacion').subscribe(response => {
            if (response.resultado == "ACK") {
                this.periodos = response.lista;
            } else {
                this.showToast("error", "Error", response.mensaje);
            }
        }, error => {
            this.showToast("error", "Error", error);
        });
    }

    cambio_fecha_ini(event) {
        let fecha = event ? event : this.fecha_gestion_ini;
        if (fecha.year) {
            this.formFiltros.controls["fecha_gestion_ini"].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
        } else {
            this.formFiltros.controls["fecha_gestion_ini"].setValue('');
        }
    }

    cambio_fecha_fin(event) {
        let fecha = event ? event : this.fecha_gestion_fin;
        if (fecha.year) {
            this.formFiltros.controls["fecha_gestion_fin"].setValue(fecha.year + "-" + fecha.month + "-" + fecha.day);
        } else {
            this.formFiltros.controls["fecha_gestion_fin"].setValue('');
        }
    }

    submit() {
        this.cargando = 1;
        this.lista = [];
        this.options = { responsive: true }
        this._reporteService.efectividad(this.formFiltros.value.fecha_gestion_ini, this.formFiltros.value.fecha_gestion_fin).subscribe(response => {
            if (response.resultado == "ACK") {
                let labels = [];
                let gestiones = [];
                let cotizaciones = [];
                let agendamientos = [];
                let confirmadas = [];
                let reservas = [];
                let compraron = [];
                response.lista_grafico.forEach(function (elem, index) {
                    labels.push(elem.nombre_completo);
                    gestiones.push(elem.gestiones);
                    cotizaciones.push(elem.cotizaciones);
                    agendamientos.push(elem.agendados);
                    confirmadas.push(elem.confirmadas);
                    reservas.push(elem.reservas);
                    compraron.push(elem.compraron);
                });
                this.data = {
                    labels: labels,
                    datasets: [{
                        label: 'Gestiones',
                        backgroundColor: "#a16eff",
                        borderWidth: 1,
                        data: gestiones,
                    }, {
                        label: 'Cotizaciones',
                        backgroundColor: "#ff3d71",
                        borderWidth: 1,
                        data: cotizaciones,
                    }, {
                        label: 'Agendamientos',
                        backgroundColor: "#66b3ff",
                        borderWidth: 1,
                        data: agendamientos,
                    }, {
                        label: 'Confirmados',
                        backgroundColor: "#5ce191",
                        data: confirmadas,
                    },
                    {
                        label: 'Reservados',
                        backgroundColor: "#ffc107",
                        data: reservas,
                    }, {
                        label: this.etiquetas.compro,
                        backgroundColor: "#e15c5c",
                        data: compraron,
                    },
                    ],
                };
                this.lista = response.lista;
                this.footer = response.footer[0];
                this.cargando = 0;
            } else {
                this.showToast("error", "Error", response.mensaje);
                this.cargando = 0;
            }
        }, error => {
            this.showToast("error", "Error", error);
            this.cargando = 0;
        });
    }


    private showToast(type: string, title: string, body: string) {
        //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 5000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }
}