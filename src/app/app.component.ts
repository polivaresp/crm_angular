import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-root',
  template: '<toaster-container></toaster-container><router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  config: ToasterConfig;

  constructor(private analytics: AnalyticsService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade'
    });
  }
}