import { NgModule } from '@angular/core';
import { ThemeModule } from '../@theme/theme.module';
import { ClienteComponent } from './cliente.component';
import { RootClienteComponent } from './root/root-cliente.component';
import { ClienteRoutingModule } from './cliente-routing.module';
import { ClienteService } from './services/cliente.service';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NbCardModule, NbCheckboxModule, NbButtonModule, NbLayoutModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const CLIENTE_COMPONENTS = [
  ClienteComponent,
  RootClienteComponent
];

@NgModule({
  imports: [
    ThemeModule,
    ClienteRoutingModule,
    NgbPaginationModule,
    NbCardModule,
    NbLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbButtonModule
  ],
  declarations: [
    CLIENTE_COMPONENTS,
  ],
  providers: [ClienteService]
})
export class ClienteModule { }
