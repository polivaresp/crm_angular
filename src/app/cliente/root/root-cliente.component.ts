import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ClienteService } from '../services/cliente.service';


@Component({
  selector: 'ngx-root-cliente',
  templateUrl: './root-cliente.component.html',
})
export class RootClienteComponent {
  public clientes: Array<any> = [];

  constructor(private router: Router, private _clienteService: ClienteService) {

  }

  ngOnInit() {
    this._clienteService.getClientesByUsuario().subscribe(response => {
      if (response.resultado == "ACK") {
        if (response.lista.length > 1) {
          this.clientes = response.lista;
        } else if (response.lista.length == 1) {
          this.router.navigate(['pages']);
          localStorage.setItem('cod-cliente', response.lista[0].codigo);
        } else {
          this.router.navigate(['logout']);
        }
      } else {
        console.log("error a cargar clientes");
      }
    });
  }

  elegir_cliente(cliente) {
    localStorage.setItem('cod-cliente', cliente.codigo);
    localStorage.setItem('logo', cliente.imagen);
    localStorage.setItem('cliente', cliente.glosa_cliente);
    this.router.navigate(['pages']);
  }
}
