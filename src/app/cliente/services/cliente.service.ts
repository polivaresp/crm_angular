import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators'

import { APP_CONFIG } from '../../services/config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ClienteService {
    public base_url: string;
    public codigo: string;
    public config;
    public user;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;
        this.user = localStorage.getItem('usr_login') ? JSON.parse(localStorage.getItem('usr_login')) : null;
    }

    getHeaders(cod = 'adm') {
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', cod);
        return headerss;
    }

    getClientesByUsuario(): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "cliente/lista_usuario", { id_usuario: this.user.usuario.id_usuario }, { headers: headerss });
    }

    verCliente(): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "cliente/ver", { codigo: this.codigo }, { headers: headerss });
    }

    servicios(): Observable<any> {
        let headerss = this.getHeaders(this.codigo)
        return this._http.post(this.base_url + "cliente/servicios", { codigo: this.codigo }, { headers: headerss });
    }

    etiquetas(): Observable<any> {
        let headerss = this.getHeaders(this.codigo)
        return this._http.post(this.base_url + "cliente/etiquetas", { codigo: this.codigo }, { headers: headerss });
    }
}