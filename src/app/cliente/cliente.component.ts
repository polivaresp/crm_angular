import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'custom-cliente',
  template: `<router-outlet></router-outlet>`
})
export class ClienteComponent {
}
