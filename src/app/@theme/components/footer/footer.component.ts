import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Creado por <b><a href="http://www.axioncode.com" target="_blank">AxionCode</a></b> 2019</span>
    <div class="socials">
    </div>
  `,
})
export class FooterComponent {
}
