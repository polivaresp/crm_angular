import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToasterService, BodyOutputType, Toast } from 'angular2-toaster';
import { AuthenticationService } from '../services/authentication.service';
import { ClienteService } from '../../cliente/services/cliente.service';
import { PerfilService } from '../../pages/perfil/services/perfil.service';

@Component({
    selector: 'custom-login',
    templateUrl: './login.component.html',
    providers: [ClienteService, AuthenticationService, PerfilService]
})
export class CustomLoginComponent {

    imagen: string;
    codigo: string;
    user: any = {};
    loading = false;
    error = '';
    cliente;

    modulos;
    afecto_a_pago;
    pagado;
    dia_notificacion;
    dia_corte;
    servicios: any[];
    etiquetas: any;

    constructor(private router: Router, private authenticationService: AuthenticationService,
        private _toasterService: ToasterService, private _clienteService: ClienteService, private _perfilService: PerfilService) {
        this.imagen = '';
        this.codigo = '';
        this.modulos = [];
        this.servicios = [];
        this.etiquetas = [];
    }

    ngOnInit() {
        // resetear valores de login
        this.authenticationService.logout();
        // Obtener Datos asociados al cliente
        this._clienteService.verCliente().subscribe(response => {
            if (response.resultado == "ACK") {
                this.cliente = response.cliente;
                this.imagen = this.cliente.imagen;
                this.codigo = this.cliente.codigo;
                this.modulos = response.modulos;
                localStorage.setItem('modulos', JSON.stringify(this.modulos));
                this.afecto_a_pago = this.cliente.afecto_a_pago;
                this.pagado = this.cliente.pagado;
                this.dia_notificacion = this.cliente.dia_notificacion;
                this.dia_corte = this.cliente.dia_corte;
            } else {
                this.showToast('error', 'Error', response.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error', err);
        });
        // Obtener servicios asociados al cliente
        this._clienteService.servicios().subscribe(response => {
            if (response.resultado == "ACK") {
                this.servicios = response.servicios;
                localStorage.setItem('servicios', JSON.stringify(this.servicios));
            } else {
                this.showToast('error', 'Error', response.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error', err);
        });
        // Obtener etiquetas personalizadas
        this._clienteService.etiquetas().subscribe(response => {
            if (response.resultado == "ACK") {
                this.etiquetas = response.etiquetas;
                localStorage.setItem('etiquetas', JSON.stringify(this.etiquetas));
            } else {
                this.showToast('error', 'Error', response.mensaje);
            }
        }, err => {
            this.showToast('error', 'Error', err);
        });
    }

    login() {
        this.loading = true;
        let habilitado = 0;
        if (this.afecto_a_pago == 1) {
            if (this.pagado == 0) {
                let now = moment();
                let fecha_notificacion = now.format('YYYY-MM-' + this.dia_notificacion);
                let fecha_corte = now.format('YYYY-MM-' + this.dia_corte);
                if (now >= moment(fecha_notificacion) && now < moment(fecha_corte)) {
                    habilitado = 1;
                    this.showToast('warning', 'Notificación de Pago', 'Recuerde Pagar Antes del día ' + this.dia_corte);
                } else if (now >= moment(fecha_corte)) {
                    this.showToast('error', 'Servicio Impago', 'Su servicio se encuentra impago, contactesé con su administrador');
                } else {
                    habilitado = 1;
                }
            } else {
                habilitado = 1;
            }
        } else {
            habilitado = 1;
        }
        if (habilitado == 1) {
            this.authenticationService.login(this.user.username, this.user.password).subscribe(result => {
                if (result.resultado == "ACK") {
                    this._perfilService.permisos(result.usuario.id_perfil).subscribe(resp => {
                        if (resp.resultado == "ACK") {
                            localStorage.setItem('permisos', JSON.stringify(resp.lista));
                            this._perfilService.acciones(result.usuario.id_perfil).subscribe(response => {
                                if (response.resultado == "ACK") {
                                    localStorage.setItem('acciones', JSON.stringify(response.lista));
                                    let home = result.perfil.home ? result.perfil.home : 'auth/login';
                                    let auth = { usuario: result.usuario, perfil: result.perfil };
                                    localStorage.setItem('usr_login', JSON.stringify(auth));
                                    this.router.navigate([home]);
                                } else {
                                    this.loading = false;
                                    this.showToast('error', 'Error de Permisos', response.mensaje);
                                }
                            }, err => {
                                this.loading = false;
                                this.showToast('error', 'Error Interno', err.statusText);
                            });
                        } else {
                            this.loading = false;
                            this.showToast('error', 'Error de Permisos', resp.mensaje);
                        }
                    }, err => {
                        this.loading = false;
                        this.showToast('error', 'Error Interno', err.statusText);
                    });
                    this.showToast('success', 'Logueado Exitosamente', 'Espere mientras se redirige a la pagina');
                } else {// login failed
                    this.showToast('error', 'Problema al iniciar sesión', result.mensaje);
                    this.error = 'Usuario o Contraseña Incorrectas';
                    this.loading = false;
                }
            }, err => {
                this.showToast('error', 'Problema al iniciar sesión', 'Usuario o Contraseña Incorrectas');
                this.error = 'Usuario o Contraseña Incorrectas';
                this.loading = false;
            });
        } else {
            this.loading = false;
        }
    }

    private showToast(type: string, title: string, body: string) {
        const toast: Toast = {
            type: type,
            title: title,
            body: body,
            timeout: 3000,
            showCloseButton: true,
            bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this._toasterService.popAsync(toast);
    }
}