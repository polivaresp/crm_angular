import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NbAuthComponent } from '@nebular/auth'
import { CustomLoginComponent } from './login/login.component';
import { CustomLogoutComponent } from './logout/logout.component';
import { AuthComponent } from './auth.component';

export const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      { path: 'login', component: CustomLoginComponent },
      { path: 'logout', component: CustomLogoutComponent },
      { path: '', redirectTo: 'login', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgxAuthRoutingModule {
}

export const routedComponents = [
  AuthComponent,
  CustomLoginComponent,
  CustomLogoutComponent
]