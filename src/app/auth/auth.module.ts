import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxAuthRoutingModule, routedComponents } from './auth-routing.module';
import {
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbInputModule,
  NbCardModule,
  NbLayoutModule
} from '@nebular/theme';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbButtonModule, 
    NbCheckboxModule,
    NbInputModule,
    NbCardModule,
    NbLayoutModule,
    NgxAuthRoutingModule,
  ],
  declarations: [
    routedComponents
  ],
})
export class NgxAuthModule {
}