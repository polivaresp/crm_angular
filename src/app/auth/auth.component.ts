import { Component } from '@angular/core';

@Component({
    selector: 'custom-auth',
    styleUrls: ['./auth.component.scss'],
    template: `<nb-layout>
  <nb-layout-column>
      <nb-card>
          <nb-card-body>
              <router-outlet></router-outlet>
          </nb-card-body>
      </nb-card>
  </nb-layout-column>
</nb-layout>`
})
export class AuthComponent {
}
