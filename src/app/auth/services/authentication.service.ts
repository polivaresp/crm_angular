import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { APP_CONFIG } from '../../services/config.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class AuthenticationService {
    public token: string;
    public base_url: string;
    public user: any;
    public codigo: string;
    public config;

    constructor(private http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;
        var currentUser = JSON.parse(localStorage.getItem('usr_login'));
        this.token = currentUser && currentUser.token;
    }

    getHeaders() {
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    login(username: string, password: string): Observable<any> {
        let headerss = this.getHeaders();
        return this.http.post(this.base_url + 'auth/login', JSON.stringify({ username: username, password: password }), { headers: headerss });
        /* if(response.json().resultado=="ACK"){
            // login successful if there's a jwt token in the response
            let token = response.json() && response.json().usuario.token;
            if (token) {
                // set token property
                this.token = token;
                let auth = {usuario : response.json().usuario, perfil: response.json().perfil};
                this.user = auth.usuario;
                // almacenar datos de usuario en local storage mantener usuario logged 
                localStorage.setItem('usr_login', JSON.stringify(auth));
                // return true to indicate successful login
                return response.json();
            }else{
                return false;
            }
        } else {
            // return false to indicate failed login
            return response.json();
        }
    }).catch((err) => {
        return Observable.throw(err);
    }); */
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('usr_login');
        localStorage.removeItem('cod-cliente');
        localStorage.removeItem('modulos');
        localStorage.removeItem('permisos');
        localStorage.removeItem('acciones');
        localStorage.removeItem('etiquetas');
        localStorage.removeItem('servicios');
        localStorage.removeItem('filtros-ctz');
        localStorage.removeItem('tiene-filtro-lista');
        localStorage.removeItem('filtros-reasignar');
        localStorage.removeItem('tiene-fltro-reasignar');
    }
}