import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'custom-logout',
  template: `<div>Cerrando Sesión, por favor espere...</div>`
})
export class CustomLogoutComponent {

  user: any = {};
  loading = false;
  error = '';

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    // reset logout status
    this.authenticationService.logout();
    this.router.navigate(['/logout']);
  }

}
