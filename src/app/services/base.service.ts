import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { map, filter } from 'rxjs/operators'

import { APP_CONFIG } from './config.service';

@Injectable()
export class BaseService {

    public base_url: string;
    public codigo: string;
    public config;
    public user: any;

    constructor(private _http: HttpClient) {
        this.config = APP_CONFIG;
        this.base_url = this.config.api.base;
        this.codigo = this.config.codigo;
        this.user = JSON.parse(localStorage.getItem('usr_login')).usuario;
    }

    getHeaders() {
        //{headers:headerss}
        let headerss = new HttpHeaders().set('Content-Type', 'application/json').set('tkn-cliente', this.codigo);
        return headerss;
    }

    lista_simple(modelo, post = {}, orden = ['orden', 'asc']): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "base/" + modelo + "/lista_simple", { post, orden }, { headers: headerss });
    }

    lista(modelo, post = {}, orden = ['orden', 'asc'], pagina): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "base/" + modelo + "/lista/" + pagina, { post, orden }, { headers: headerss });
    }

    periodos(modelo): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + "base/" + modelo + "/periodos", {}, { headers: headerss });
    }

    ver(modelo, id): Observable<any> {
        let headerss = this.getHeaders();
        return this._http.post(this.base_url + 'base/' + modelo + '/ver/' + id, JSON.stringify({}), { headers: headerss });
    }

    desactivar(modelo, id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_usuario: this.user.id_usuario };

        return this._http.post(this.base_url + 'base/' + modelo + '/desactivar/' + id, data, { headers: headerss });
    }

    habilitar(modelo, id): Observable<any> {
        let headerss = this.getHeaders();
        let data = { id_usuario: this.user.id_usuario };

        return this._http.post(this.base_url + 'base/' + modelo + '/habilitar/' + id, data, { headers: headerss });
    }

    agregar(modelo, form): Observable<any> {
        let headerss = this.getHeaders();
        let data = { campos: form, id_usuario: this.user.id_usuario };

        return this._http.post(this.base_url + 'base/' + modelo + '/agregar', data, { headers: headerss });
    }

    editar(modelo, form, id): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'campos': form, id_usuario: this.user.id_usuario };

        return this._http.post(this.base_url + 'base/' + modelo + '/editar/' + id, data, { headers: headerss });
    }

    ordenar(modelo, form, id): Observable<any> {
        let headerss = this.getHeaders();

        let data = { 'form': form, "id": id };

        return this._http.post(this.base_url + 'base/' + modelo + '/ordenar', data, { headers: headerss });
    }
}