import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as CSV from 'json2csv';

const EXCEL_TYPE = '';
const EXCEL_EXTENSION = '.xlsx'

@Injectable()
export class ExcelService {
    constructor() {

    }

    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    public exportAsCsvFile(json: any[], csvFileName: string): void {
        let myFields = json.keys();
        const parser = new CSV.Parser(myFields);
        parser.opts.withBOM = true;
        //console.log(parser);
        const csvX = parser.parse(json); +
            this.saveAsCsvFile(csvX, csvFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

    private saveAsCsvFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: 'text/csv'
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + '.csv');
    }
}