import { Inject, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Rx';
import { environment } from "../../environments/environment";

export class AppConfig {

  //Your properties here
  readonly apiEndpoint: string;
}

/**
 * Global variable containing actual config to use. Initialised via ajax call
 */
export let APP_CONFIG: AppConfig;

@Injectable()
export class AppConfigService {

  constructor(private http: HttpClient) {
  }

  public load() {
    return new Promise((resolve, reject) => {
      this.http.get('/assets/config/config.json').catch((error: any): any => {
        reject(true);
        return Observable.throw('Server error');
      }).subscribe((envResponse: any) => {
        let t = new AppConfig();

        APP_CONFIG = Object.assign(t, envResponse);
        resolve(true);
      });

    });
  }
}