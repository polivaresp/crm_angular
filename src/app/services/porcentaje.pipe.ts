import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'porcentaje' })
export class PorcentajePipe implements PipeTransform {
    transform(cociente: number, dec: any) {
        /* console.log(cociente) */
        let aux = 0;
        if (cociente && cociente!=Infinity) {
            let potenciado = cociente * 100;
            aux = Math.round(potenciado);
        }
        let porcentaje = aux + '%'
        return porcentaje;
    }
}