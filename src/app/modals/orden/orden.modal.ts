import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseService } from '../../services';

@Component({
  selector: 'ngx-orden-modal',
  templateUrl: './orden.modal.html',
  providers: [BaseService]
})
export class OrdenModalComponent {

  data: any;
  lista;
  modulo;
  obj;
  form: FormGroup;

  constructor(private activeModal: NbDialogRef<OrdenModalComponent>, private fb: FormBuilder, private _toasterService: ToasterService,
    private _baseService: BaseService) {
    this.form = fb.group({
      tipo_orden: ['', Validators.required],
      orden: ['', Validators.required],
      despues: ''
    });
  }

  ngOnInit() {
    if (this.lista && this.lista.length > 0) {
      let aux = [];
      this.lista.forEach(element => {
        if (element["id_" + this.modulo] != this.obj["id_" + this.modulo]) {
          aux.push(element);
        }
      });
      this.lista = aux;
    } else {
      let filtro = [];
      filtro["activo"] = 1;
      if (this.modulo == "perfil") {
        filtro["elegible"] = 1;
      }
      let post = {};
      Object.assign(post, filtro);

      this._baseService.lista(this.modulo, post, ["orden", "ASC"], "all").subscribe(res => {
        if (res.resultado == "ACK") {
          let aux = [];
          res.lista.forEach(element => {
            if (element["id_" + this.modulo] != this.obj["id_" + this.modulo]) {
              aux.push(element);
            }
          });
          this.lista = aux;
        } else {
          this.showToast('error', 'Error!', res.mensaje);
        }
      }, err => {
        this.showToast('error', 'Error!', err.statusText);
      });
    }
    this.form.get('tipo_orden').valueChanges.subscribe((value) => {
      this.form.get('despues').setValidators([]);
      let orden = this.obj.orden;
      let primero = this.lista[0]["orden"];
      let ultimo = this.lista[this.lista.length - 1]["orden"];
      if (value == 1) {
        orden = primero - 1;
      } else if (value == 2) {
        orden = ultimo + 1;
      } else if (value == 3) {
        this.form.get('despues').setValidators([Validators.required]);
      }
      this.form.controls["orden"].setValue(orden);
      this.form.get('despues').updateValueAndValidity();
    });
  }

  cambio_despues(e) {
    let index = e.target.selectedIndex - 1;
    let primero = this.lista[0]["orden"];
    let ultimo = this.lista[this.lista.length - 1]["orden"];
    let despues = this.lista[index];
    let orden = this.form.value.orden;
    if (despues.orden == ultimo) {
      orden = ultimo + 1;
    } else {
      let siguiente = this.lista[index + 1]["orden"];
      orden = (despues.orden + siguiente) / 2;
    }
    this.form.controls["orden"].setValue(orden);
  }

  submit() {
    this._baseService.ordenar(this.modulo, this.form.value, this.obj["id_" + this.modulo]).subscribe(res => {
      if (res.resultado == "ACK") {
        this.data = 1;
        this.closeModal();
      } else {
        this.showToast('error', 'Error!', res.mensaje);
      }
    }, err => {
      this.showToast('error', 'Error!', err.statusText);
    });
  }


  closeModal() {
    this.activeModal.close(this.data);
  }

  private showToast(type: string, title: string, body: string) {
    //this.showToast('error', 'Error!', 'Problemas'); -->ejemplo
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this._toasterService.popAsync(toast);
  }

}