import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import * as moment from 'moment';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router){}
    
    canActivate(){
        if(localStorage.getItem('usr_login')){
            let user =  JSON.parse(localStorage.getItem('usr_login'));
            let hoy = moment();
            let last = moment(user.usuario.last_login, 'YYYY-MM-DD HH:mm:ss');
            let dif = hoy.diff(last,'minutes');
            console.log(dif)
            if(dif<720){
                return true;
            }else{
                this.router.navigate(['auth/login']);
                return false;
            }            
        }
        this.router.navigate(['auth/login']);
        return false;
    }
}