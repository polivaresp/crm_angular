import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbCardModule,
  NbRadioModule,
  NbButtonModule,
  NbLayoutModule,
} from '@nebular/theme';
import { ToasterModule } from 'angular2-toaster';
import { AppConfigService } from './services/config.service';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrdenModalComponent } from './modals/orden/orden.modal';
import { GestionListaModalComponent } from './pages/gestion/modals/gestion_lista.modal';
import { AuthGuard } from './auth-guard.service';

export function loadConfigService(configService: AppConfigService): Function {
  return () => { return configService.load() };
}

@NgModule({
  declarations: [AppComponent, OrdenModalComponent, GestionListaModalComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule.forRoot(),
    ThemeModule.forRoot(),
    NbCardModule,
    NgbModule,
    NbButtonModule,
    NbRadioModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbLayoutModule,
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
  ],
  providers: [
    AuthGuard,
    AppConfigService,
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: APP_INITIALIZER, useFactory: loadConfigService, deps: [AppConfigService], multi: true }
  ],
  entryComponents: [
    OrdenModalComponent,
    GestionListaModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
